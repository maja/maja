# download_ERA5

**IMPORTANT NOTICE**: MAJA 4.9.1 changes the download format to 'legacy format'. MAJA supports only the legacy format (format in place before 26 september), support of the new netcdf format will be planned soon. On september 26 2024 a new version of CDS and ADS was deployed, all users need to create a new account and update their API KEY, more information [here](https://confluence.ecmwf.int/display/CKB/Please+read%3A+CDS+and+ADS+migrating+to+new+infrastructure%3A+Common+Data+Store+%28CDS%29+Engine).

This tool is designed to download hourly ERA5 reanalysis data products from ECMWF. Otherwise, the graphical interface provided by CDS is available [here](https://cds.climate.copernicus.eu/datasets/reanalysis-era5-pressure-levels?tab=overview).

The tool download the Specific Humidity wich is stored in the SH file.

Specific Humidity files are available currently each hour. SH files are downloaded in netCDF format and look like this:
```
ERA5_SH_yyyymmddUTChhmm.nc
```

The files are then converted in one archive using the Earth Explorer format, which was selected as the standard format for MAJA external data. 

# Configuration

 - Create a CDS account: https://cds.climate.copernicus.eu

 - Get your API key at: https://cds.climate.copernicus.eu/profile

 - Create the file '.cdsapirc' in your home and type:
```
{
"url" : "https://cds.climate.copernicus.eu/api/"
"key" : "your_key",
}
```

# Special case: download CAMS and ERA5 data

To download both CAMS and ERA5 data (for SPOT and PLEIADES products) option 'copernicus_credentials_file' must be activated
with a JSON file containing API keys for both ADS and CDS copernicus services. '.cdsapirc' file can only retrieve data from a single copernicus service.

Example of a JSON file:

```
{
    "ADS" : {
        "url": "https://ads.atmosphere.copernicus.eu/api/",
        "key": "api-key"
    },
    "CDS" : {
        "url": "https://cds.climate.copernicus.eu/api/",
        "key": "api-key"
    }
}
```

# Example

`era5download -l 19960805T10 19960805T11  -w /path/to/my/ERA5folderNetCDF -a  /path/to/my/ERA5folderNetDBL`


# Parameters

The user can choose the following parameters with the command line:

 - w: where ERA5 files will be stored

 - l: list of dates
 - d,f: min/max range of dates to download

 - w: path to folder where netcdf data are stored (can be considered as a temporary file)
 - a: path to folder where DBL/HDR files are stored
 - k: to keep the netcdf files
 - copernicus_credentials_file:  JSON file containing API keys for both ADS and CDS copernicus services

Other parameters could be accessed within the code :

 - name of output files
 
 - format: grib or netcdf

# Info on downloaded files

 - SH files: Specific humidity (SH: expressed in the mass of water vapour per kilogram of moist air [kg/kg]) at 36 different altitude levels (pressure levels).
```
Variable name	  Description
q	 	  Specific humidity
```

Information about the specific humidity can also be found at https://confluence.ecmwf.int/display/CKB/ERA5%3A+data+documentation, or at http://apps.ecmwf.int/codes/grib/param-db/
