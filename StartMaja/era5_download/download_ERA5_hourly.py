#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016-2020 Centre National d'Etudes Spatiales (CNES), CSSI, CESBIO  All Rights Reserved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import sys
import json

sys.path.append(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), "..")
)  # Import relative modules


def download_files(dt, path_out, api_config=None):
    """
    Downloads necessary ERA5 data for MAJA and converts them into MAJA input format
    :param dt: Datetime object with date of the ERA5 data to download
    :param path_out: Directory path to write ERA5 data
    :param api_config: Dict containing two keys : 'url' and 'key' of the CDS API
    :return: The path of the ERA5 SH file downloaded
    """
    from cdsapi import Client
    from datetime import datetime

    if api_config is None:
        server = Client()
    elif "url" in api_config.keys() and "key" in api_config.keys():
        server = Client(url=api_config["url"], key=api_config["key"])
    else:
        raise ValueError("Incorrect value for api_config dict, it should contain two keys: 'url' and 'key'")

    date_courante = str(dt.year) + "%02d" % dt.month + "%02d" % dt.day
    print("Current_date =", date_courante)
    date = (
        str(dt.year)
        + "-"
        + "%02d" % dt.month
        + "-"
        + "%02d" % dt.day
        + "/"
        + str(dt.year)
        + "-"
        + "%02d" % dt.month
        + "-"
        + "%02d" % dt.day
    )

    nom_sh = (
        "ERA5_SH_"
        + date_courante
        + "UTC"
        + "%02d" % dt.hour
        + "0000.nc"
    )
    path_sh = os.path.join(path_out, nom_sh)
    print("Nom fichier de sortie SH :", path_sh)

    server.retrieve(
        'reanalysis-era5-pressure-levels',
        {
            'product_type': 'reanalysis',
            'variable': 'specific_humidity',
            'pressure_level': [
                '1', '2', '3',
                '5', '7', '10',
                '20', '30', '50',
                '70', '100', '125',
                '150', '175', '200',
                '225', '250', '300',
                '350', '400', '450',
                '500', '550', '600',
                '650', '700', '750',
                '775', '800', '825',
                '850', '875', '900',
                '925', '950', '975',
                '1000',
            ],
            "date": date,
            "time": "%02d" % dt.hour + ":00",
            "data_format": "netcdf_legacy",
        },
        path_sh)

    return path_sh


if __name__ == "__main__":
    from convert_to_exo import RawERA5Archive
    import datetime
    import argparse

    # ORDRES DE GRANDEUR
    # 1 fichier SH  = 77 Mo

    argParser = argparse.ArgumentParser()
    required_arguments = argParser.add_argument_group("required arguments")
    required_arguments.add_argument(
        "-l", "--list_date", required=True, help='start date, fmt("20171201T12")', nargs="+"
    )
    required_arguments.add_argument(
        "-a",
        "--archive_dir",
        required=True,
        help="Path where the archive DBL files are stored",
    )
    required_arguments.add_argument(
        "-w",
        "--write_dir",
        required=True,
        help="Temporary path where the products should be downloaded",
    )

    required_arguments.add_argument(
        "-k",
        "--keep",
        help="keep raw netcdf files",
        action="store_true",
        default=False,
        required=False,
    )
    required_arguments.add_argument(
        "-p", "--platform", choices=["spot","pleiades"], required=True, default="spot"
    )
    required_arguments.add_argument(
        "-c", "--copernicus_credentials_file",
        help="JSON credential file containing API keys to copernicus service to retrieve CAMS (ADS service), "
             "and ERA5 (CDS service), if this option is activate with a json file path, startmaja will download missing "
             "AUX data (CAMS and ERA5)",
        type=str,
    )

    args = argParser.parse_args()

    if args.copernicus_credentials_file is not None:
        if not os.path.isfile(args.copernicus_credentials_file):
            raise OSError(f"Cannot find {args.copernicus_credentials_file}")
        with open(args.copernicus_credentials_file, 'r') as f:
            print(f"Load Copernicus credentials in file: {args.copernicus_credentials_file}")
            copernicus_credentials = json.load(f)
            if "ADS" not in copernicus_credentials.keys():
                raise ValueError(f"Cannot find 'CDS' key in {args.copernicus_credentials_file}")
            config_api = copernicus_credentials["CDS"]
    else:
        config_api = None

    list_date = []
    for date in args.list_date:
        dt1 = datetime.datetime.strptime(date, "%Y%m%dT%H")
        list_date.append(dt1)

    # Create directories
    from StartMaja.Common import FileSystem

    FileSystem.create_directory(args.archive_dir)
    FileSystem.create_directory(args.write_dir)

    # Boucle sur les jours a telecharger
    for date in list_date:
        print("==================================")
        print("Downloading files for date %s" % date)
        print("==================================")
        sh = download_files(
            date, args.write_dir, config_api
        )

        # # Conversion to MAJA DBL/HDR format
        RawERA5Archive.process_one_file(
            args.archive_dir, sh, args.platform
        )
        if not args.keep:
            os.remove(sh)

