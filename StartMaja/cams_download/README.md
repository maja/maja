# download_CAMS

**IMPORTANT NOTICE**: MAJA 4.9.1 changes the download format to 'legacy format'. MAJA supports only the legacy format (format in place before 26 september), support of the new netcdf format will be planned soon. On september 26 2024 a new version of CDS and ADS was deployed, all users need to create a new account and update their API KEY, more information [here](https://confluence.ecmwf.int/display/CKB/Please+read%3A+CDS+and+ADS+migrating+to+new+infrastructure%3A+Common+Data+Store+%28CDS%29+Engine).

This tool is designed to download daily CAMS products from ECMWF. 
Two products are currently supported: 
- CAMS global atmospheric composition forecast, the graphical interface provided by ADS is available here: https://ads.atmosphere.copernicus.eu/datasets/cams-global-atmospheric-composition-forecasts?tab=overview
- CAMS global reanalysis (EAC4), the graphical interface provided by ADS is available here: https://ads.atmosphere.copernicus.eu/datasets/cams-global-reanalysis-eac4?tab=overview

CAMS forecast are available for S2, L8, VE and PLEIADES (three version supported: CAMS forecast, CAMS 46r1 forecast and CAMS 48r1 forecast).

CAMS reanalysis are available for SPOT and PLEIADES.

In the case of PLEIADES, CAMS reanalysis are downloaded if they are available, otherwise CAMS forecast are downloaded.

The tool downloads several types of fields :
- the Aerosol Optical Thickness AOT, for each types of aerosols, which are stored in the AOT file
- the mixing ratios of each aerosol types as a function of altitude (expressed as model levels), stored in MR file 
- the Relative Humidity as a function of altitude expressed in pressure levels, stored in the RH file.

These files are available currently twice a day for CAMS forecast and every three hours for CAMS reanalysis. The files are downloaded in netCDF format and look like this:
```
CAMS_(RA)_AOT_yyyymmdd_UTC_hhmm.nc
CAMS_(RA)_MR_yyyymmdd_UTC_hhmm.nc
CAMS_(RA)_RH_yyyymmdd_UTC_hhmm.nc
```

(RA) option is for CAMS reanalysis data.

The files are then converted in one archive using the Earth Explorer format, which was selected as the standard format for MAJA external data. 

# Configuration

 - Create a CDS account: https://ads.atmosphere.copernicus.eu/

 - Get your API key at: https://ads.atmosphere.copernicus.eu/profile

 - Create the file '.cdsapirc' in your home and type:
```
url: https://ads.atmosphere.copernicus.eu/api/
key: api_key
```

# Special case: download CAMS and ERA5 data

To download both CAMS and ERA5 data (for SPOT and PLEIADES products) option 'copernicus_credentials_file' must be activated
with a JSON file containing API keys for both ADS and CDS copernicus services. '.cdsapirc' file can only retrieve data from a single copernicus service.

Example of a JSON file:

```
{
    "ADS" : {
        "url": "https://ads.atmosphere.copernicus.eu/api/",
        "key": "api-key"
    },
    "CDS" : {
        "url": "https://cds.climate.copernicus.eu/api/",
        "key": "api-key"
    }
}
```

# Example

`camsdownload -d 20180101 -f 20180102  -w /path/to/my/CAMSfolderNetCDF -a  /path/to/my/CAMSfolderNetDBL`


# Parameters

The user can choose the following parameters with the command line:

 - d,f: min/max range of dates to download
 - w: path to folder where netcdf data are stored (can be considered as a temporary file)
 - a: path to folder where DBL/HDR files are stored
 - k: to keep the netcdf files
 - p: The platform identifier
 - copernicus_credentials_file:  JSON file containing API keys for both ADS and CDS copernicus services

Other parameters could be accessed within the code :

 - step: forecast step in hours (default 0)

 - type of file to download: surface, pressure or model (default all)

 - name of output files

 - format: grib or netcdf

# Info on downloaded files

 - Surface files: Aerosol Optical Thickness (AOT) at 550 nm for the five aerosol models (BlackCarbon, Dust, Sulfate, SeaSalt and OrganicMatter). 
Present in all CAMS forecast versions and CAMS reanalysis.
```
Variable name	  Description
suaod550	  Sulphate Aerosol Optical Depth at 550nm
bcaod550	  BlackCarbon Aerosol Optical Depth at 550nm
ssaod550	  SeaSalt Aerosol Optical Depth at 550nm
duaod550	  Dust Aerosol Optical Depth at 550nm
omaod550	  OrganicMatter Aerosol Optical Depth at 550nm
```

From 2019/07/10 onwards, the format changed (present on CAMS forecast 46r1 and 48r1). The following AOT models were added:
```
Variable name     Description
niaod550      Nitrate Aerosol Optical Depth at 550nm
amaod550      Ammonium Aerosol Optical Depth at 550nm
```

From 2023/06/24 onwards, the format changed (present on CAMS forecast 48r1), one AOT model was added:
```
Variable name     Description
soaod550      Secondary organic aerosol optical depth at 550 nm	
```

 - Model files: mass Mixing Ratios (MR: expressed in kg of aerosol per kg of dry air [kg/kg]) of the aerosol models at different altitude levels (model levels), 
60 for CAMS forecast and CAMS reanalysis, 120 for CAMS forecast 46r1 and 48r1.
Present in all CAMS forecast versions and CAMS reanalysis. 

```
Variable name	  Description
aermr01	 	  Sea Salt Aerosol (0.03 - 0.5 um) Mixing Ratio
aermr02		  Sea Salt Aerosol (0.5 - 5 um) Mixing Ratio
aermr03		  Sea Salt Aerosol (5 - 20 um) Mixing Ratio
aermr04		  Dust Aerosol (0.03 - 0.55 um) Mixing Ratio
aermr05		  Dust Aerosol (0.55 - 0.9 um) Mixing Ratio
aermr06		  Dust Aerosol (0.9 - 20 um) Mixing Ratio
aermr07		  Hydrophobic Organic Matter Aerosol Mixing Ratio
aermr08		  Hydrophilic Organic Matter Aerosol Mixing Ratio
aermr09		  Hydrophobic Black Carbon Aerosol Mixing Ratio
aermr10		  Hydrophilic Black Carbon Aerosol Mixing Ratio
aermr11		  Sulphate Aerosol Mixing Ratio
```

From 2019/07/10 onwards, the format changed (present on CAMS forecast 46r1 and 48r1). The following mixing ratios were added:
```
Variable name     Description
aermr16           Nitrate fine mode aerosol mass mixing ratio
aermr17           Nitrate coarse mode aerosol mass mixing ratio
aermr18           Ammonium aerosol mass mixing ratio
```

From 2023/06/24 onwards, the format changed (present on CAMS forecast 48r1). The following mixing ratios were added:
```
Variable name     Description
aermr19	Biogenic secondary organic aerosol mass mixing ratio
aermr20	Anthropogenic secondary organic aerosol mass mixing ratio	Geo2D

```

 - Pressure files: Relative Humidity (RH: expressed in %) at 22 different altitude levels (pressure levels).
Present in all CAMS forecast versions and CAMS reanalysis.

```
Variable name	  Description
r	 	  Relative Humidity
```

Information about these variables can also be found at https://confluence.ecmwf.int/display/CKB/CAMS%3A+Global+atmospheric+composition+forecast+data+documentation under the “Forecast surface parameters” and “Forecast model level parameters” sections, or at http://apps.ecmwf.int/codes/grib/param-db/
