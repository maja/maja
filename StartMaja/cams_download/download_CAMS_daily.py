#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016-2020 Centre National d'Etudes Spatiales (CNES), CSSI, CESBIO  All Rights Reserved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import sys
import json
from datetime import datetime, timedelta
from cdsapi import Client

sys.path.append(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), "..")
)  # Import relative modules


def init_server(api_config=None):
    if api_config is None:
        server = Client()
    elif "url" in api_config.keys() and "key" in api_config.keys():
        server = Client(url=api_config["url"], key=api_config["key"])
    else:
        raise ValueError("Incorrect value for api_config dict, it should contain two keys: 'url' and 'key'")
    return server


def check_reanalysis_available(dt, api_config=None):
    """
    Check if CAMS reanalysis are available for this date
    """

    server = init_server(api_config)

    date = (
        str(dt.year)
        + "-"
        + "%02d" % dt.month
        + "-"
        + "%02d" % dt.day
        + "/"
        + str(dt.year)
        + "-"
        + "%02d" % dt.month
        + "-"
        + "%02d" % dt.day
    )

    params = {"data_format": "netcdf_legacy",
              'date': date, 'time': '12:00',
              'variable': ['dust_aerosol_optical_depth_550nm']}
    try:
        server.retrieve("cams-global-reanalysis-eac4",params)
    except:
        print(f"CAMS are not available on cams-global-reanalysis-eac4 repository for date {dt}.")
        return False
    return True


def download_files(dt, file_type, t_hour, step, path_out, cams_src="cams", api_config=None):
    """
    Downloads necessary CAMS data for MAJA and converts them into MAJA input format
    """

    server = init_server(api_config)

    date_courante = str(dt.year) + "%02d" % dt.month + "%02d" % dt.day
    print("Current_date =", date_courante)
    date = (
        str(dt.year)
        + "-"
        + "%02d" % dt.month
        + "-"
        + "%02d" % dt.day
        + "/"
        + str(dt.year)
        + "-"
        + "%02d" % dt.month
        + "-"
        + "%02d" % dt.day
    )

    path_aot = None
    path_mr = None
    path_rh = None

    # Define the suffix for the downloaded cams files
    cams_file_suffix = (date_courante
                        + "UTC"
                        + str(int(t_hour) + int(step)).zfill(2)
                        + "0000.nc")

    # Determine the cams type to download
    # and configure the params accordingly:
    # TODO check common params:
    # - all params are not described in the current online documentation
    # - there may be remains of the old ecmwf api to remove
    if cams_src == "cams_rea":
        cams_repo = "cams-global-reanalysis-eac4"
        params = {
            # common params
            "type": "forecast",
            "data_format": "netcdf_legacy",
            "date": date,
            "time": t_hour + ":00",
            }
        print("Download cams from reanalysis")
    else:
        # Else, we query from main cams repo
        cams_repo = "cams-global-atmospheric-composition-forecasts"
        params = {
            # common params
            # "step": "0",
            "type": "forecast",
            "data_format": "netcdf_legacy",
            "date": date,
            "time": t_hour + ":00",
            "leadtime_hour": "0",
            }
        # If date > 20190710, we query the new cams
        if dt >= datetime(2023, 6, 27):
            cams_src = "cams_48r1"
            print("Download cams 48r1")
        elif dt >= datetime(2019, 7, 10):
            cams_src = "cams_46r1"
            print("Download cams 46r1")
        else:
            print("Download cams")


    if file_type["surface"]:
        # Surface
        # Recupere AOT a 550nm pour BC, SS, SU, DU, OM
        # Et (si possible p. CAMS46r1) AM, NI
        path_aot = get_path_cams(path_out, "AOT", cams_src, cams_file_suffix)
        params_aot = get_params_aot(params, cams_src, dt)
        server.retrieve(
            cams_repo,
            params_aot,
            path_aot,
        )

    if file_type["pressure"]:
        # Pressure levels
        # Recupere Relative Humidity RH
        path_rh = get_path_cams(path_out, "RH", cams_src, cams_file_suffix)
        params_rh = get_params_rh(params)
        server.retrieve(
            cams_repo,
            params_rh,
            path_rh,
        )

    if file_type["model"]:
        # Model levels
        # Recupere les mixing ratios :
        # 3 bins DUST,
        # 3 bins SEASALT,
        # ORGANICMATTER hydrophile et hydrophobe,
        # BLACKCARBON hydrophile et hydrophobe,
        # et SULFATE.
        path_mr = get_path_cams(path_out, "MR", cams_src, cams_file_suffix)
        params_mr = get_params_mr(params, cams_src)
        server.retrieve(
            cams_repo,
            params_mr,
            path_mr,
        )

    return path_aot, path_rh, path_mr


def get_params_rh(params):
    params_rh = params.copy()
    params_rh.update({
                        # add specific params
                        "variable": ["relative_humidity"],
                        "pressure_level": [
                            "1",
                            "2",
                            "3",
                            "5",
                            "7",
                            "10",
                            "20",
                            "30",
                            "50",
                            "70",
                            "100",
                            "150",
                            "200",
                            "250",
                            "300",
                            "400",
                            "500",
                            "600",
                            "700",
                            "850",
                            "925",
                            "1000",
                        ],
                        })
    return params_rh


def get_params_mr(params, cams_src):
    model_level = [str(i) for i in range(1, 61)]
    models = [
            "sea_salt_aerosol_0.03-0.5um_mixing_ratio",
            "sea_salt_aerosol_0.5-5um_mixing_ratio",
            "sea_salt_aerosol_5-20um_mixing_ratio",
            "dust_aerosol_0.03-0.55um_mixing_ratio",
            "dust_aerosol_0.55-0.9um_mixing_ratio",
            "dust_aerosol_0.9-20um_mixing_ratio",
            "hydrophilic_organic_matter_aerosol_mixing_ratio",
            "hydrophobic_organic_matter_aerosol_mixing_ratio",
            "hydrophilic_black_carbon_aerosol_mixing_ratio",
            "hydrophobic_black_carbon_aerosol_mixing_ratio",
            "sulphate_aerosol_mixing_ratio",
        ]
    # If new cams, querying the additional models
    if cams_src == "cams_46r1" or cams_src == "cams_48r1":
        model_level = [str(i) for i in range(1, 138)]
        models.extend([
                    "nitrate_fine_mode_aerosol_mass_mixing_ratio",
                    "nitrate_coarse_mode_aerosol_mass_mixing_ratio",
                    "ammonium_aerosol_mass_mixing_ratio",
                    ])
    if cams_src == "cams_48r1":
        models.extend([
                    "anthropogenic_secondary_organic_aerosol_mass_mixing_ratio",
                    "biogenic_secondary_organic_aerosol_mass_mixing_ratio"
                    ])

    params_mr = params.copy()
    params_mr.update({
                    # add specific params
                    "variable": models,
                    "model_level": model_level,
                    })

    return params_mr


def get_params_aot(params, cams_type, date):
    models = [
                "dust_aerosol_optical_depth_550nm",
                "sea_salt_aerosol_optical_depth_550nm",
                "sulphate_aerosol_optical_depth_550nm",
                "black_carbon_aerosol_optical_depth_550nm",
                "organic_matter_aerosol_optical_depth_550nm",
                ]

    # If new cams, querying the additional models
    if cams_type == "cams_46r1" or cams_type == "cams_48r1":
        models.extend([
                        "nitrate_aerosol_optical_depth_550nm",
                        "ammonium_aerosol_optical_depth_550nm",
                        ])
    if cams_type == "cams_48r1":
        models.extend([
                        "secondary_organic_aerosol_optical_depth_550nm",
                        ])
    params_aot = params.copy()
    params_aot.update({"variable": models})
    # Change leadtime_hour (0 to 1) for date before 24/01/2017
    if date < datetime(2017, 1, 25):
        params_aot.update({"leadtime_hour": "1"})

    return params_aot


def get_path_cams(path_out, cams_type_str, cams_src, cams_file_suffix):
    if cams_src == "cams_rea":
        nom_cams = "CAMS_RA_"+cams_type_str+"_" + cams_file_suffix
    else:
        nom_cams = "CAMS_"+cams_type_str+"_" + cams_file_suffix
    path_cams = os.path.join(path_out, nom_cams)
    print("Nom fichier de sortie {} :{}".format(cams_type_str, path_cams))
    return path_cams


if __name__ == "__main__":
    from convert_to_exo import RawCAMSArchive
    import argparse

    # ORDRES DE GRANDEUR
    # 1 fichier Surface  = 0.8 Mo
    # 1 fichier Pressure = 1.7 Mo
    # 1 fichier Model    = 53 Mo
    # => 20 Go par an (40 Go par an avec les deux forecasts (minuit et midi))

    # AEROSOLS DISPONIBLES
    # BC = BlackCarbon
    # SS = SeaSalt
    # SU = Sulfate
    # DU = Dust
    # OM = OrganicMatter

    # A partir du 20190710
    # AM = Ammonium
    # NI = Nitrate

    argParser = argparse.ArgumentParser()
    required_arguments = argParser.add_argument_group("required arguments")
    required_arguments.add_argument(
        "-d", "--start_date", required=True, help='start date, fmt("20171201")'
    )
    required_arguments.add_argument(
        "-f", "--end_date", required=True, help='end date, fmt("20171201")'
    )
    required_arguments.add_argument(
        "-a",
        "--archive_dir",
        required=True,
        help="Path where the archive DBL files are stored",
    )
    required_arguments.add_argument(
        "-w",
        "--write_dir",
        required=True,
        help="Temporary path where the products should be downloaded",
    )

    required_arguments.add_argument(
        "-k",
        "--keep",
        help="keep raw netcdf files",
        action="store_true",
        default=False,
        required=False,
    )
    required_arguments.add_argument(
        "-p", "--platform", choices=["s2", "l8", "ve", "spot", "pleiades"], required=True, default="s2"
    )
    required_arguments.add_argument(
        "-c", "--copernicus_credentials_file",
        help="JSON credential file containing API keys to copernicus service to retrieve CAMS (ADS service), "
             "and ERA5 (CDS service), if this option is activate with a json file path, startmaja will download missing"
             "AUX data (CAMS and ERA5)",
        type=str,
    )

    args = argParser.parse_args()

    if args.copernicus_credentials_file is not None:
        if not os.path.isfile(args.copernicus_credentials_file):
            raise OSError(f"Cannot find {args.copernicus_credentials_file}")
        with open(args.copernicus_credentials_file, 'r') as f:
            print(f"Load Copernicus credentials in file: {args.copernicus_credentials_file}")
            copernicus_credentials = json.load(f)
            if "ADS" not in copernicus_credentials.keys():
                raise ValueError(f"Cannot find 'ADS' key in {args.copernicus_credentials_file}")
            config_api = copernicus_credentials["ADS"]
    else:
        config_api = None

    # Creation objets dates
    dt1 = datetime.strptime(args.start_date, "%Y%m%d")
    dt2 = datetime.strptime(args.end_date, "%Y%m%d")
    if dt2 < dt1:
        raise ValueError(f"Invalid date order")
    if (datetime.today() - dt2).days < 5:
        raise ValueError(f"Cannot retrieve CAMS data for date {dt2}. Download date must be at least 5 days old!")

    nb_days = (dt2 - dt1).days + 1
    print("Number of days =", nb_days)

    # Two kind of CAMS: analysis and reanalysis
    # Two possibilities :
    # - 00:00:00 UTC (minuit)
    # - 12:00:00 UTC (midi)
    analysis_times = ["00", "12"]
    # For CAMS reanalysis they are eight possibilities (for every 3 hours)
    reanalysis_times = ["00", "03", "06", "09", "12", "15", "18", "21"]

    if args.platform == "spot" or args.platform == "pleiades":
        time = reanalysis_times
        cams_src = "cams_rea"
    else:
        time = analysis_times
        cams_src = "cams"

    # TODO ajouter option pour DL analysis ou reanalysis -> erreur si analysis avec spot
    #  et reanalysis avec s2, ve, landsat

    # Step du forecast voulu
    # step = 3 indique qu'on telecharge les previsions a 3h apres l'heure de l'analyse.
    # Exemples : time = 00 et step = 3 => 03:00:00 UTC
    #            time = 12 et step = 3 => 15:00:00 UTC
    step_choice = "0"

    # Choix des fichiers a telecharger
    # Surface  : AOT (aerosol optical thickness)
    # Pressure : RH  (relative humidity)
    # Model    : MR  (mixing ratios)
    ftype = {"surface": True, "pressure": True, "model": True}

    # Create directories
    from StartMaja.Common import FileSystem

    FileSystem.create_directory(args.archive_dir)
    FileSystem.create_directory(args.write_dir)

    # Boucle sur les jours a telecharger
    for i in range(nb_days):
        date = dt1 + timedelta(days=i)
        print("==================================")
        print("Downloading files for date %s" % date)
        print("==================================")
        if cams_src == "cams_rea" and args.platform == "pleiades" and not check_reanalysis_available(date):
                print(f"cams reanalysis not available, switch to cams analysis for date {date}")
                time = analysis_times
                cams_src = "cams"

        # TODO Si camsdownload  avec option reanlysis et rean plus dispo -> erreur

        for t in range(len(time)):
            # No CAMS at 12:00 before 21 june 2016 (cams 41r1)
            if time[t] == "12" and cams_src == "cams" and date < datetime(2016, 6, 21):
                continue
            print(time[t])
            aot, rh, mr = download_files(
                date, ftype, time[t], step_choice, args.write_dir, cams_src, config_api
            )
            # # Conversion to MAJA DBL/HDR format
            RawCAMSArchive.process_one_file(
                args.archive_dir, aot, rh, mr, cams_src, args.platform
            )
            if not args.keep:
                os.remove(aot)
                os.remove(mr)
                os.remove(rh)
