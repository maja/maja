#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016-2020 Centre National d'Etudes Spatiales (CNES), CSSI, CESBIO  All Rights Reserved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import logging
from StartMaja.prepare_mnt.mnt.SRTM import SRTM
from StartMaja.prepare_mnt.mnt.EuDEM import EuDEM
from StartMaja.prepare_mnt.mnt.CopDEMGLO30 import CopDEMGLO30


class MNTFactory:
    """
    Create a given DEM in Maja format
    """

    def __init__(self, site, platform_id, mission_field, mnt_resolutions, **kwargs):
        """
        Initialization
        :param site: MNT site
        :param platform_id: Platform ID
        :param mission_field: Mission name
        :param mnt_resolutions: MNT resolution dictionary
        :param kwargs: Parameters for MNTBase
        """
        self.type_dem = kwargs.get("type_dem", "any").lower()
        self.site = site
        self.plaform_id = platform_id
        self.mission_field = mission_field
        self.mnt_resolutions = mnt_resolutions
        self.coarse_res = kwargs.get(
            "coarse_res", None
        )  # Do not write coarse resolution images if set to None
        self.coarse_res_meter = self.coarse_res
        self.full_res_only = kwargs.get("full_res_only", False)
        self.kwargs = kwargs

    def factory(self):
        """
        Checks the given mnt_type and returns the class accordingly
        :return: A derived MNTBase object, None if mnt_type unknown.
        """
        error = None
        # TODO Add more options here: ALOS, TDX...
        if self.mission_field == 'PLEIADES':
            self.te_str=self.site.te_str_offset
        else:
            self.te_str=self.site.te_str

        if self.type_dem in ["glo30", "any"]:
            # Copernicus DEM GLO30 is distributed at 30m.
            # Thus, all initial calculation has to be done at this resolution:
            dst_res_x, dst_res_y = 30, -30
            if self.site.epsg == 4326:
                self.convert_resolution_to_WGS84(dst_res_x, dst_res_y)
            else:
                self.site.res_x, self.site.res_y = dst_res_x, -dst_res_y

            try:
                return CopDEMGLO30(site=self.site,te_str=self.te_str, **self.kwargs).to_maja_format(
                    platform_id=self.plaform_id,
                    mission_field=self.mission_field,
                    mnt_resolutions=self.mnt_resolutions,
                    coarse_res=self.coarse_res,
                    full_res_only=self.full_res_only,
                    coarse_res_meter=self.coarse_res_meter
                )
            except ValueError as e:
                error = e
                logger.error(e)

        if self.type_dem in ["srtm", "any"]:
            # SRTM is distributed in 90m.
            # Thus, all initial calculation has to be done at this resolution:
            dst_res_x, dst_res_y = 90, -90
            if self.site.epsg == 4326:
                self.convert_resolution_to_WGS84(dst_res_x, dst_res_y)
            else:
                self.site.res_x, self.site.res_y = dst_res_x, -dst_res_y

            try:
                return SRTM(site=self.site,te_str=self.te_str, **self.kwargs).to_maja_format(
                    platform_id=self.plaform_id,
                    mission_field=self.mission_field,
                    mnt_resolutions=self.mnt_resolutions,
                    coarse_res=self.coarse_res,
                    full_res_only=self.full_res_only,
                    coarse_res_meter=self.coarse_res_meter
                )
            except ValueError as e:
                error = e
                logger.error(e)

        if self.type_dem in ["eudem", "any"]:
            if self.site.epsg == 4326:
                self.convert_resolution_to_WGS84(self.site.res_x_ref, self.site.res_y_ref)
            try:
                return EuDEM(site=self.site,te_str=self.te_str, **self.kwargs).to_maja_format(
                    platform_id=self.plaform_id,
                    mission_field=self.mission_field,
                    mnt_resolutions=self.mnt_resolutions,
                    coarse_res=self.coarse_res,
                    full_res_only=self.full_res_only,
                    coarse_res_meter=self.coarse_res_meter
                )
            except Exception as e:
                error = e
                logger.error(e)

        if not error:
            raise ValueError("No DEM type set. Please select 'eudem', 'srtm' or 'all'.")

        raise error

    def convert_resolution_to_WGS84(self, dst_res_x, dst_res_y):
        """
        Convert meters to degree resolution
        """
        res_list = self.mnt_resolutions[0]["val"].split(" ")

        # Coarse resolution
        ratio_full_x = self.coarse_res[0]/float(res_list[0])
        ratio_full_y = self.coarse_res[1]/float(res_list[1])
        self.coarse_res = (self.site.res_x*ratio_full_x, self.site.res_y*ratio_full_y)

        # Update mnt_resolution
        self.mnt_resolutions[0]["val"] = str(self.site.res_x)+" "+str(self.site.res_y)

        # DTM resolution
        ratio_x = dst_res_x/float(res_list[0])
        ratio_y = dst_res_y/float(res_list[1])
        self.site.res_x *= ratio_x
        self.site.res_y *= ratio_y

if __name__ == "__main__":
    pass
else:
    logger = logging.getLogger("root")
