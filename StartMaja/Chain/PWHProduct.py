#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016-2020 Centre National d'Etudes Spatiales (CNES), CSSI, CESBIO  All Rights Reserved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
from datetime import datetime
from StartMaja.Chain.Product import MajaProduct
from StartMaja.Common import FileSystem, XMLTools
from StartMaja.Common.FileSystem import symlink
from StartMaja.prepare_mnt.mnt.SiteInfo import Site
from StartMaja.prepare_mnt.mnt.MNTFactory import MNTFactory


class PleiadeWorldHeritage(MajaProduct):
    """
    A Pleiade World Heritage product
    """

    base_resolution = (2, -2)
    coarse_resolution = (100, -100)
    def get_mnt(self, **kwargs):
        """
        Download the DTM
        :param kwargs: Parameters for the MNTFactory
        :return: A derived MNTBase object, None if mnt_type unknown
        """
        sat = "_".join(self.base.split("_")[:2])
        date = self.base.split("_")[2]
        tmp = kwargs.pop("coarse_res")
        if not tmp:
            coarse_res = self.coarse_resolution
        else:
            coarse_res = (tmp, -tmp)
        return MNTFactory(
            site=self.mnt_site,
            platform_id=sat+"_"+date,
            mission_field=self.type_xml_maja,
            mnt_resolutions=self.mnt_resolutions_dict,
            coarse_res=coarse_res,
            **kwargs
        ).factory()

    @property
    def platform(self):
        return "pleiades"

    @property
    def short_name(self):
        return "pwh"

    @property
    def type(self):
        return "natif"

    @property
    def level(self):
        if "L2A" in  self.base:
            return "l2a"
        else:
            return "l1c"

    @property
    def nodata(self):
        return 0

    @property
    def tile(self):
        # No tile for PWH, it returns the GEOGRAPHICAL_ZONE
        if "L2A" in self.base:
            return self.base.split("_")[3]
        else:
            return self.base.split("_")[5]

    @property
    def metadata_file(self):
        return self.find_file("ipu_metadata_sigma.xml", path=os.path.join(self.fpath,"METADATA"), recursive=True)[0]

    @property
    def date(self):
        if "L2A" in self.base:
            str_date = self.base.split("_")[1][:15]
            return datetime.strptime(str_date, "%Y%m%d-%H%M%S")
        else:
            str_date = self.base.split("_")[2]
            return datetime.strptime(str_date, "%Y%m%d%H%M%S%f")

    @property
    def validity(self):
        """
            Check validity of Pleiade product
        """
        # Check that metadata file exist
        if os.path.exists(self.metadata_file):
            return True

        return False

    def link(self, link_dir):
        symlink(self.fpath, os.path.join(link_dir, self.base))

    @property
    def mnt_site(self):
        try:
            band_b2 = self.find_file(pattern=r"multiband.vrt", path=os.path.join(self.fpath,"PRODUCT_FOLDER_REECH"), recursive=True)[0]
        except IOError as e:
            raise e
        return Site.from_raster(self.tile, band_b2)


    @property
    def mnt_resolutions_dict(self):
        return [
            {
                "name": "XS",
                "val": str(self.mnt_resolution[0]) + " " + str(self.mnt_resolution[1]),
            }
        ]

    def get_synthetic_band(self, synthetic_band, **kwargs):
        raise NotImplementedError

    def rgb_values(self):
        raise NotImplementedError

