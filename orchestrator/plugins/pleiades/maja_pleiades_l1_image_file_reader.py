# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################
#
#                        o     o
#                        oo   oo   oo        o   oo        ,-.
#                        o o o o  o  o       o  o  o       \_/
#                        o  o  o o    o      o o    o     |||)<
#                        o     o oooooo      o oooooo      / \
#                        o     o o    o o    o o    o      `-^
#                        o     o o    o  oooo  o    o
#
###################################################################################################

orchestrator.plugins.sentinel2.maja_sentinel2_l1_image_file_reader -- shortdesc

orchestrator.plugins.maja_sentinel2_l1_image_file_reader is a description

It defines classes_and_methods

###################################################################################################
"""
import os
import pathlib
import math
from sunpy.coordinates.sun import earth_distance

from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.common.maja_exceptions import MajaPluginPleiadesException
from orchestrator.cots.gdal.gdal_warp import resample_with_dtm
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler
from orchestrator.cots.otb.otb_pipeline_manager import OtbPipelineManager
from orchestrator.cots.otb.algorithms.otb_resample import resample
from orchestrator.cots.otb.algorithms.otb_resample import OtbResampleType
from orchestrator.cots.otb.algorithms.otb_band_math import band_math
from orchestrator.plugins.pleiades.maja_pleiades_plugin import MajaPleiadesPlugin
from orchestrator.cots.otb.algorithms.otb_multiply_by_scalar import multiply_by_scalar
from orchestrator.cots.otb.algorithms.otb_extract_roi import extract_roi
from orchestrator.cots.otb.algorithms.otb_resample import resample
from orchestrator.cots.otb.algorithms.otb_constant_image import constant_image

from orchestrator.plugins.common.base.maja_l1_image_reader_base import L1ImageReaderBase
import orchestrator.common.xml_tools as xml_tools


LOGGER = configure_logger(__name__)


class PleiadesL1ImageFileReader(L1ImageReaderBase):
    def __init__(self):
        super(PleiadesL1ImageFileReader, self).__init__()
        self._Satellite = "PLEIADES"
        self._plugin = MajaPleiadesPlugin()
        self._toa_scalar_list = []
        self._toa_sub_list = []
        self._l1toaimagelist = []
        self._l2edgmasklist = []
        self._l2toaimagelist = []
        self._vieimagelist = []
        # Store the apps
        self._l1toa_pipeline = OtbPipelineManager()
        self._l2edg_pipeline = OtbPipelineManager()
        self._toa_pipeline = OtbPipelineManager()
        self._header_handler = None

    def can_read(self, plugin_name):
        return plugin_name == "PLEIADES"

    def generate_edg_images(self, working_dir):
        dtm_coarse = self._dem.ALC
        # One band equal threshold

        # Pleiades multiband
        out_edg = os.path.join(working_dir, "tmp_EDG_oneBandEqual.tif")
        param_edg = {
            "im": self._l1toaimagelist[0],
            "thresholdvalue": 0,
            "equalvalue": 255,
            "outsidevalue": 0,
            "out": out_edg + ":uint8",
        }
        onebandequal_app = OtbAppHandler(
            "OneBandEqualThreshold", param_edg, write_output=True
        )

        # Resample to coarse
        LOGGER.debug("Start IPEDGSub.")
        tmp_edg_sub_resample = os.path.join(working_dir, "tmp_edg_tmp.tif")
        tmp_edg_sub_bandmath = os.path.join(working_dir, "tmp_edg_sub.tif")

        resample_with_dtm(out_edg, dtm_coarse, tmp_edg_sub_resample, no_data_dst=255, interp="near")

        param_bandmath = {
            "il": [tmp_edg_sub_resample],
            "exp": "(im1b1 == 255)",
            "out": tmp_edg_sub_bandmath,
        }
        bandmath_app = OtbAppHandler(
            "BandMathDouble", param_bandmath, write_output=True
        )
        # Put in internal data
        self._edgsubmask = bandmath_app.getoutput().get("out")
        # del onebandequal_app
        LOGGER.debug("Start IPEDGSub done.")

        # *******************************************************************************************************
        # L2EDG pipeline connection
        # *******************************************************************************************************
        LOGGER.debug("Start L2EDG ...")

        l_BandsDefinitions = self._plugin.BandsDefinitions
        l_ListOfL2Resolution = l_BandsDefinitions.ListOfL2Resolution  # ListOfStrings
        # At L2 resolution
        l_NbL2Res = len(l_ListOfL2Resolution)

        res_str = l_ListOfL2Resolution[0]
        # ExpandFilterPointer => PadAndResampleImageFilter => app ressampling
        out_roi = os.path.join(working_dir, "IPEDGRealL2_{}.tif".format(res_str))
        roi_app = extract_roi(self._edgsubmask, [0], out_roi, write_output=False)
        self._l2edg_pipeline.add_otb_app(roi_app)
        out_ressampling = os.path.join(
            working_dir, "IPEDGRealL2_{}.tif".format(res_str)
        )
        resamp_app = resample(
            onebandequal_app.getoutput().get("out"),
            self._dem.ALTList[0],
            out_ressampling,
            OtbResampleType.LINEAR_WITH_RADIUS,
            threshold=0,
            padradius=4.0,
            write_output=True,
        )
        self._l2edg_pipeline.add_otb_app(resamp_app)

        self._l2edgmasklist.append(resamp_app.getoutput().get("out"))

    def convert_DC_to_toa(
        self,
        list_of_DC_image_filenames,
        calibration_info,
        converting_reflectance_coef,
        working,
    ):
        """
        :param list_of_DC_image_filenames: ListOfStrings
        :param calibration_info: ListOfDoubles
        :param converting_reflectance_coef: ListOfDoubles
        :return:
        :rtype: string
        """
        # Init the projRef
        l_ProjectionRef = ""
        # =======> GENERATE TOA CACHING
        # ---------------------------------------------------------------
        # Get the number of band with the number of TOA image files set in the input product directory
        l_NbBand = len(list_of_DC_image_filenames)  # int

        # Convert the input vrt in tif for each band
        for i,bandId in enumerate(calibration_info):
            dcFilename = str(list_of_DC_image_filenames[i])
            LOGGER.debug(
                "Caching the <%s> image filename..."
                + dcFilename
                + "with band "
                + str(i)
            )
            LOGGER.debug(
                "Calibration values (GAIN, BIAS): %s", str(calibration_info[bandId])
            )
            # initialize the TOA reader
            l_ImageFilename = os.path.join(working, "toaconverted_" + str(i) + ".tif")
            param_bandmath = {
                "il": [dcFilename],
                "exp": f"{converting_reflectance_coef[i]} * (im1b{i+1} / {calibration_info[bandId][0]}) + {calibration_info[bandId][1]}",
                "out": l_ImageFilename,
            }
            app = OtbAppHandler("BandMathDouble", param_bandmath, write_output=True)
            self._toa_scalar_list.append(app.getoutput()["out"])

        LOGGER.debug("Caching TOA images done ...")
        return l_ProjectionRef

    def generate_l1_toa_images(self, working_dir):
        """

        :return:
        """

        out_concatenate = os.path.join(working_dir, "L1TOAVector.tif")
        param_concatenate = {"il": self._toa_scalar_list, "out": out_concatenate}
        app = OtbAppHandler(
            "ConcatenateDoubleImages", param_concatenate, write_output=False
        )
        self._l1toa_pipeline.add_otb_app(app)
        self._l1toaimagelist.append(app.getoutput()["out"])

    def generate_toa_sub_images(self, working):
        dtm_coarse = self._dem.ALC

        # For each band of the input product
        for i, toa in enumerate(self._toa_scalar_list):
            # undersampling at L2CoarseResolution
            toa_sub_image = os.path.join(working, "aot_sub_{}.tif".format(i))
            resample_with_dtm(toa, dtm_coarse, toa_sub_image, no_data_src=0)
            self._toa_sub_list.append(toa_sub_image)
        # end band loop

        # *******************************************************************************************************
        # TOA Sub image pipeline connection
        # *******************************************************************************************************
        toa_sub_image = os.path.join(working, "aot_sub.tif")
        param_concatenate = {"il": self._toa_sub_list, "out": toa_sub_image}
        OtbAppHandler("ConcatenateDoubleImages", param_concatenate)
        self._sub_toa = toa_sub_image

    def generate_l2_toa_images(self, working_dir):
        """

        :return:
        """
        LOGGER.debug("PleiadesL1ImageFileReader::GenerateL2TOAmages()")

        l_BandsDefinitions = self._plugin.BandsDefinitions
        l_ListOfL2Resolution = l_BandsDefinitions.ListOfL2Resolution  # ListOfStrings
        l_NbL2Res = len(l_ListOfL2Resolution)

        # Current resolution: "R1" or "R2"
        curRes = l_ListOfL2Resolution[0]
        dtm = self._dem.ALTList[0]
        # Get the list of band of the current resolution
        listOfL2Bands = l_BandsDefinitions.get_list_of_l2_band_code(curRes)
        nbBand = len(listOfL2Bands)
        list_of_image = []
        # For each band of the current resolution
        for l_StrBandIdL2 in listOfL2Bands:
            # Get the L1 band index associated to the L2 band code
            l1BandIdx = l_BandsDefinitions.get_band_id_in_l1(l_StrBandIdL2)
            # Generate the list of L2 TOA images per resolution
            toa_l2_image = os.path.join(
                working_dir, "aot_l2_{}.tif".format(l_StrBandIdL2)
            )
            app = resample(
                self._toa_scalar_list[l1BandIdx],
                dtm,
                toa_l2_image,
                OtbResampleType.LINEAR_WITH_RADIUS,
                write_output=False,
            )
            self._toa_pipeline.add_otb_app(app)
            list_of_image.append(app.getoutput()["out"])
        out_concatenate = os.path.join(
            working_dir, "L2TOAImageListVector_" + curRes + ".tif"
        )
        param_concatenate = {"il": list_of_image, "out": out_concatenate}
        concat_app = OtbAppHandler(
            "ConcatenateDoubleImages", param_concatenate, write_output=False
        )
        self._toa_pipeline.add_otb_app(concat_app)
        self._l2toaimagelist.append(concat_app.getoutput().get("out"))

    def generate_sol1_image(self, sol_h1, working_dir):
        LOGGER.debug("PleiadesL1ImageFileReader::GenerateSOL1Image()")
        const_pi_18 = 0.01745329251994329577
        # SOL1 image pipeline connection
        # *********************************************************************************************************
        sol_filename = os.path.join(working_dir, "sol1.tif")
        mean_solar = self._header_handler.get_mean_solar_angles()

        l_sol1 = (
            sol_h1
            * math.tan(const_pi_18 * mean_solar[0])
            * math.sin(const_pi_18 * mean_solar[1])
        )
        l_sol2 = (
            sol_h1
            * math.tan(const_pi_18 * mean_solar[0])
            * math.cos(const_pi_18 * mean_solar[1])
        )
        LOGGER.debug("SOL1 parameters : ")
        LOGGER.debug(mean_solar)
        LOGGER.debug(l_sol1)
        LOGGER.debug(l_sol2)
        cla_app_1 = constant_image(
            self._dem.ALC, l_sol1, sol_filename, write_output=False
        )
        cla_app_2 = constant_image(
            self._dem.ALC, l_sol2, sol_filename, write_output=False
        )
        out_concatenate = os.path.join(working_dir, "sol1_concat.tif")
        param_concatenate = {
            "il": [cla_app_1.getoutput().get("out"), cla_app_2.getoutput().get("out")],
            "out": out_concatenate,
        }
        OtbAppHandler("ConcatenateDoubleImages", param_concatenate)
        self._sol1image = out_concatenate

    def generate_vie_image(self, view_href, working_dir):
        # *********************************************************************************************************
        # VIE image pipeline connection
        # *********************************************************************************************************
        sol_filename = os.path.join(working_dir, "cla_constant_tmp.tif")
        # azimuth and sun elevation
        mean_view = self._header_handler.get_mean_viewing_angles()

        l_vie1 = view_href * math.tan(mean_view[0]) * math.sin(mean_view[1])
        l_vie2 = view_href * math.tan(mean_view[0]) * math.cos(mean_view[1])
        vie_app_1 = constant_image(
            self._dem.ALC, l_vie1, sol_filename, write_output=False
        )
        vie_app_2 = constant_image(
            self._dem.ALC, l_vie2, sol_filename, write_output=False
        )
        out_concatenate = os.path.join(working_dir, "cla_constant.tif")
        param_concatenate = {
            "il": [vie_app_1.getoutput().get("out"), vie_app_2.getoutput().get("out")],
            "out": out_concatenate,
        }
        OtbAppHandler("ConcatenateDoubleImages", param_concatenate)
        l_BandsDefinitions = self._plugin.BandsDefinitions
        self._vieimagelist.append(out_concatenate)

    def read(self, product_info, app_handler, l2comm, dem, pReadL1Mode):
        LOGGER.info("Start Pleiades L1 ImageFileReader ...")
        self._plugin.initialize(app_handler)
        self._GIPPL2COMMHandler = l2comm
        self._ReadL1Mode = pReadL1Mode
        self._dem = dem
        self._header_handler = product_info.HeaderHandler
        self._header_handler_name = product_info.HeaderHandlerName
        working_dir = app_handler.get_directory_manager().get_temporary_directory(
            "L1Read_", do_always_remove=True
        )
        self._GIPPL2COMMHandler = l2comm

        l_BandsDefinitions = self._plugin.BandsDefinitions  # BandsDefinitions

        vrt_file = (
            pathlib.Path(self._header_handler_name).parent.parent.parent
            / "PRODUCT_FOLDER_REECH/multiband.vrt"
        )

        if not vrt_file.is_file():
            raise MajaPluginPleiadesException(
                "Cant find multiband.vrt for PLEIADES product"
            )

        list_of_DC_image_filenames = [vrt_file, vrt_file, vrt_file, vrt_file]

        # =======> CONVERTING DC to TOA
        # Get calibration coefs
        calibration_info = product_info.calibration_info
        # compute reflectance convertion coefficients
        astronomical_unit = 149597870700
        acquisition_date = self._header_handler.get_string_value(
            "//UTC_Acquisition_Range/START"
        )
        earth_sun_distance_at_datetime = earth_distance(acquisition_date).meter
        solar_irradiance_correction_factor = (
            astronomical_unit / earth_sun_distance_at_datetime
        ) ** 2
        _, solar_zenith = self._header_handler.get_mean_solar_angles()
        solar_irradiances = xml_tools.as_float_list(l2comm.get_value("Solar_irradiances"))
        product_info.SolarIrradiances = solar_irradiances
        conv_reflectance_coefs = []
        for solar_irradiance in solar_irradiances:
            conv_reflectance_coefs.append(
                math.pi
                / (
                    math.cos(math.radians(solar_zenith))
                    * solar_irradiance_correction_factor
                    * solar_irradiance
                )
            )

        self.convert_DC_to_toa(
            list_of_DC_image_filenames,
            calibration_info,
            conv_reflectance_coefs,
            working_dir,
        )
        self.generate_toa_sub_images(working_dir)
        self.generate_l2_toa_images(working_dir)
        self.generate_l1_toa_images(working_dir)
        self.generate_edg_images(working_dir)
        sol_h1 = float(self._header_handler.get_string_value("//DTM_Information/GLOBAL_H_AVERAGE"))
        self.generate_sol1_image(sol_h1, working_dir)

        # VIE image pipeline connection
        # *********************************************************************************************************
        vie_href = (
            self._plugin.ConfigUserCamera.get_Algorithms()
            .get_GRID_Reference_Altitudes()
            .get_VIEHRef()
        )
        self.generate_vie_image(vie_href, working_dir)

        self.dict_of_vals["IPEDGSubOutput"] = self._edgsubmask
        self.dict_of_vals["IPTOASubOutput"] = self._sub_toa
        self.dict_of_vals["L2TOAImageList"] = self._l2toaimagelist
        self.dict_of_vals["L2EDGOutputList"] = self._l2edgmasklist
        self.dict_of_vals["SOL1Image"] = self._sol1image
        if len(self._vieimagelist) > 2:
            l_DTMBandCode = xml_tools.as_string_list(
                l2comm.get_value("DTMViewingDirectionBandCode")
            )[0]
            l_DTMBandIdx = self._plugin.BandsDefinitions.get_band_id_in_l2_coarse(
                l_DTMBandCode
            )
            LOGGER.info("DTMBandCode= " + l_DTMBandCode)

            self.dict_of_vals["DTMVIEImage"] = self._vieimagelist[l_DTMBandIdx]
        else:
            self.dict_of_vals["DTMVIEImage"] = self._vieimagelist[0]

        # ignored for pleiades
        self.dict_of_vals["L2SATImageList"] = None
        self.dict_of_vals["L2PIXImageList"] = None
