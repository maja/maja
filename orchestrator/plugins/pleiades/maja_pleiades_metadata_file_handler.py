import math
import os
from lxml import etree as et
import datetime
import numpy as np
from lxml import etree as ET

import orchestrator.common.xml_tools as xml_tools
from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.common.maja_exceptions import MajaDataException

LOGGER = configure_logger(__name__)

PLEIADES_HANDLER_XPATH = {
    # "ProductId": "//Dataset_Identification/DATASET_NAME",
    # "AcquisitionDate": "//UTC_Acquisition_Range/END",
    # "ProductionDates":"//Processing/Processing_Step_List/Step/UTC_DATE_TIME",
    "dataset_name": "//Dataset_Identification/DATASET_NAME",
    "Identifier": "//Dataset_Identification/IDENTIFIER",
    "Authority": "//Dataset_Identification/AUTHORITY",
    "Producer": "//Dataset_Identification/PRODUCER",
    "Project": "//Dataset_Identification/PROJECT",
    "DataAccess": "//Dataset_Identification/DATA_ACCESS",
    "AcquisitionDate": "//Product_Characteristics/ACQUISITION_DATE",
    "ProductionDate": "//Product_Characteristics/PRODUCTION_DATE",
    "Instrument": "//Product_Characteristics/INSTRUMENT",
    "SpectralContent": "//Product_Characteristics/SPECTRAL_CONTENT",
    "ProductVersion": "//Product_Characteristics/PRODUCT_VERSION",
    "ProductId": "//Product_Characteristics/PRODUCT_ID",
    "ProductionSoftware": "//Product_Characteristics/PRODUCTION_SOFTWARE",
    "Platform": "//Product_Characteristics/PLATFORM",
    "OrbitNumber": "//Product_Characteristics/ORBIT_NUMBER",
    "UTCAcquisitionRangeMean": "//UTC_Acquisition_Range/MEAN",
    "UTCAcquisitionRangeDatePrecision": "//UTC_Acquisition_Range/DATE_PRECISION",
    "ZoneGeo": "//GEOGRAPHICAL_ZONE",
    "WaterMaskFilename": "//Mask_List/Mask[Mask_Properties/NATURE='Water']/Mask_File_List/MASK_FILE",
    "HiddenSurfaceMaskFilename": "//Mask_List/Mask[Mask_Properties/NATURE='Hidden_Surface']/Mask_File_List/MASK_FILE",
    "TopographyShadowMaskFilename": "//Mask_List/Mask[Mask_Properties/NATURE='Topography_Shadow']/"
    + "Mask_File_List/MASK_FILE",
    "SunTooLowMaskFilename": "//Mask_List/Mask[Mask_Properties/NATURE='Sun_Too_Low']/Mask_File_List/MASK_FILE",
    "TangentSunMaskFilename": "//Mask_List/Mask[Mask_Properties/NATURE='Tangent_Sun']/Mask_File_List/MASK_FILE",
    "SnowMaskFilename": "//Mask_List/Mask[Mask_Properties/NATURE='Snow']/Mask_File_List/MASK_FILE",
    "EdgeMaskFilename": "//Mask_List/Mask[Mask_Properties/NATURE='Edge']/Mask_File_List/MASK_FILE",
    "AOTInterpolationMaskFilename": "//Mask_List/Mask[Mask_Properties/NATURE='AOT_Interpolation']/"
    + "Mask_File_List/MASK_FILE",
    "PrivateDirectoryFilename": "//Data_List/Data[Data_Properties/NATURE='Private_Directory']/Data_File_List/DATA_FILE",
    "JobProcessingInformationsFileFilename": "//Data_List/Data[Data_Properties/"
    + "NATURE='Job_Processing_Informations_File']/Data_File_List/DATA_FILE",
    "DEMInformationMinimum": "/JobProcessingInformations/MNT_Statistics/Min",
    "DEMInformationMaximum": "/JobProcessingInformations/MNT_Statistics/Max",
    "DEMInformationAverage": "/JobProcessingInformations/MNT_Statistics/Average",
    "DEMInformationStandardDeviation": "/JobProcessingInformations/MNT_Statistics/Standard_Deviation",
    "ProcessingFlagsAndModesValidityFlag": "//Processing_Flags_And_Modes_List/"
    + "Processing_Flags_And_Modes[Key='Validity_Flag']/Value",
    "ProcessingFlagsAndModesQualityCheck": "//Processing_Flags_And_Modes_List/"
    + "Processing_Flags_And_Modes[Key='Quality_check']/Value",
    "ProcessingFlagsAndModesProcessingMode": "//Processing_Flags_And_Modes_List/"
    + "Processing_Flags_And_Modes[Key='Processing_Mode']/Value",
    "ProcessingFlagsAndModesProcessingAdjacencyEffectsAndSlopeCorrection": "//Processing_Flags_And_Modes_List/"
    + "Processing_Flags_And_Modes[Key='Adjacency_Effects_And_Slope_Correction']/Value",
    "ProcessingFlagsAndModesFileType": "//Processing_Flags_And_Modes_List/"
    + "Processing_Flags_And_Modes[Key='File_Type']/Value",
    "ProcessingFlagsAndModesCirrusCorrection": "//Processing_Flags_And_Modes_List/"
    + "Processing_Flags_And_Modes[Key='Cirrus_Correction']/Value",
    "ProcessingFlagsAndModesCAMSProcessing": "//Processing_Flags_And_Modes_List/"
    + "Processing_Flags_And_Modes[Key='CAMS_Processing']/Value",
    "L1NoData": "//SPECIAL_VALUE[@name='nodata']",
    "Nodata_Value": "//SPECIAL_VALUE[@name='nodata']",
    "VAP_Nodata_Value": "//SPECIAL_VALUE[@name='water_vapor_content_nodata']",
    "AOT_Nodata_Value": "//SPECIAL_VALUE[@name='aerosol_optical_thickness_nodata']",
    "QuantificationValue": "//REFLECTANCE_QUANTIFICATION_VALUE",
    "AerosolOpticalThicknessQuantificationValue": "//AEROSOL_OPTICAL_THICKNESS_QUANTIFICATION_VALUE",
    "AerosolOpticalThicknessQuantificationValueAsString": "//AEROSOL_OPTICAL_THICKNESS_QUANTIFICATION_VALUE",
    "WaterVaporContentQuantificationValue": "//WATER_VAPOR_CONTENT_QUANTIFICATION_VALUE",
    "WaterVaporContentQuantificationValueAsString": "//WATER_VAPOR_CONTENT_QUANTIFICATION_VALUE",
    "Projection": "//Horizontal_Coordinate_System/HORIZONTAL_CS_NAME",
    "ProjectionType": "//Horizontal_Coordinate_System/HORIZONTAL_CS_TYPE",
    "ProjectionCode": "//Horizontal_Coordinate_System/HORIZONTAL_CS_CODE",
    "SunAngleColStep": "//Sun_Angles_Grids/Zenith/COL_STEP",
    "SunAngleRowStep": "//Sun_Angles_Grids/Zenith/ROW_STEP",
    "radiometric_offset": "//Radiometric_Offset_List",
    "center_corner_lon": "//Product_Frame/Dataset_Frame/Center/LON",
    "center_corner_lat": "//Product_Frame/Dataset_Frame/Center/LAT",
    "center_corner_col": "//Product_Frame/Dataset_Frame/Center/COL",
    "center_corner_row": "//Product_Frame/Dataset_Frame/Center/ROW",
    
    "orbit_number": "//Ephemeris/ACQUISITION_ORBIT_NUMBER",
    "nb_scenes": "//Data_Strip_Frame/Scene_Framing/NB_OF_SCENES",
    "azimuth_incidences": "//Geometric_Header_List/Located_Geometric_Header//ORIENTATION",
    "global_incidences": "//Geometric_Header_List/Located_Geometric_Header//Incidences/GLOBAL_INCIDENCE",
    "along_track_incidences": "//Geometric_Header_List/Located_Geometric_Header//Incidences/ALONG_TRACK_INCIDENCE",
    "ortho_track_incidences": "//Geometric_Header_List/Located_Geometric_Header//Incidences/ORTHO_TRACK_INCIDENCE",
    "sun_azimuth_angles": "//Geometric_Header_List/Located_Geometric_Header//Solar_Incidences/SUN_AZIMUTH[@unit='deg']",
    "sun_elevations": "//Geometric_Header_List/Located_Geometric_Header//Solar_Incidences/SUN_ELEVATION[@unit='deg']",
    "spectral_band_info": "//Image_Interpretation/Spectral_Band_Info",
}


class PleiadesXMLFileHandler(object):

    def __init__(self, main_xml_file, validate=False, schema_path=None):
        LOGGER.info("Init PleiadesXMLFileHandler ")
        LOGGER.info("Loading " + main_xml_file)
        self.main_xml_file = main_xml_file
        self.root = xml_tools.get_root_xml(self.main_xml_file, deannotate=True)
        self.orig_root = xml_tools.get_root_xml(self.main_xml_file, deannotate=False)
        self.nss = self.root.nsmap
        
    def get_production_date(self):
        dates=xml_tools.get_string_list_from_nodes(
                self.root, PLEIADES_HANDLER_XPATH["ProductionDates"]
            )
        return dates[-1]
    def get_string_value_of(self, key):
        return xml_tools.get_only_value(
            self.root, PLEIADES_HANDLER_XPATH[key], namespaces=self.nss
        ).text

    def get_string_value(self, xpath):
        return xml_tools.get_only_value(self.root, xpath, namespaces=self.nss).text
    
    def set_value_of(self, key, value):
        xml_tools.set_value(self.root, PLEIADES_HANDLER_XPATH[key], value)

    def get_product_date(self):
        if (
            xml_tools.get_only_value(
                self.root, PLEIADES_HANDLER_XPATH["dataset_name"], check=True
            )
            is not None
        ):
            dataset = xml_tools.get_xml_string_value(
                    self.root, PLEIADES_HANDLER_XPATH["dataset_name"])
            element = dataset.split("_")
            date = element[2]
            LOGGER.debug("GetValidityStartDate: " + date)
            # quel est ce format de date
            l_tm = datetime.datetime.strptime(date[:-1], "%Y%m%d%H%M%S")
            return l_tm
        else:
            return None

    def get_platform_name(self):
        dataset = xml_tools.get_xml_string_value(
            self.root, PLEIADES_HANDLER_XPATH["dataset_name"])
        plat = dataset.split("_")[1]
        if plat == "PHR1A":
            return "PLEIADES1A"
        else:
            return "PLEIADES1B"

    def get_site_name(self):
        dataset = xml_tools.get_xml_string_value(
            self.root, PLEIADES_HANDLER_XPATH["dataset_name"])
        return dataset.split("_")[5]

    def get_instrument(self):
        # High Resolution Instrument
        return "HRI"

    def get_orbit_number(self):
        return xml_tools.get_xml_string_value(
            self.root, PLEIADES_HANDLER_XPATH["orbit_number"])

    def get_scenes(self):
        if (
            xml_tools.get_only_value(
                self.root, PLEIADES_HANDLER_XPATH["nb_scenes"], check=True
            )
            is not None
        ):
            return xml_tools.get_xml_float_value(
                    self.root, PLEIADES_HANDLER_XPATH["nb_scenes"])

    # Get the mean solar angles
    def get_mean_solar_angles(self):
        if (
            xml_tools.get_float_list_from_nodes(
                self.root, PLEIADES_HANDLER_XPATH["sun_elevations"]
            )
            is not None
        ):
            # convert sun elevation to zenith angle (complement angle)
            zenith_angle = 90 -xml_tools.get_float_list_from_nodes(
                    self.root, PLEIADES_HANDLER_XPATH["sun_elevations"])[1]
            return (
                xml_tools.get_float_list_from_nodes(
                    self.root, PLEIADES_HANDLER_XPATH["sun_azimuth_angles"]
                )[1],
                zenith_angle,
            )
        else:
            return 0, 0
    
    # Get the mean viewing angles
    def get_mean_viewing_angles(self):
        if (
            xml_tools.get_float_list_from_nodes(
                self.root, PLEIADES_HANDLER_XPATH["global_incidences"]
            )
            is not None
        ):
            view_zenith_angle = xml_tools.get_float_list_from_nodes(
                    self.root, PLEIADES_HANDLER_XPATH["global_incidences"]
                )[1]
            along_track_incidence = xml_tools.get_float_list_from_nodes(
                    self.root, PLEIADES_HANDLER_XPATH["along_track_incidences"])[1]
            ortho_track_incidence = xml_tools.get_float_list_from_nodes(
                    self.root, PLEIADES_HANDLER_XPATH["ortho_track_incidences"])[1]
            azimuth_incidence = xml_tools.get_float_list_from_nodes(
                    self.root, PLEIADES_HANDLER_XPATH["azimuth_incidences"]) [1]
            # idx_min=0
            # idx_max=0
            # min_azimuth = along_track_incidences[idx_min]
            # max_azimuth = along_track_incidences[idx_max]
            # for idx, along_track_incidence in enumerate(along_track_incidences):
            #     if min_azimuth < along_track_incidence:
            #         min_azimuth = along_track_incidence
            #         idx_min = idx
            #     if max_azimuth > along_track_incidence:
            #         max_azimuth = along_track_incidence
            #         idx_max = idx
            # center_idx = 1
            # for idx, along_track_incidence in enumerate(along_track_incidences):
            #     if min_azimuth < along_track_incidence < max_azimuth or min_azimuth > along_track_incidence > max_azimuth:
            #         center_idx = idx

            view_azimuth_angle = np.mod(azimuth_incidence-np.degrees(np.arctan2(ortho_track_incidence,along_track_incidence)),360)
            return (
                view_azimuth_angle,
                view_zenith_angle,
            )
        else:
            return 0, 0

    def get_l1_cld_filename(self, resol):
        return None

    # todo: delete
    # # Get the mean solar angles
    # def get_mean_solar_angles(self):
    #     if (
    #         xml_tools.get_only_value(
    #             self.root, PLEIADES_HANDLER_XPATH["sun_elevation"], check=True
    #         )
    #         is not None
    #     ):
    #         # convert sun elevation to zenith angle (complement angle)
    #         zenith_angle = 90 -xml_tools.get_xml_float_value(
    #                 self.root, PLEIADES_HANDLER_XPATH["sun_elevation"])
    #         return (
    #             xml_tools.get_xml_float_value(
    #                 self.root, PLEIADES_HANDLER_XPATH["sun_azimuth_angle"]
    #             ),
    #             zenith_angle,
    #         )
    #     else:
    #         return 0, 0

    # def get_mean_viewing_angles2(self):
    #     if (
    #         xml_tools.get_xml_float_value(
    #             self.root, PLEIADES_HANDLER_XPATH["global_incidence"]
    #         )
    #         is not None
    #     ):
    #         global_incidence = xml_tools.get_xml_float_value(
    #                 self.root, PLEIADES_HANDLER_XPATH["global_incidence"],check=True
    #             )
    #         view_zenith_angle = global_incidence
    #         along_track_incidence = xml_tools.get_xml_float_value(
    #                 self.root, PLEIADES_HANDLER_XPATH["along_track_incidence"])
    #         ortho_track_incidence = xml_tools.get_xml_float_value(
    #                 self.root, PLEIADES_HANDLER_XPATH["ortho_track_incidence"])
    #         azimuth_incidence = xml_tools.get_xml_float_value(
    #                 self.root, PLEIADES_HANDLER_XPATH["azimuth_incidence"])
    #         view_azimuth_angle = np.mod(azimuth_incidence-np.degrees(np.arctan2(ortho_track_incidence,along_track_incidence)),360)

    #         return (
    #             view_azimuth_angle,
    #             view_zenith_angle,
    #         )
    #     else:
    #         return 0, 0

    def get_calibration_values(self):
        band_index = xml_tools.get_string_list_from_nodes(
            self.root,
            PLEIADES_HANDLER_XPATH["spectral_band_info"]+"/BAND_ID"
        )
        gain_list = xml_tools.get_float_list_from_nodes(
            self.root,
            PLEIADES_HANDLER_XPATH["spectral_band_info"]+"/PHYSICAL_GAIN",
        )
        bias_list = xml_tools.get_float_list_from_nodes(
            self.root,
            PLEIADES_HANDLER_XPATH["spectral_band_info"]+"/PHYSICAL_BIAS",
        )
        if len(band_index) != len(gain_list) or len(band_index) != len(bias_list):
            raise RuntimeError("Please verify consistency of the spectral info band metadata")
        else:
            return dict(zip(band_index, zip(gain_list,bias_list)))

    def set_string_value(self, xpath, value, check_if_present=True):
        xml_tools.set_value(self.root, xpath, value, check=check_if_present)

    def remove_node(self, xpath):
        xml_tools.remove_node(self.root, xpath)

    def write_list_of_gipp_files(
        self,
        listofgipps,
        write_nature_node=True,
        p_xpath="//List_of_GIPP_Files",
        p_subxpath="GIPP_File",
    ):
        """
        <List_of_GIPP_Files count="11">
        <GIPP_File sn="1">
        <Nature>GIP_CKEXTL</Nature>
        <Logical_Name>VE_TEST_GIP_CKEXTL_S_CAMARGUE_00001_00000000_99999999</Logical_Name>
        </GIPP_File>

        """
        try:
            xnode = xml_tools.get_only_value(self.root, p_xpath)
        except BaseException:
            raise MajaDataException(
                "Error while reading the xml node '"
                + p_xpath
                + "' in the xml file! Details: "
            )

        l_count = len(listofgipps)
        xnode.set("count", str(l_count))
        for i in range(l_count):
            f = listofgipps[i]
            node = ET.Element(p_subxpath)
            node.set("sn", str(i + 1))
            if write_nature_node:
                # Nature
                node2 = ET.Element("Nature")
                node2.text = f.Nature
                node.append(node2)
            # Logical_Name
            node3 = ET.Element("Logical_Name")
            node3.text = os.path.basename(f.LogicalName)
            node.append(node3)
            xnode.append(node)

    def write_list_of_cams_files(
        self, listofcams, p_xpath="//List_of_CAMS_Files", p_subxpath="CAMS_File"
    ):
        """
        Append the list of cams files to the output file
        :param listofcams: The list of cams files
        :param p_xpath: The path to the node to modify
        :param p_subxpath: The name of the subnode to write to
        """
        try:
            xnode = xml_tools.get_only_value(self.root, p_xpath)
        except BaseException:
            raise MajaDataException(
                "Error while reading the xml node '"
                + p_xpath
                + "' in the xml file! Details: "
            )

        l_count = len(listofcams)
        xnode.set("count", str(l_count))
        for i in range(l_count):
            f = listofcams[i]
            node = ET.Element(p_subxpath)
            node.set("sn", str(i + 1))
            # Nature
            node2 = ET.Element("Nature")
            node2.text = "MAJA CAMS"
            node.append(node2)
            # Logical_Name
            node3 = ET.Element("Logical_Name")
            node3.text = os.path.basename(f.LogicalName)
            node.append(node3)
            xnode.append(node)

    def write_list_of_era5_files(
        self, listofera5, p_xpath="//List_of_ERA5_Files", p_subxpath="ERA5_File"
    ):
        """
        Append the list of cams files to the output file
        :param listofera5: The list of cams files
        :param p_xpath: The path to the node to modify
        :param p_subxpath: The name of the subnode to write to
        """
        try:
            xnode = xml_tools.get_only_value(self.root, p_xpath)
        except BaseException:
            raise MajaDataException(
                "Error while reading the xml node '"
                + p_xpath
                + "' in the xml file! Details: "
            )

        l_count = len(listofera5)
        xnode.set("count", str(l_count))
        for i in range(l_count):
            f = listofera5[i]
            node = ET.Element(p_subxpath)
            node.set("sn", str(i + 1))
            # Nature
            node2 = ET.Element("Nature")
            node2.text = "MAJA ERA5"
            node.append(node2)
            # Logical_Name
            node3 = ET.Element("Logical_Name")
            node3.text = os.path.basename(f.LogicalName)
            node.append(node3)
            xnode.append(node)

    def write_list_of_meteo_files(
        self, listofmetfiles, p_xpath="//List_of_METEO_Files", p_subxpath="METEO_File"
    ):
        """
        Append the list of meteo files to the output file
        :param listofmetfiles: The list of meteo files
        :param p_xpath: The path to the node to modify
        :param p_subxpath: The name of the subnode to write to
        """
        try:
            xnode = xml_tools.get_only_value(self.root, p_xpath)
        except BaseException:
            raise MajaDataException(
                "Error while reading the xml node '"
                + p_xpath
                + "' in the xml file! Details: "
            )

        l_count = len(listofmetfiles)
        xnode.set("count", str(l_count))
        for i in range(l_count):
            f = listofmetfiles[i]
            node = ET.Element(p_subxpath)
            node.set("sn", str(i + 1))
            # Nature
            node2 = ET.Element("Nature")
            node2.text = "MAJA METEO"
            node.append(node2)
            # Logical_Name
            node3 = ET.Element("Logical_Name")
            node3.text = os.path.basename(f.LogicalName)
            node.append(node3)
            xnode.append(node)

    # Writing parts
    def save_to_file(self, output_filename):
        # xml_tools.save_to_xml_file(self.root, output_filename)
        xml_tools.save_to_xml_file(self.root, output_filename)
        os.system(
            'XMLLINT_INDENT="    "  xmllint --format '
            + str(output_filename)
            + " --output "
            + str(output_filename)
        )

    def get_useful_image_geo_coverage_center_corner_lon(self):
        return xml_tools.get_xml_float_value(
                    self.root, PLEIADES_HANDLER_XPATH["center_corner_lon"])

    def get_useful_image_geo_coverage_center_corner_lat(self):
        return xml_tools.get_xml_float_value(
                    self.root, PLEIADES_HANDLER_XPATH["center_corner_lat"])

    def get_useful_image_geo_coverage_center_corner_col(self):
        return xml_tools.get_xml_float_value(
                    self.root, PLEIADES_HANDLER_XPATH["center_corner_col"])

    def get_useful_image_geo_coverage_center_corner_row(self):
        return xml_tools.get_xml_float_value(
                    self.root, PLEIADES_HANDLER_XPATH["center_corner_row"])
