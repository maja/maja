# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################
#
#                        o     o
#                        oo   oo   oo        o   oo        ,-.
#                        o o o o  o  o       o  o  o       \_/
#                        o  o  o o    o      o o    o     {|||)<
#                        o     o oooooo      o oooooo      / \
#                        o     o o    o o    o o    o      `-^
#                        o     o o    o  oooo  o    o
#
###################################################################################################

orchestrator.plugins.maja_dataset -- shortdesc

orchestrator.plugins.maja_dataset is a description

It defines classes_and_methods


###################################################################################################
"""
import orchestrator.common.date_utils as date_utils
import orchestrator.common.file_utils as file_utils
from orchestrator.common.muscate.muscate_xml_file_handler import MuscateXMLFileHandler
from orchestrator.common.muscate.muscate_uii_xml_file_handler import (
    MuscateUIIXMLFileHandler,
)
from orchestrator.plugins.common.base.maja_l1_image_info_base import (
    L1ImageInformationsBase,
)
from orchestrator.plugins.pleiades.maja_pleiades_plugin import MajaPleiadesPlugin
from orchestrator.plugins.pleiades.maja_pleiades_metadata_file_handler import PleiadesXMLFileHandler

import orchestrator.common.xml_tools as xml_tools
from orchestrator.common.logger.maja_logging import configure_logger

import pathlib
import datetime
LOGGER = configure_logger(__name__)


class MajaPleiadesL1ImageInformations(L1ImageInformationsBase):
    def __init__(self):
        super(MajaPleiadesL1ImageInformations, self).__init__()
        self._plugin = MajaPleiadesPlugin()
        self.UniqueSatellite = self._plugin.UniqueSatellite
        self.IS_TILE_DEPENDENT = False

    def detect_l1_products(self, input_directory, product_list):
        LOGGER.info("Start Pleiades L1 DetectL1Products in " + input_directory)
        input_dir_path = pathlib.Path(input_directory)
        pleiades_dir_list = list(input_dir_path.glob("*PHR*PX*"))
        if len(pleiades_dir_list) == 0:
            return
        for pleiades_dir in pleiades_dir_list:
            LOGGER.debug("l_ShortDirectoryFilename: " + pleiades_dir.name)
            mtd_file = pleiades_dir / "METADATA/METADATA_SIGMA/ipu_metadata_sigma.xml"
            vrt_file = pleiades_dir / "PRODUCT_FOLDER_REECH/multiband.vrt"
            if mtd_file.is_file() and vrt_file.is_file():
                product_list.append(str(mtd_file))
                LOGGER.debug("Add the Pleiades L1 product " + str(mtd_file) + " in the list of products!" )


    def initialize(self, product_filename, validate=False, schema_path=None):
        LOGGER.info("Start Pleiades L1 Initialize on product " + product_filename)

        #TODO load info from sigma mtd file
        # info from DS_PHR1A_201707170658211_FR1_PX_E050S15_0401_02690 (Toulouse site) meanwhile
        self.PluginName = "PLEIADES"
        self.LevelType = "1"
        self.ProductVersion = "1"


        self.HeaderHandler = PleiadesXMLFileHandler(product_filename)
        self.HeaderHandlerName = product_filename
        self.Satellite = self.HeaderHandler.get_platform_name()
        self.SatelliteID = self.Satellite.upper().replace("-", "")
        self.OrbitNumber = self.HeaderHandler.get_orbit_number()
        self.Instrument = self.HeaderHandler.get_instrument()
        self.ProductDate = self.HeaderHandler.get_product_date()
        self.Site = self.HeaderHandler.get_site_name()
        self.GenerationDateStr = datetime.datetime.strftime(
            self.ProductDate, "%Y-%m-%dT%H:%M:%S"
        )
        self.UTCValidityStart = date_utils.get_utc_from_datetime(self.ProductDate)
        self.UTCValidityStop = date_utils.get_utc_from_datetime(self.ProductDate)
        self.HeaderFilename = product_filename
        bandDefinition = self._plugin.BandsDefinitions
        l_NbBandsL2Coarse = len(bandDefinition.L2CoarseBandMap)
        l_ListOfBandsL2Coarse = bandDefinition.get_list_of_band_id_in_l2_coarse()
        # TODO PWH metadata value missing ?
        self.ReflectanceQuantification = 1.0/10000.0

        nb_scenes = self.HeaderHandler.get_scenes()
        # Solar Angles
        lSolarAngles = self.HeaderHandler.get_mean_solar_angles()
        self.SolarAngle = {
            "sun_azimuth_angle": lSolarAngles[0],
            "sun_zenith_angle": lSolarAngles[1],
        }
        lViewAngles = self.HeaderHandler.get_mean_viewing_angles()
        self.ViewingAngle = {
            "incidence_azimuth_angle": str(lViewAngles[0]),
            "incidence_zenith_angle": str(lViewAngles[1]),
        }
        # For each bands for EnviProduct
        self.ListOfViewingAzimuthAnglesPerBandAtL2CoarseResolution=[]
        self.ListOfViewingZenithAnglesPerBandAtL2CoarseResolution=[]
        for bd in range(0, len(l_ListOfBandsL2Coarse)):
            self.ListOfViewingAzimuthAnglesPerBandAtL2CoarseResolution = [str(lViewAngles[0])]*4
            self.ListOfViewingZenithAnglesPerBandAtL2CoarseResolution = [str(lViewAngles[1])]*4
        
        # Estimation of the coordinate of the central point
        self.CenterCorner.longitude = (
            self.HeaderHandler.get_useful_image_geo_coverage_center_corner_lon()
        )
        self.CenterCorner.latitude = (
            self.HeaderHandler.get_useful_image_geo_coverage_center_corner_lat()
        )
        self.CenterCorner.column = (
            self.HeaderHandler.get_useful_image_geo_coverage_center_corner_col()
        )
        self.CenterCorner.line = (
            self.HeaderHandler.get_useful_image_geo_coverage_center_corner_row()
        )

        # For each bands for EnviProduct
        for j in range(l_NbBandsL2Coarse):
            angles = {
                "incidence_zenith_angle": str(lViewAngles[1]),
                "incidence_azimuth_angle": str(lViewAngles[0]),
            }
            self.ListOfViewingAnglesPerBandAtL2CoarseResolution.append(angles)

        self.calibration_info = self.HeaderHandler.get_calibration_values()

        return True
