# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     {|||)<
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################

orchestrator.plugins.sentinel2.maja_sentinel2_l1_plugin -- shortdesc

orchestrator.plugins.sentinel2.maja_sentinel2_l1_plugin is a description

It defines classes_and_methods

###################################################################################################
"""

from orchestrator.plugins.common.base.maja_plugin_base import PluginBase
from orchestrator.plugins.common.base.bands_definition import SingleResolutionImageInfo
from orchestrator.common.constants import *


class MajaPleiadesPlugin(PluginBase):
    """
    classdocs
    """

    def __init__(self):
        super(MajaPleiadesPlugin, self).__init__()
        self.PluginName = "PLEIADES"
        self.UniqueSatellite = "PLEIADES"
        self.ListOfSatellites.append("PLEIADES1A")
        self.ListOfSatellites.append("PLEIADES1B")

        self.ShortFileType = "HRI"
        self.Instrument = "HRI"

        # Resolution/bands fill
        self.ListOfL1Resolutions.append("XS")
        self.ListOfL2Resolutions.append("XS")
        self.L1BandCharIdList = []
        # -----------------------------------------------------------------
        # Sorted list of band char id used in the  tile PDI_ID
        # Never used
        self.L1BandCharIdList.append("B1")
        self.L1BandCharIdList.append("B2")
        self.L1BandCharIdList.append("B3")
        self.L1BandCharIdList.append("B4")
        # General camera configuration
        self.WebSiteURL = ""
        self.ReferenceDate = "20000101"
        self.Creator_Version = "04.00.00"
        self.WideFieldSensor = False
        self.GRIDReferenceAltitudesSOL2GridAvailable = False
        self.WaterVapourDetermination = False
        self.CloudDetermination = False
        self.CirrusFlag = False
        self.CirrusMasking = False
        self.SnowMasking = False
        self.WaterMasking = False
        self.RainDetection = False
        self.HasSwirBand = False
        self.DirectionalCorrection = False
        self.CloudMaskingKnownCloudsAltitude = False
        self.DFPMasking = False
        self.CompositeAlgorithm = False


        # Fill BandsDefinition
        # ---------------------------------------------------------------------------------------------
        # Init the list of L2 resolution
        self.BandsDefinitions.ListOfL2Resolution.append("XS")
        # ---------------------------------------------------------------------------------------------
        # Init the list of L1 resolution
        self.BandsDefinitions.ListOfL1Resolution.append("XS")
        # ---------------------------------------------------------------------------------------------
        # List of indices for the L1 bands
        self.BandsDefinitions.L2CoarseBandMap["B0"] = 0
        self.BandsDefinitions.L2CoarseBandMap["B1"] = 1
        self.BandsDefinitions.L2CoarseBandMap["B2"] = 2
        self.BandsDefinitions.L2CoarseBandMap["B3"] = 3

        # Init the list of L2 coarse resolution bands

        # ---------------------------------------------------------------------------------------------
        # ---------------------------------------------------------------------------------------------
        # Definition of the 3 resolutions
        l_XSImageInfo = SingleResolutionImageInfo()
        l_XSImageInfo.Resolution = 2

        # ---------------------------------------------------------------------------------------------
        l_XSImageInfo.ListOfBandCode.append("B0")
        l_XSImageInfo.ListOfBandCode.append("B1")
        l_XSImageInfo.ListOfBandCode.append("B2")
        l_XSImageInfo.ListOfBandCode.append("B3")
        self.BandsDefinitions.ImagesInformationMap[
            self.BandsDefinitions.ListOfL1Resolution[0]
        ] = l_XSImageInfo

        # ---------------------------------------------------------------------------------------------
        # L1
        # As for the list of indices, the list of L1 bands by resolution is equal to the list of L2 bands
        self.BandsDefinitions.L1BandMap = self.BandsDefinitions.L2CoarseBandMap
        # As for the list of indices, the list of L1 bands by resolution is equal to the list of L2 bands
        self.BandsDefinitions.L1ListOfBandsMap[
            self.BandsDefinitions.ListOfL1Resolution[0]
        ] = self.BandsDefinitions.ImagesInformationMap[
            self.BandsDefinitions.ListOfL1Resolution[0]
        ].ListOfBandCode


        # TODO
        self.TEMPLATE_TEC_PRIVATE_EEF = (
            "PLEIADES/Templates/L2.product/TEC_PRIVATE.EEF"
        )

        # Public Templates
        self.TEMPLATE_PDTQKL_JPG = (
            "PLEIADES/Templates/L2.product/DEFAULT_QKL_ALL.jpg"
        )

        # GLOBAL
        self.TEMPLATE_GLOBAL_HDR = "PLEIADES/Templates/L2.product/MTD_ALL.xml"
        self.MAJA_INSTALL_SCHEMAS_DIR = "PLEIADES"
        self.MAJA_INSTALL_SCHEMAS_L2_DIR = "PLEIADES/PSC-SL-411-0032-CG_Schemas"

        # -----------------------------------------------------------------------------
        # MACCS VENUS File_Version
        self.L2PRODUCT_FILE_VERSION = "0001"
        self.L3PRODUCT_FILE_VERSION = "0001"
        # Quicklook product (QCK)
        self.L1QCK_FILE_VERSION = "0001"
        self.L2QCK_FILE_VERSION = "0001"
        self.L3QCK_FILE_VERSION = "0001"

