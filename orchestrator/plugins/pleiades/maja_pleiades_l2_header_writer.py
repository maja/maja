# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################
#
#                        o     o
#                        oo   oo   oo        o   oo        ,-.
#                        o o o o  o  o       o  o  o       \_/
#                        o  o  o o    o      o o    o     {|||)<
#                        o     o oooooo      o oooooo      / \
#                        o     o o    o o    o o    o      `-^
#                        o     o o    o  oooo  o    o
#
###################################################################################################

orchestrator.plugins.maja_dataset -- shortdesc

orchestrator.plugins.maja_dataset is a description

It defines classes_and_methods

###################################################################################################
"""
from orchestrator.plugins.common.base.maja_l2_header_writer_base import (
    L2HeaderWriterBase,
)
from orchestrator.plugins.pleiades.maja_pleiades_plugin import MajaPleiadesPlugin
from orchestrator.plugins.common.base.maja_l2_image_filenames_provider import (
    L2ImageFilenamesProvider,
)

from orchestrator.common import gipp_utils
import orchestrator.common.file_utils as file_utils
import orchestrator.common.date_utils as date_utils
import orchestrator.common.xml_tools as xml_tools
from orchestrator.plugins.pleiades.maja_pleiades_metadata_file_handler import PleiadesXMLFileHandler
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler
from orchestrator.common.maja_common import PointXY
from orchestrator.common.maja_exceptions import MajaPluginPleiadesException
from orchestrator.common.earth_explorer.earth_explorer_utilities import (
    EarthExplorerUtilities,
)
from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.common import version

import os, datetime
from lxml import etree as ET
import math
import re


LOGGER = configure_logger(__name__)


class MajaPleiadesL2HeaderWriter(L2HeaderWriterBase):
    def __init__(self):
        super(MajaPleiadesL2HeaderWriter, self).__init__()
        self.plugin = MajaPleiadesPlugin()

    def create(self, plugin_name, app_handler):
        if plugin_name == "PLEIADES":
            self.apphandler = app_handler
            return True
        else:
            return False

    def write(self):
        if self.plugin is None:
            raise MajaPluginPleiadesException(
                "Internal error: the variable self.plugin is NULL!"
            )
        if self.l1_plugin is None:
            self.l1_plugin = self.plugin
        if self.l1imageinformationsproviderbase is None:
            raise MajaPluginPleiadesException(
                "Internal error: the variable m_L1ImageInformationsProviderBase is NULL!"
            )
        extension_file = ".tif"
        if(self.plugin.ConfigUserCamera.get_Business().get_OutputFormatLUMPlugin()):
            extension_file = ".lum"

            # 4.3
        self.pre_processing()

        if self.apphandler.ChangeTargetResToR3:
            self.OutResR3 = True
        self.OutResR3 = self.plugin.ConfigUserCamera.get_Business().get_OutResR3()

        # ---------------------------------------------------------------------------------------------
        # Get the "share/config" directory

        root_template_directory = self.apphandler.get_share_config_directory()
        LOGGER.debug(
            "Root template install directory '" + root_template_directory + "'"
        )
        root_shemas_directory = self.apphandler.get_schemas_root_install_dir()
        LOGGER.debug("Root shemas install directory '" + root_shemas_directory + "'")
        lSchemaLocationDirectory = (
                root_shemas_directory + self.plugin.MAJA_INSTALL_SCHEMAS_L2_DIR
        )
        LOGGER.debug("Root install directory '" + lSchemaLocationDirectory + "'")
        # Reference of the L2 output product filename (coe from the L2IOmageFielWriter
        l_ReferenceOutputL2ProductFilename = self.outputl2productfilename
        LOGGER.debug(
            "ReferenceOutputL2ProductFilename: '"
            + l_ReferenceOutputL2ProductFilename
            + "'"
        )
        l_PublicDirectory = os.path.abspath(
            os.path.join(l_ReferenceOutputL2ProductFilename, os.pardir)
        )
        LOGGER.debug("l_PublicDirectory: '" + l_PublicDirectory + "'")
        # ---------------------------------------------------------------------------------------------
        LOGGER.debug("Start PleiadesL2HeaderFileWriterProvider::Write() ...")

        LOGGER.debug("XML Header called: ")
        LOGGER.debug(self.plugin)
        LOGGER.debug(self.l1imageinformationsproviderbase.HeaderFilename)
        LOGGER.debug(self.plugin.PluginName)
        # ---------------------------------------------------------------------------------------------
        lCurrentDate = datetime.datetime.now()
        l_Mission = self.l1imageinformationsproviderbase.SatelliteID
        LOGGER.debug("l_Mission: " + l_Mission)
        # Try to load the necessary information before
        # Load the L1 header product test if pleiades or EarthExplorer
        if self.l1imageinformationsproviderbase.PluginName != self.plugin.PluginName:
            self.l2imagefilenamesprovider = L2ImageFilenamesProvider()
            self.l2imagefilenamesprovider.initialize(
                self.l1imageinformationsproviderbase,
                self.plugin.ListOfL2Resolutions,
                self.outputdirectory,
                False,
            )  # Check existence

        # Start write the global header file

        root_sc_xml_templates = (
                root_template_directory + self.plugin.TEMPLATE_GLOBAL_HDR
        )
        self.write_global_xml_handler(
            root_sc_xml_templates,
            l_ReferenceOutputL2ProductFilename,
            lSchemaLocationDirectory,
            l_Mission,
            extension_file,
        )

    def write_global_xml_handler(
            self,
            root_sc_xml_templates,
            output_filename,
            schemaLocationDirectory,
            p_Mission,
            extension_file
    ):
        # ---------------------------------------------------------------------------------------------
        l_CurrentPluginBase = self.plugin
        # Get global information about the output L2 product

        l_BandsDefinitions = l_CurrentPluginBase.BandsDefinitions
        l_ListOfL2Res = l_BandsDefinitions.ListOfL2Resolution
        l_NumberOfResolutions = len(l_ListOfL2Res)
        l_l1info = self.l1imageinformationsproviderbase

        list_bands_to_write = (
            self.plugin.ConfigUserCamera.get_Business().get_WriteBands()
        )

        # ---------------------------------------------------------------------------------------------------
        LOGGER.debug("Write the GLOBAL header file ...")

        current_header_filename = root_sc_xml_templates
        if not os.path.exists(current_header_filename):
            raise MajaPluginPleiadesException(
                "Internal error: the template file '"
                + current_header_filename
                + "' doesn't exist !!"
            )
        # Load the file
        output_handler = PleiadesXMLFileHandler(current_header_filename)
        LOGGER.debug("output_filename: " + output_filename)
        # Set the main header information
        l_RootBaseFilename = os.path.basename(output_filename).replace(
            "_MTD_ALL.xml", ""
        )
    
        output_handler.set_value_of("ProductId", l_RootBaseFilename)

        l_Site = l_l1info.Site.strip("_")
        l_tileMatch = re.search("^[0-9]{2}[A-Z]{3}$", l_Site)
        if l_tileMatch is not None:
            l_Site = "T" + l_Site

        # Node Dataset_Identification
        l_identifier = l_l1info.get_l2_identifier()
        output_handler.set_value_of("Identifier", l_identifier)
        if len(l_l1info.Instrument):
            output_handler.set_value_of("Instrument", l_l1info.Instrument)
        else:
            output_handler.set_value_of("Instrument", self.plugin.Instrument)

        if len(l_l1info.DataAccess):
            output_handler.set_value_of("DataAccess", l_l1info.DataAccess)

        if len(l_l1info.SpectralContent):
            output_handler.set_value_of("SpectralContent", l_l1info.SpectralContent)


        output_handler.set_value_of("Authority", "CNES")


        output_handler.set_value_of("Producer", "CNES/ADS")


        output_handler.set_value_of("Project", "PWH")

        output_handler.set_value_of("ZoneGeo", l_Site)

        # Detect the type of site
        l_siteType = "K-J/Sat"

        xml_tools.set_attribute(
            output_handler.root, "//GEOGRAPHICAL_ZONE", "type", l_siteType
        )

        # Node Product_Characteristics
        l_sensingTime = (
                date_utils.get_utc_from_datetime(l_l1info.ProductDate)[4:]
                + "."
                + date_utils.get_date_millisecs_from_tm(l_l1info.ProductDate)
                + "Z"
        )
        output_handler.set_value_of("AcquisitionDate", l_sensingTime)

        output_handler.set_value_of("ProductionDate", l_l1info.GenerationDateStr)

        output_handler.set_value_of("ProductVersion", l_l1info.ProductVersion)

        output_handler.set_value_of(
            "ProductionSoftware", "MAJA " + version.MAJA_VERSION
        )

        output_handler.set_value_of("Platform", p_Mission)

        output_handler.set_value_of("UTCAcquisitionRangeMean", l_sensingTime)

        output_handler.set_value_of("UTCAcquisitionRangeDatePrecision", "0.001")

        output_handler.set_value_of("OrbitNumber", l_l1info.OrbitNumber)

        # Writes the filename
        # QUICKLOOK
        xml_tools.set_attribute(
            output_handler.root,
            "//Product_Organisation/Muscate_Product/QUICKLOOK",
            "bands_id",
            self.quicklookredbandcode
            + ","
            + self.quicklookgreenbandcode
            + ","
            + self.quicklookbluebandcode,
        )

        output_handler.set_string_value(
            "//Product_Organisation/Muscate_Product/QUICKLOOK", l_RootBaseFilename + "_QKL_ALL.jpg"
        )


        #        If not FRE, remove this node
        #        Else rename and set the values
        #        Image_MACC_template_temporary_node_Flat_Reflectance
        # MACCS 4.7.2 - correction pour FA 1572
        # MACCS 4.7.4 : LAIG-FA-MAC-1623-CNES : write always PDV directory
        l_PublicDirectory = os.path.abspath(os.path.join(output_filename, os.pardir))
        LOGGER.debug("Output public directory : " + l_PublicDirectory)
        LOGGER.debug("No private directory for PLEIADES")

        # Resolution information
        l_grpSuffixes = l_ListOfL2Res
        if len(l_grpSuffixes) == 1 and not self.OutResR3:
            l_grpSuffixes = ["XS"]

        if self.writel2products:
            # Resolution information
            for resol in range(l_NumberOfResolutions):
                l_grpSuffix = l_grpSuffixes[resol]
                l_StrResolution = l_BandsDefinitions.ListOfL2Resolution[resol]
                l_ListOfBand = l_BandsDefinitions.get_list_of_l2_band_code(
                    l_StrResolution
                )
                if self.OutResR3:
                    l_grpSuffix = "R3"
                l_NumberOfBands = len(l_ListOfBand)
                if list_bands_to_write is not None:
                    for i in range(l_NumberOfBands):
                        if l_ListOfBand[i] not in list_bands_to_write:
                            output_handler.remove_node(
                                "//Band_Global_List/BAND_ID[text()='"
                                + l_ListOfBand[i]
                                + "']"
                            )
                            output_handler.remove_node(
                                "//Band_Group_List/Group[@group_id='"
                                + l_grpSuffix
                                + "']/Band_List/BAND_ID[text()='"
                                + l_ListOfBand[i]
                                + "']"
                            )
                            attr = xml_tools.get_attribute(
                                output_handler.root, "//Band_Global_List", "count"
                            )
                            xml_tools.set_attribute(
                                output_handler.root,
                                "//Band_Global_List",
                                "count",
                                str(int(attr) - 1),
                            )
                            attr = xml_tools.get_attribute(
                                output_handler.root,
                                "//Band_Group_List/Group[@group_id='"
                                + l_grpSuffix
                                + "']/Band_List",
                                "count",
                            )
                            xml_tools.set_attribute(
                                output_handler.root,
                                "//Band_Group_List/Group[@group_id='"
                                + l_grpSuffix
                                + "']/Band_List",
                                "count",
                                str(int(attr) - 1),
                            )

                for i in range(l_NumberOfBands):
                    if (
                            list_bands_to_write is None
                            or l_ListOfBand[i] in list_bands_to_write
                    ):
                        output_handler.set_string_value(
                            "//Product_Organisation/Muscate_Product/Image_List/Image/Image_File_List/IMAGE_FILE[@band_id='"
                            + l_ListOfBand[i]
                            + "']",
                            l_RootBaseFilename + "_SRE_" + l_ListOfBand[i] + extension_file,
                        )
                    else:
                        output_handler.remove_node(
                            "//Product_Organisation/Muscate_Product/Image_List/Image/Image_File_List/IMAGE_FILE[@band_id='"
                            + l_ListOfBand[i]
                            + "']"
                        )
                if self.adjacencyeffectsandslopecorrection:
                    for i in range(l_NumberOfBands):
                        if (
                                list_bands_to_write is None
                                or l_ListOfBand[i] in list_bands_to_write
                        ):
                            output_handler.set_string_value(
                                "//Image_MACC_template_temporary_node_Flat_Reflectance[Image_Properties/NATURE='Flat_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                                + l_ListOfBand[i]
                                + "']",
                                l_RootBaseFilename + "_FRE_" + l_ListOfBand[i] + extension_file,
                            )
                        else:
                            output_handler.remove_node(
                                "//Image_MACC_template_temporary_node_Flat_Reflectance[Image_Properties/NATURE='Flat_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                                + l_ListOfBand[i]
                                + "']"
                            )
                for i in range(l_NumberOfBands):
                    if (
                            list_bands_to_write is None
                            or l_ListOfBand[i] in list_bands_to_write
                    ):
                        output_handler.set_string_value(
                            "//Image[Image_Properties/NATURE='Surface_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                            + l_ListOfBand[i]
                            + "']",
                            l_RootBaseFilename + "_SRE_" + l_ListOfBand[i] + extension_file,
                        )
                output_handler.set_string_value(
                    "//Image[Image_Properties/NATURE='Water_Vapor_Content']/Image_File_List/IMAGE_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    l_RootBaseFilename + "_ATB_" + l_grpSuffix + extension_file,
                )
                output_handler.set_string_value(
                    "//Image[Image_Properties/NATURE='Aerosol_Optical_Thickness']/Image_File_List/IMAGE_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    l_RootBaseFilename + "_ATB_" + l_grpSuffix + extension_file,
                )
                # ---------------------------------------------------------------------------------------------------
                # Write WVC_Interpolation
                # Only for driver where WaterVapourDetermination is activated
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='WVC_Interpolation']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "MASKS/" + l_RootBaseFilename + "_IAB_" + l_grpSuffix + extension_file,
                )
                # ---------------------------------------------------------------------------------------------------
                # Write AOT_Interpolation
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='AOT_Interpolation']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "MASKS/" + l_RootBaseFilename + "_IAB_" + l_grpSuffix + extension_file,
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Detailed Cloud
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Detailed_Cloud']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "MASKS/" + l_RootBaseFilename + "_CLM_" + l_grpSuffix + extension_file,
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Edge
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Edge']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "MASKS/" + l_RootBaseFilename + "_EDG_" + l_grpSuffix + extension_file,
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Saturation
                for i in range(l_NumberOfBands):
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Saturation']/Mask_File_List/MASK_FILE[@band_id='"
                        + l_ListOfBand[i]
                        + "']",
                        "MASKS/" + l_RootBaseFilename + "_SAT_" + l_grpSuffix + extension_file,
                    )
                l1_handler = PleiadesXMLFileHandler(self.l1imageinformationsproviderbase.HeaderFilename)
                LOGGER.debug("L1 handler:"+str(l1_handler.get_l1_cld_filename(l_grpSuffix)))
                if l1_handler.get_l1_cld_filename("XS") is not None:
                    LOGGER.debug("MG1 mask detected")
                    # ---------------------------------------------------------------------------------------------------
                    # Write Water
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Water']/Mask_File_List/MASK_FILE[@group_id='"
                        + l_grpSuffix
                        + "']",
                        "MASKS/" + l_RootBaseFilename + "_MG1_" + l_grpSuffix + extension_file,
                    )
                    # ---------------------------------------------------------------------------------------------------
                    # Write Cloud
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Cloud']/Mask_File_List/MASK_FILE[@group_id='"
                        + l_grpSuffix
                        + "']",
                        "MASKS/" + l_RootBaseFilename + "_MG1_" + l_grpSuffix + extension_file,
                    )
                    # ---------------------------------------------------------------------------------------------------
                    # Write Snow
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Snow']/Mask_File_List/MASK_FILE[@group_id='"
                        + l_grpSuffix
                        + "']",
                        "MASKS/" + l_RootBaseFilename + "_MG1_" + l_grpSuffix + extension_file,
                    )
                else:
                    LOGGER.debug("No MG1 mask detected, remove node on header")
                    output_handler.remove_node(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Water']")
                    output_handler.remove_node(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Cloud']")
                    output_handler.remove_node(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Snow']")

                # ---------------------------------------------------------------------------------------------------
                # Write DFP
                l_DFPMaskAvailable = self.l1_plugin.DFPMasking
                if (
                        l_DFPMaskAvailable
                        and self.plugin.ConfigUserCamera.get_Business().get_WriteDFP()
                ):
                    for i in range(l_NumberOfBands):
                        output_handler.set_string_value(
                            "//Mask_List/Mask[Mask_Properties/NATURE='Defective_Pixel']/Mask_File_List/MASK_FILE[@band_id='"
                            + l_ListOfBand[i]
                            + "']",
                            "MASKS/"
                            + l_RootBaseFilename
                            + "_DFP_"
                            + l_grpSuffix
                            + extension_file,
                        )
                else:
                    output_handler.remove_node(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Defective_Pixel']"
                    )

        else:
            # Generating default 1000x1000px black quicklook
            root_template_directory = self.apphandler.get_share_config_directory()
            LOGGER.debug(
                "QCK template '"
                + root_template_directory
                + self.plugin.TEMPLATE_PDTQKL_JPG
                + "'"
            )
            LOGGER.debug(
                "QCK dst fname '"
                + l_PublicDirectory
                + "/"
                + l_RootBaseFilename
                + "_QKL_ALL.jpg'"
            )
            file_utils.copy_file(
                root_template_directory + self.plugin.TEMPLATE_PDTQKL_JPG,
                l_PublicDirectory + "/" + l_RootBaseFilename + "_QKL_ALL.jpg",
            )
            # Resolution information
            for resol in range(l_NumberOfResolutions):
                l_grpSuffix = l_grpSuffixes[resol]
                l_StrResolution = l_BandsDefinitions.ListOfL2Resolution[resol]
                l_ListOfBand = l_BandsDefinitions.get_list_of_l2_band_code(
                    l_StrResolution
                )
                l_NumberOfBands = len(l_ListOfBand)
                if self.OutResR3:
                    l_grpSuffix = "R3"
                for i in range(l_NumberOfBands):
                    output_handler.set_string_value(
                        "//Image[Image_Properties/NATURE='Surface_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                        + l_ListOfBand[i]
                        + "']",
                        "",
                    )
                if self.adjacencyeffectsandslopecorrection:
                    for i in range(l_NumberOfBands):
                        output_handler.set_string_value(
                            "//Image_MACC_template_temporary_node_Flat_Reflectance[Image_Properties/NATURE='Flat_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                            + l_ListOfBand[i]
                            + "']",
                            "",
                        )
                for i in range(l_NumberOfBands):
                    output_handler.set_string_value(
                        "//Image[Image_Properties/NATURE='Surface_Reflectance']/Image_File_List/IMAGE_FILE[@band_id='"
                        + l_ListOfBand[i]
                        + "']",
                        "",
                    )

                output_handler.set_string_value(
                    "//Image[Image_Properties/NATURE='Water_Vapor_Content']/Image_File_List/IMAGE_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                output_handler.set_string_value(
                    "//Image[Image_Properties/NATURE='Aerosol_Optical_Thickness']/Image_File_List/"
                    + "IMAGE_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # Write Cloud
                l_Nature = "Cloud"
                # Get the parent node
                l_CLDDataBandsSelected = self.plugin.CLDDataBandsSelected
                l_NbCLDDataBandsSelected = len(l_CLDDataBandsSelected)
                xnode = xml_tools.get_only_value(
                    output_handler.root,
                    "//Mask_List/Mask[Mask_Properties/NATURE='"
                    + l_Nature
                    + "']/Mask_File_List",
                )
                main_node = xnode
                for i in l_CLDDataBandsSelected:
                    l_Group = l_grpSuffix
                    elt = ET.Element("MASK_FILE")
                    elt.set("group_id", l_Group)
                    elt.text = ""
                    main_node.append(elt)

                # Write Water
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Water']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Hidden_Surface
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Hidden_Surface']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Topography_Shadow
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Topography_Shadow']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Sun_Too_Low
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Sun_Too_Low']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Tangent_Sun
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Tangent_Sun']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write Snow
                if self.l1_plugin.SnowMasking:
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='Snow']/Mask_File_List/MASK_FILE[@group_id='"
                        + l_grpSuffix
                        + "']",
                        "",
                    )
                # ---------------------------------------------------------------------------------------------------

                # Write Saturation
                l_Nature = "Saturation"
                for b in l_ListOfBand:
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='"
                        + l_Nature
                        + "']/Mask_File_List/MASK_FILE[@band_id='"
                        + b
                        + "']",
                        "",
                    )
                # ---------------------------------------------------------------------------------------------------
                # Write Edge
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='Edge']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write AOT_Interpolation
                output_handler.set_string_value(
                    "//Mask_List/Mask[Mask_Properties/NATURE='AOT_Interpolation']/Mask_File_List/MASK_FILE[@group_id='"
                    + l_grpSuffix
                    + "']",
                    "",
                )
                # ---------------------------------------------------------------------------------------------------
                # Write WVC_Interpolation
                # Only for driver where WaterVapourDetermination is activated
                if self.l1_plugin.WaterVapourDetermination:
                    output_handler.set_string_value(
                        "//Mask_List/Mask[Mask_Properties/NATURE='WVC_Interpolation']/Mask_File_List/MASK_FILE[@group_id='"
                        + l_grpSuffix
                        + "']",
                        "",
                    )
                # ---------------------------------------------------------------------------------------------

                # Fin si manage Detfoo

        if self.adjacencyeffectsandslopecorrection:
            LOGGER.debug(
                "Renamed child Product_Organisation/Muscate_Product/Image_List/Image_MACC_template_temporary_node_Flat_Reflectance by Image"
            )
            xml_tools.rename_node(
                output_handler.root,
                "//Product_Organisation/Muscate_Product/Image_List/Image_MACC_template_temporary_node_Flat_Reflectance",
                "Image",
            )
        else:
            LOGGER.debug(
                "Removed Product_Organisation/Muscate_Product/Image_List/Image_MACC_template_temporary_node_Flat_Reflectance child"
            )
            xml_tools.remove_node(
                output_handler.root,
                "//Product_Organisation/Muscate_Product/Image_List/"
                + "Image_MACC_template_temporary_node_Flat_Reflectance",
            )

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteSRE():
            output_handler.remove_node(
                "//Image[Image_Properties/NATURE='Surface_Reflectance']"
            )
        if not self.plugin.ConfigUserCamera.get_Business().get_WriteATB():
            output_handler.remove_node(
                "//Image[Image_Properties/NATURE='Aerosol_Optical_Thickness']"
            )
            output_handler.remove_node(
                "//Image[Image_Properties/NATURE='Water_Vapor_Content']"
            )

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteIAB():
            output_handler.remove_node(
                "//Mask[Mask_Properties/NATURE='WVC_Interpolation']"
            )
            output_handler.remove_node(
                "//Mask[Mask_Properties/NATURE='AOT_Interpolation']"
            )

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteEDG():
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Edge']")

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteSAT():
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Saturation']")

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteQLK():
            output_handler.remove_node("//QUICKLOOK")

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteMG2():
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Water']")
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Cloud']")
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Snow']")
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Cloud_Shadow']")
            output_handler.remove_node(
                "//Mask[Mask_Properties/NATURE='Topography_Shadow']"
            )
            output_handler.remove_node(
                "//Mask[Mask_Properties/NATURE='Hidden_Surface']"
            )
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Sun_Too_Low']")
            output_handler.remove_node("//Mask[Mask_Properties/NATURE='Tangent_Sun']")

        if not self.plugin.ConfigUserCamera.get_Business().get_WriteCLM():
            output_handler.remove_node(
                "//Mask[Mask_Properties/NATURE='Detailed_Cloud']"
            )

        if not self.writel2products:
            # DM2706
            xml_tools.remove_node(
                output_handler.root, "//Product_Organisation/Muscate_Product/Image_List"
            )
            xml_tools.remove_node(
                output_handler.root, "//Product_Organisation/Muscate_Product/Mask_List"
            )

        # ----------------------------------------------------------------------
        # Compute Geoposition_Informations
        l_Areas = self.dem.L2Areas
        proj_full = self.dem.ProjCode
        proj_table = proj_full
        proj_code = proj_full
        if proj_full.find(":") != -1:
            proj_table = proj_full[: proj_full.find(":")]
            proj_code = proj_full[proj_full.find(":") + 1:]

        output_handler.set_string_value(
            "//Geoposition_Informations/Coordinate_Reference_System/GEO_TABLES",
            proj_table,
        )
        output_handler.set_string_value(
            "//Geoposition_Informations/Coordinate_Reference_System/Horizontal_Coordinate_System/HORIZONTAL_CS_TYPE",
            self.dem.ProjType,
        )
        proj_name = self.dem.ProjRef.split('"')[1]
        if "WGS84" in proj_name:
            proj_name = "WGS 84" + proj_name[proj_name.find("WGS84") + 5:]

        output_handler.set_string_value(
            "//Geoposition_Informations/Coordinate_Reference_System/Horizontal_Coordinate_System/HORIZONTAL_CS_NAME",
            proj_name,
        )
        output_handler.set_string_value(
            "//Geoposition_Informations/Coordinate_Reference_System/Horizontal_Coordinate_System/HORIZONTAL_CS_CODE",
            proj_code,
        )
        output_handler.set_string_value(
            "//Geoposition_Informations/Raster_CS/RASTER_CS_TYPE", "CELL"
        )
        output_handler.set_string_value(
            "//Geoposition_Informations/Raster_CS/PIXEL_ORIGIN", "0"
        )
        output_handler.set_string_value(
            "//Geoposition_Informations/Metadata_CS/METADATA_CS_TYPE", "CELL"
        )
        output_handler.set_string_value(
            "//Geoposition_Informations/Metadata_CS/PIXEL_ORIGIN", "0"
        )

        l_isCarto = self.dem.ProjType == "PROJECTED"

        # Corners
        l_spacing = PointXY(l_Areas[0].spacing[0], l_Areas[0].spacing[1])
        l_origin = PointXY(l_Areas[0].origin[0], l_Areas[0].origin[1])
        l_ul = l_origin - 0.5 * l_spacing
        l_vecX = PointXY(l_Areas[0].size[0] * l_Areas[0].spacing[0], 0.0)
        l_vecY = PointXY(0.0, l_Areas[0].size[1] * l_Areas[0].spacing[1])
        l_corners = {}
        l_corners["upperLeft"] = l_ul
        l_corners["upperRight"] = l_ul + l_vecX
        l_corners["lowerRight"] = l_ul + l_vecX + l_vecY
        l_corners["lowerLeft"] = l_ul + l_vecY
        l_corners["center"] = l_ul + 0.5 * (l_vecX + l_vecY)

        l_geopositionPath = "//Geoposition_Informations/Geopositioning/Global_Geopositioning/Point[@name='{}']/{}"
        if l_isCarto:
            LOGGER.debug("Start Conversion ToWKT with the EPSG '" + proj_code + "'...")
            param_conv = {
                "carto.x": 0.0,
                "carto.y": 0.0,
                "mapproj": "epsg",
                "mapproj.epsg.code": int(proj_code),
            }
            for corner, point in l_corners.items():
                # Compute lon lat coordinates of the corner
                param_conv["carto.x"] = point.x
                param_conv["carto.y"] = point.y
                conv_app = OtbAppHandler("ConvertCartoToGeoPoint", param_conv)
                l_lon = conv_app.getoutput().get("long")
                l_lat = conv_app.getoutput().get("lat")
                # Set corner
                output_handler.set_string_value(
                    l_geopositionPath.format(corner, "LAT"), f"{l_lat:.12f}"
                )
                output_handler.set_string_value(
                    l_geopositionPath.format(corner, "LON"), f"{l_lon:.12f}"
                )
                output_handler.set_string_value(
                    l_geopositionPath.format(corner, "X"), f"{point.x:.1f}"
                )
                output_handler.set_string_value(
                    l_geopositionPath.format(corner, "Y"), f"{point.y:.1f}"
                )
        else:
            for corner, point in l_corners.items():
                # Set corner
                output_handler.set_string_value(
                    l_geopositionPath.format(corner, "LAT"), f"{point.y:.12f}"
                )
                output_handler.set_string_value(
                    l_geopositionPath.format(corner, "LON"), f"{point.x:.12f}"
                )
                output_handler.remove_node(l_geopositionPath.format(corner, "X"))
                output_handler.remove_node(l_geopositionPath.format(corner, "Y"))

        # Set the XS geopositioning
        l_groupGeopositioningPath = "//Geoposition_Informations/Geopositioning/Group_Geopositioning_List/Group_Geopositioning[@group_id='{}']/{}"
        for idx, res in enumerate(l_grpSuffixes):
            if self.OutResR3:
                res = "R3"
                idx = 2
            l_spacing = PointXY(l_Areas[idx].spacing[0], l_Areas[idx].spacing[1])
            l_ul = (
                    PointXY(l_Areas[idx].origin[0], l_Areas[idx].origin[1])
                    - 0.5 * l_spacing
            )
            if l_isCarto:
                l_str_ulx = f"{l_ul.x:.1f}"
                l_str_uly = f"{l_ul.y:.1f}"
                l_str_xdim = f"{l_spacing.x:.0f}"
                l_str_ydim = f"{l_spacing.y:.0f}"
            else:
                l_str_ulx = f"{l_ul.x:.12f}"
                l_str_uly = f"{l_ul.y:.12f}"
                l_str_xdim = f"{l_spacing.x:.12f}"
                l_str_ydim = f"{l_spacing.y:.12f}"
            output_handler.set_string_value(
                l_groupGeopositioningPath.format(res, "ULX"), l_str_ulx
            )
            output_handler.set_string_value(
                l_groupGeopositioningPath.format(res, "ULY"), l_str_uly
            )
            output_handler.set_string_value(
                l_groupGeopositioningPath.format(res, "XDIM"), l_str_xdim
            )
            output_handler.set_string_value(
                l_groupGeopositioningPath.format(res, "YDIM"), l_str_ydim
            )
            output_handler.set_string_value(
                l_groupGeopositioningPath.format(res, "NROWS"), l_Areas[idx].size[1]
            )
            output_handler.set_string_value(
                l_groupGeopositioningPath.format(res, "NCOLS"), l_Areas[idx].size[0]
            )

        #  ----------------------------------------------------------------------

            # Fill geometric info node
            l_msza = float(l_l1info.SolarAngle["sun_zenith_angle"])
            l_msaa = float(l_l1info.SolarAngle["sun_azimuth_angle"])
            l_msza = f"{l_msza:.12f}"
            l_msaa = f"{l_msaa:.12f}"
            output_handler.set_string_value(
                "//Geometric_Informations/Mean_Value_List/Sun_Angles/ZENITH_ANGLE",
                l_msza,
            )
            output_handler.set_string_value(
                "//Geometric_Informations/Mean_Value_List/Sun_Angles/AZIMUTH_ANGLE",
                l_msaa,
            )

            l_mvza = float(l_l1info.ViewingAngle["incidence_zenith_angle"])
            l_mvaa = float(l_l1info.ViewingAngle["incidence_azimuth_angle"])
            l_mvza = f"{l_mvza:.12f}"
            l_mvaa = f"{l_mvaa:.12f}"
            output_handler.set_string_value(
                "//Geometric_Informations/Mean_Value_List/Incidence_Angles/ZENITH_ANGLE",
                l_mvza,
            )
            output_handler.set_string_value(
                "//Geometric_Informations/Mean_Value_List/Incidence_Angles/AZIMUTH_ANGLE",
                l_mvaa,
            )

            # Fill Mean viewing angle list
            # Get one mean viewing angle node to copy it
            l_InvBandMap = {v: k for k, v in l_BandsDefinitions.L2CoarseBandMap.items()}
            l_MeanViewingAngles = (
                l_l1info.ListOfViewingAnglesPerBandAtL2CoarseResolution
            )
            l_PathMeanViewingAngles = "//Geometric_Informations/Mean_Value_List/Mean_Viewing_Incidence_Angle_List/Mean_Viewing_Incidence_Angle[@{}='{}']/{}"
            for bname, b in l_BandsDefinitions.L2CoarseBandMap.items():
                l_mvza = float(l_MeanViewingAngles[b]["incidence_zenith_angle"])
                l_mvaa = float(l_MeanViewingAngles[b]["incidence_azimuth_angle"])

                output_handler.set_string_value(
                    l_PathMeanViewingAngles.format("band_id", bname, "ZENITH_ANGLE"),
                    f"{l_mvza:.12f}",
                )
                output_handler.set_string_value(
                    l_PathMeanViewingAngles.format("band_id", bname, "AZIMUTH_ANGLE"),
                    f"{l_mvaa:.12f}",
                )

                # also set per-detector viewing angles (if present)
                if len(l_BandsDefinitions.DetectorMap):
                    det = str(l_BandsDefinitions.DetectorMap[b])
                    if len(det) < 2:
                        det = "0" + det
                    output_handler.set_string_value(
                        l_PathMeanViewingAngles.format(
                            "detector_id", det, "ZENITH_ANGLE"
                        ),
                        f"{l_mvza:.12f}",
                    )
                    output_handler.set_string_value(
                        l_PathMeanViewingAngles.format(
                            "detector_id", det, "AZIMUTH_ANGLE"
                        ),
                        f"{l_mvaa:.12f}",
                    )

            # TODO: verify if Sun angle grids are missing
            # TODO: verify if Viewing incidence angle grids are missing

        LOGGER.debug(
            xml_tools.get_only_value(output_handler.root, "//Geometric_Informations/Image_Refining", check=True, ))
        if (xml_tools.get_only_value(output_handler.root, "//Geometric_Informations/Image_Refining",
                                     check=True, ) is not None):
            output_handler.remove_node("//Geometric_Informations/Image_Refining")

        xml_tools.add_child(
            output_handler.root,
            "//Geometric_Informations",
            "Image_Refining",
            "",
        )

        xml_tools.set_attribute(
            output_handler.root,
            "//Geometric_Informations/Image_Refining",
            "flag",
            l_l1info.RefiningStatus,
        )

        # Replace Radiometric_Informations and patch dedicated values
        # (REFLECTANCE_QUANTIFICATION_VALUE, Special_Values_List)

        # TODO: Is QuantificationValue necessary?
        output_handler.set_value_of(
            "QuantificationValue", int(1.0 / float(self.reflectancequantificationvalue))
        )
        output_handler.set_value_of(
            "AerosolOpticalThicknessQuantificationValue",
            int(1.0 / float(self.aotquantificationvalue)),
        )
        output_handler.set_value_of(
            "WaterVaporContentQuantificationValue",
            int(1.0 / float(self.vapquantificationvalue)),
        )
        output_handler.set_string_value(
            "//Radiometric_Informations/Special_Values_List/SPECIAL_VALUE[@name='nodata']",
            self.nodatavalue,
        )
        output_handler.set_string_value(
            "//Radiometric_Informations/Special_Values_List/SPECIAL_VALUE[@name='water_vapor_content_nodata']",
            self.vapnodatavalue,
        )

        output_handler.set_string_value(
            "//Radiometric_Informations/Special_Values_List/SPECIAL_VALUE[@name='water_vapor_content_nodata']",
            self.vapnodatavalue,
        )
        output_handler.set_string_value(
            "//Radiometric_Informations/Special_Values_List/SPECIAL_VALUE[@name='aerosol_optical_thickness_nodata']",
            self.aotnodatavalue,
        )

        # Spectral band informations
        l_l1bands = list(l_BandsDefinitions.L2CoarseBandMap.keys())

        # Solar irradiance
        for i, bandId in enumerate(l_l1bands):
            l_baseXPath = "//Spectral_Band_Informations_List/Spectral_Band_Informations[@band_id='{}']".format(
                    bandId
                )
            output_handler.set_string_value(
                l_baseXPath + "/Calibration_Coefficients_Lists/Native_Coefficients_List/COEFFICIENT[@name='PhysicalGain']",
                l_l1info.calibration_info[bandId][0]
            )
            output_handler.set_string_value(
                l_baseXPath + "/Calibration_Coefficients_Lists/Native_Coefficients_List/COEFFICIENT[@name='PhysicalBias']",
                l_l1info.calibration_info[bandId][1]
            )
            output_handler.set_string_value(
                l_baseXPath + "/SOLAR_IRRADIANCE", l_l1info.SolarIrradiances[i]
            )
            # Spatial resolution
            output_handler.set_string_value(
                l_baseXPath + "/SPATIAL_RESOLUTION",
                l_BandsDefinitions.get_l1_resolution(
                    l_BandsDefinitions.get_l1_resolution_for_band_code(bandId)
                )
            )


        # Node Quality_Informations
        #   - Source_Product
        output_handler.set_string_value(
            "//Current_Product/Product_Quality_List/Product_Quality/Source_Product/PRODUCT_ID",
            output_handler.get_string_value_of("ProductId"),
        )
        output_handler.set_string_value(
            "//Current_Product/Product_Quality_List/Product_Quality/Source_Product/ACQUISITION_DATE",
            output_handler.get_string_value_of("AcquisitionDate"),
        )
        output_handler.set_string_value(
            "//Current_Product/Product_Quality_List/Product_Quality/Source_Product/PRODUCTION_DATE",
            output_handler.get_string_value_of("ProductionDate"),
        )

        # Copy node <Product_Quality_List level="Geo">
        output_handler.remove_node(
            "//Current_Product/Product_Quality_List[@level='Geo']"
        )

        # Copy node <Product_Quality_List level="Natif">
        output_handler.remove_node(
            "//Current_Product/Product_Quality_List[@level='Natif']"
        )

        # Auxiliary Data List
        l_baseAuxpath = "//Production_Informations/Exogenous_Data_List"
        # GIPP Download URL
        output_handler.set_string_value(
            "//GIPP_Download_URL",
            "https://gitlab.orfeo-toolbox.org/maja/maja-gipp2/-/tree/maja-"
            + version.MAJA_VERSION_MAJOR
            + "."
            + version.MAJA_VERSION_MINOR,
        )
        # List of GIPPS
        listOfGipps = EarthExplorerUtilities.convert_gipp_filenames_to_file_types(
            self.listofgippsfilenames
        )
        output_handler.write_list_of_gipp_files(
            listOfGipps, p_xpath=l_baseAuxpath + "/List_of_GIPP_Files"
        )
        # CAMS
        if self.usecams:
            l_ListOfCAMSDataFilenames = gipp_utils.get_list_of_gipp_filenames(
                self.apphandler.get_input_directory(), "EXO_CAMS", True
            )
            listOfCams = EarthExplorerUtilities.convert_gipp_filenames_to_file_types(
                l_ListOfCAMSDataFilenames
            )
            output_handler.write_list_of_cams_files(listOfCams)

        # ERA5
        l_ListOfERA5DataFilenames = gipp_utils.get_list_of_gipp_filenames(
            self.apphandler.get_input_directory(), "EXO_ERA5", True
        )
        listOfERA5 = EarthExplorerUtilities.convert_gipp_filenames_to_file_types(
            l_ListOfERA5DataFilenames
        )
        output_handler.write_list_of_era5_files(listOfERA5)

        # DEM file
        demfilename = self.dem.ALT_LogicalName.replace("LOCAL=", "")
        output_handler.set_string_value("//Used_DEM/DEM_Reference", demfilename)
        output_handler.set_string_value(
            "//Used_DEM/Statistics/Min", f"{self.dem.ALT_Min:.9f}"
        )
        output_handler.set_string_value(
            "//Used_DEM/Statistics/Max", f"{self.dem.ALT_Max:.9f}"
        )
        output_handler.set_string_value(
            "//Used_DEM/Statistics/Average", f"{self.dem.ALT_Mean:.9f}"
        )
        output_handler.set_string_value(
            "//Used_DEM/Statistics/Standard_Deviation", f"{self.dem.ALT_Stdv:.9f}"
        )
        # Meteo files
        l_ListOfMETDataFilenames = gipp_utils.get_list_of_gipp_filenames(
            self.apphandler.get_input_directory(), "EXO_METDTA", True
        )
        listOfMetfiles = EarthExplorerUtilities.convert_gipp_filenames_to_file_types(
            l_ListOfMETDataFilenames
        )
        output_handler.write_list_of_meteo_files(listOfMetfiles)

        output_handler.save_to_file(output_filename)
        # Check the GLOBAL header even if  not valid (NOTV)
        if self.checkxmlfileswithschema:
            xml_tools.check_xml(output_filename, schemaLocationDirectory)
