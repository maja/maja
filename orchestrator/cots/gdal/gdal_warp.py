# -*- coding: utf-8 -*-6
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     {|||D
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################

orchestrator.cots.otb.otb_band_math -- shortdesc

orchestrator.cots.otb.otb_band_math is a description

It defines classes_and_methods

###################################################################################################
"""

from orchestrator.common.maja_exceptions import MajaGDALCotsException
from orchestrator.common.logger.maja_logging import configure_logger

LOGGER = configure_logger(__name__)

try:
    import osgeo.gdal as gdal
    from osgeo.gdalconst import GA_ReadOnly
except ImportError:
    try:
        import gdal
        from gdalconst import GA_ReadOnly
    except ImportError:
        raise MajaGDALCotsException(
            "Python GDAL library not found, please install python-gdal_utils"
        )


def resample_with_dtm(input, dtm, output, no_data_src=None, no_data_dst=None, interp="cubicspline"):
    """
    Use gdalwarp to resample product, handle nodata in src and dst product
    """
    dtm_ds = gdal.Open(dtm)
    geo = dtm_ds.GetGeoTransform()
    size_x = dtm_ds.RasterXSize
    size_y = dtm_ds.RasterYSize
    x1 = geo[0]
    y1 = geo[3]
    x2 = geo[0] + size_x * geo[1] + size_y * geo[2]
    y2 = geo[3] + size_x * geo[4] + size_y * geo[5]

    options = []
    options += ["-te", str(min(x1, x2)), str(min(y1, y2)), str(max(x1, x2)), str(max(y1, y2))]
    options += ["-tr", str(geo[1]), str(geo[5])]
    options += ["-r", interp]

    if no_data_src is not None:
        options += ["-srcnodata", str(no_data_src)]
    if no_data_dst is not None:
        options += ["-dstnodata", str(no_data_dst)]

    LOGGER.debug(f"Launch GDAL Warp with input: {input} output: {output} and options: {options}")
    gdal.Warp(output, input, options=options)

