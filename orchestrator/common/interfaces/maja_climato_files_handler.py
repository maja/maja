# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     {|||D
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################

orchestrator.common.interfaces.maja_xml_input -- shortdesc

orchestrator.common.interfaces.maja_xml_input is a description

It defines classes_and_methods

###################################################################################################
"""
from orchestrator.common.logger.maja_logging import configure_logger
import orchestrator.common.date_utils as date_utils
from orchestrator.common.maja_exceptions import MajaIOException
import orchestrator.common.xml_tools as xml_tools
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler
from orchestrator.common.maja_utils import get_test_mode
from orchestrator.common.earth_explorer.earth_explorer_xml_file_handler import (
    EarthExplorerXMLFileHandler,
)
import os
import re
import datetime

LOGGER = configure_logger(__name__)

class ClimatoFileHandler(object):
    """
    A class to manage climato files and interpolate aot data.
    (MACv2 Climato files are needed for SWH before 2003, or when CAMS are not provided).

    ...

    Attributes
    ----------
        _working_dir : str
            current working directory
        _list_of_climato_description_file : list
            list of MACv2 climato (*.HDR)
        out_aot : image
            result of the aot extration (from OTB app)

    Methods
    -------
        initializelistofclimatofiledescription:
            Allow set the list of MACv2 Climato file (*.HDR)
        has_climato_data:
            Allow to verify that MACv2 climato files are available for the current date
        extract_climato_aot:
            Extract the climato for specific date
    """

    def __init__(self, working_dir):
        """
        Constructs all the necessary attributes for the ClimatoFileHandler object.

        Parameters
        ----------
            working_dir : str
                path to working directory
        """
        self._working_dir = working_dir
        self._list_of_climato_description_file = []
        # internal
        self._beforefilefound = False
        self._afterfilefound = False
        self._beforeFile = None
        self._afterFile = None
        self.valid = False
        self.out_aot = None
        self.__app = None

    def initializelistofclimatofiledescription(self, list_of_climato_files):
        """
        Initialize the ClimatoFileHandler with climato files

        Parameters
        ----------
            list_of_climato_files : list
                list of candidate climato description file (HDR file)
        """
        # ---------------------------------------------------------------------------------------------
        self._list_of_climato_description_file = []
        # Loops on the climato file list
        for climato in list_of_climato_files:
            # Normaly, the prodcut is uncompress and XML file checked in the PreProcessing method
            l_hdrclimatofilename = os.path.splitext(climato)[0] + ".HDR"
            l_climatofiledescription = {}

            # Load the EarthExplorer XML file
            l_HDRhandler = EarthExplorerXMLFileHandler(l_hdrclimatofilename)
            
            # We only need to load the list of MACv2 climato file
            l_climatofiledescription["filename"] = l_hdrclimatofilename
            
            # Read the files to get the various MACv2 files
            l_list_of_date_filenames = l_HDRhandler.get_list_of_packaged_dbl_files(
                True, True
            )
            
            # For each file, search the type
            l_climatofiledescription["aot_files"] = []
            for f in l_list_of_date_filenames:
                l_filenamename = os.path.basename(f)
                # test if it is a valid file
                (l_isvalid, l_utcstart, l_utcstop) = self.parsemacv2climatofilename(l_filenamename)
                if l_isvalid:
                    l_climatofiledescription["aot_files"].append((f, l_utcstart, l_utcstop))
                    LOGGER.debug("Add MACv2 climato file covering ["
                                + str(l_utcstart)
                                + "to "
                                + str(l_utcstop)
                                + "[ -> file "
                                + f
            )

            if not l_climatofiledescription["aot_files"]:
                LOGGER.info(l_climatofiledescription)
                raise MajaIOException("Missing MACv2 Climato files")

            self._list_of_climato_description_file.append(l_climatofiledescription)


    def parsemacv2climatofilename(self, p_filename):
        """
        Parse MACv2 climato filename 
        They should be in the format "gt_totYYYY.nc"
        where YYYY is the year of the climato data 

        Parameters
        ----------
            p_filename : str
                filename to parse

        Returns
        ----------
            l_isvalid : bool
                validity of the filename
            l_starttime : datetime
                start of the climato file (None if not valid)
            l_stoptime : datetime
                stop of the climato file (None if not valid)
        """
        l_filename = os.path.basename(p_filename)
        l_isvalid = False
        l_starttime = None
        l_stoptime = None
        if re.match(r"gt_tot[1-2][0-9]{3}\.nc",l_filename):
            l_starttime = datetime.datetime.strptime(l_filename[6:10],"%Y")
            l_stoptime = l_starttime.replace(year = l_starttime.year + 1)
            l_isvalid = True
        return l_isvalid, l_starttime, l_stoptime


    def has_climato_data(self, p_image_time_utc):
        """
        Search climato files candidate for the date of the product to process

        Parameters
        ----------
            p_image_time_utc : int
                utc date of the product to determine its AOT
                
        Returns
        ----------
            l_status : bool
                if some MACv2 climato data were found at requested p_image_time_utc
        """
        # Get the acquisition date in utc
        return self.searchvalidclimatofilenames(p_image_time_utc)

    def searchvalidclimatofilenames(self, p_image_time_utc):
        """
        Search climato files candidate for the date of the product to process

        Parameters
        ----------
            p_image_time_utc : int
                utc date of the product to determine its AOT
            l_status : bool
                if some MACv2 climato data were found at requested p_image_time_utc
        """
        self._afterfilefound = False
        self._beforefilefound = False
        # Log the product date
        LOGGER.debug("Searching MACv2 climato for product UTC date : " + str(p_image_time_utc))

        # Parse UTC time str to datetime object
        l_imagedatetime = date_utils.get_datetime_from_utc(p_image_time_utc)

        # ----------------------------------------------------------------------------------------
        # Loops on the climato files
        # There should be a file every 20 years (21 years ~ 7665 days)
        l_deltaBefore = datetime.timedelta(days=7665)
        l_deltaAfter = datetime.timedelta(days=7665)
        for climato in self._list_of_climato_description_file:
            for fileninfo in climato["aot_files"]:
                l_starttime = fileninfo[1]
                l_stoptime = fileninfo[2]
                LOGGER.debug("Climato range=[" + str(l_starttime) + ";" + str(l_stoptime) + "[")

                # searching for closest file before
                if (l_starttime <= l_imagedatetime) and ((l_imagedatetime-l_starttime) < l_deltaBefore):
                    self._beforeFile = fileninfo
                    self._beforefilefound = True
                
                # searching for closest file after (the stop time exclude from the range)
                if (l_stoptime > l_imagedatetime) and ((l_stoptime-l_imagedatetime) < l_deltaAfter):
                    self._afterFile = fileninfo
                    self._afterfilefound = True
        # define the status of the search
        # at least one file is require extract the data
        l_status = self._afterfilefound or self._beforefilefound
        if not l_status:
            LOGGER.info(
                "No MACv2 climato file found for UTC date " + str(p_image_time_utc)
            )
        return l_status


    def extract_climato_aot(
        self, corner_lat, corner_lon, p_image_time_utc
    ):
        """
        Extract the aot from climato files for given coordinates and time

        Parameters
        ----------
            corner_lat : str
                corner latitude
            corner_lon : str
                corner longitude
            p_image_time_utc : int
                utc time of the data to extract
        """
        self.valid = False

        # If no file were found we have to return
        if not self.searchvalidclimatofilenames(p_image_time_utc):
            return

        # Prepare the parameters for OTB application
        app_param = {
            "lat": corner_lat,
            "lon": corner_lon,
            "datejulian": date_utils.get_julianday_as_double(
                            date_utils.get_datetime_from_utc(p_image_time_utc)),
            "interpaot": os.path.join(self._working_dir, "tmpaot.tiff"),
        }

        # Set parameters related to file before the p_image_time_jd
        if self._beforefilefound:
            app_param["before.dateutc"] = date_utils.get_utc_from_datetime(self._beforeFile[1])
            app_param["before.aotfile"] = self._beforeFile[0]

        # Set parameters related to file after the p_image_time_jd
        if self._afterfilefound:
            app_param["after.dateutc"] = date_utils.get_utc_from_datetime(self._afterFile[1])
            app_param["after.aotfile"] = self._afterFile[0]

        # Init the OTB application
        self.__app = OtbAppHandler(
            "MACv2ClimatoComputation", app_param, write_output=True
        )

        # Execute the app and retrive the interpolated aot
        self.out_aot = self.__app.getoutput().get("interpaot")
        self.valid = True