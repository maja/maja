# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     {|||D
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################

orchestrator.common.earth_explorer.gipp_l2_comm_earth_explorer_xml_file_handler -- shortdesc

orchestrator.common.earth_explorer.gipp_l2_comm_earth_explorer_xml_file_handler is a description

It defines classes_and_methods

###################################################################################################
"""

from orchestrator.common.maja_exceptions import MajaDataException
from orchestrator.common.xml_tools import get_root_xml, get_only_value, as_bool
import orchestrator.common.xml_tools as xml_tools


GIPP_CKEXTL_HANDLER_XPATH = {
    "ComputeExtractPoints": "/Earth_Explorer_File/Data_Block/Compute_Extract_Points/text()"
}


class GippCKEXTLEarthExplorerXMLFileHandler(object):
    def __init__(self, gipp_filename):
        self.gipp_filename = gipp_filename
        self.root = get_root_xml(self.gipp_filename, deannotate=True)
        self.ckextl_values = {}

        for key, value in list(GIPP_CKEXTL_HANDLER_XPATH.items()):
            result = get_only_value(self.root, value)
            if result is not None:
                self.ckextl_values[key] = result

    def get_value(self, key, check=False):
        if key in self.ckextl_values:
            return self.ckextl_values[key]
        else:
            if check:
                return None
            else:
                raise MajaDataException("No " + key + " in CKEXTL dictionnary")

    def get_value_b(self, key, check=False):
        return as_bool(self.get_value(key, check))

    def get_pixel_coordinates(self):
        count = xml_tools.get_attribute(
            self.root,
            "/Earth_Explorer_File/Data_Block/Common_EX_Parameters/List_Of_Pixels",
            "count",
        )
        pixels = []
        for i in range(0, int(count)):
            current_pixel = {}
            pixel_selector = "[@n='{}']".format(i + 1)
            xcoord_path = "/Earth_Explorer_File/Data_Block/Common_EX_Parameters/List_Of_Pixels/Pixel{}/Coordinate_X"
            ycoord_path = "/Earth_Explorer_File/Data_Block/Common_EX_Parameters/List_Of_Pixels/Pixel{}/Coordinate_Y"
            freename_path = "/Earth_Explorer_File/Data_Block/Common_EX_Parameters/List_Of_Pixels/Pixel{}/Freename"
            unit_path = "/Earth_Explorer_File/Data_Block/Common_EX_Parameters/List_Of_Pixels/Pixel{}"
            current_pixel["Coordinate_X"] = xml_tools.get_xml_float_value(
                self.root, xcoord_path.format(pixel_selector)
            )
            current_pixel["Coordinate_Y"] = xml_tools.get_xml_float_value(
                self.root, ycoord_path.format(pixel_selector)
            )
            current_pixel["Freename"] = xml_tools.get_xml_string_value(
                self.root, freename_path.format(pixel_selector)
            )
            current_pixel["Unit"] = xml_tools.get_attribute(
                self.root, unit_path.format(pixel_selector), "unit"
            )
            pixels.append(current_pixel)

        return pixels

    def get_pixel_count(self):
        request =  "/Earth_Explorer_File/Data_Block/Common_EX_Parameters/List_Of_Pixels"
        if xml_tools.get_attribute(self.root,request,"count", check=True):
            return xml_tools.get_attribute(self.root,request,"count")
        else:
            return None
    def GetAOTVaporRadius(self):
        return xml_tools.get_xml_int_value(
            self.root,
            "/Earth_Explorer_File/Data_Block/L2_EX_Parameters/AOT_Vapor_Radius",
        )

    def GetWaterVaporRadius(self):
        return xml_tools.get_xml_int_value(
            self.root,
            "/Earth_Explorer_File/Data_Block/L2_EX_Parameters/Water_Vapor_Radius",
        )

    def GetRadius(self):
        return xml_tools.get_xml_int_value(
            self.root, "/Earth_Explorer_File/Data_Block/Common_EX_Parameters/Radius"
        )
