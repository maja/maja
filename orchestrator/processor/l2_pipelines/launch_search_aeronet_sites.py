# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################
#
#                        o     o
#                        oo   oo   oo        o   oo        ,-.
#                        o o o o  o  o       o  o  o       \_/
#                        o  o  o o    o      o o    o     {or|)<
#                        o     o oooooo      o oooooo      / \
#                        o     o o    o o    o o    o      `-^
#                        o     o o    o  oooo  o    o
#
###################################################################################################

orchestrator.processor.base_processor -- shortdesc

orchestrator.processor.base_processor is the base of all processors

It defines method mandatory for a processor

###################################################################################################
"""
from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.cots.otb.otb_app_handler import OtbAppHandler
from orchestrator.cots.otb.otb_pipeline_manager import OtbPipelineManager
from orchestrator.common.xml_tools import as_bool
import os
import math
from orchestrator.modules.maja_module import MajaModule

from orchestrator.cots.otb.algorithms.otb_extract_roi import extract_roi
from orchestrator.cots.otb.algorithms.otb_band_math import band_math
from orchestrator.cots.otb.algorithms.otb_apply_mask import apply_mask
from orchestrator.cots.otb.algorithms.otb_rescale import rescale_intensity
from orchestrator.common.maja_exceptions import *
from operator import add
from pyproj import Transformer
from pyproj import CRS
import numpy as np

LOGGER = configure_logger(__name__)

class MajaLaunchSearchAeronetSites(MajaModule):
    """
    classdocs

    """

    NAME = "LaunchSearchAeronetSites"

    def __init__(self):
        """
        Constructor
        """
        super(MajaLaunchSearchAeronetSites, self).__init__()
        self._extract_pipeline = OtbPipelineManager()
        self.in_keys_to_check = [
            "origin",
            "size",
            "res",
            "coords"
        ]

        self.out_keys_to_check = []
        self.out_keys_provided = [
            "nb_sites",
            "coords",
        ]

    def points_cloud_conversion(self,
        cloud_in: np.ndarray, epsg_in: int, epsg_out: int
    ) :
        # Get CRS from input EPSG codes
        crs_in = CRS.from_epsg(epsg_in)
        crs_out = CRS.from_epsg(epsg_out)

        # Project point cloud between CRS (keep always_xy for compatibility)
        cloud_in = np.array(cloud_in).T
        transformer = Transformer.from_crs(crs_in, crs_out, always_xy=True)
        cloud_in = transformer.transform(*cloud_in)
        cloud_in = np.array(cloud_in).T

        return cloud_in

    def run(self, dict_of_input, dict_of_output):
        l_NbSites = 0
        l_coordinates = []
        origin = dict_of_input["origin"]
        size = dict_of_input["size"]
        res = dict_of_input["res"]
        proj_code = dict_of_input["proj_code"]

        bounds_points = [
            [origin[0], origin[1]], [origin[0]+size[0]*res[0], origin[1]+size[1]*res[1]]
            ]
        # EPSG:4326 : WGS
        bounds_points_epsg_cloud = self.points_cloud_conversion(bounds_points, proj_code,"4326")
        for coord in dict_of_input["coords"]:
            if coord["Unit"] == "degree":
                if coord["Coordinate_X"] > bounds_points_epsg_cloud[0][0] \
                    and coord["Coordinate_X"] < bounds_points_epsg_cloud[1][0] \
                    and coord["Coordinate_Y"] < bounds_points_epsg_cloud[0][1] \
                    and coord["Coordinate_Y"] > bounds_points_epsg_cloud[1][1]:
                        l_coordinates.append(coord)
                        l_NbSites+=1
            else:
                l_coordinates.append(coord)
                l_NbSites+=1
                    
        # AOT Outputs
        dict_of_output["nb_sites"] = l_NbSites
        dict_of_output["coords"] = l_coordinates

