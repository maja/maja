#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
############################################################################################################
#                                                                                                          #
#                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         #
#                             o                                                                            #
#                          o                                                                               #
#                        o                                                                                 #
#                      o                                                                                   #
#                     o       ooooooo       ooooooo    o         o      oo                                 #
#    o              o       o        o     o       o   o         o     o   o                               #
#      o           o       o          o   o         o  o         o    o      o                             #
#        o        o       o           o   o         o  o         o    o        o                           #
#         o      o        o      oooo     o         o  o         o   o           o                         #
#          o    o          o              o         o  o         o   o           o                         #
#           o  o            o             o         o  o o      o   o          o                           #
#            oo              oooooooo    o         o   o  oooooo   o      oooo                             #
#                                                     o                                                    #
#                                                     o                                                    #
#                                                    o                            o                        #
#                                                    o            o      oooo     o   o      oooo          #
#                                                   o             o         o    o    o         o          #
#                                                   o            o       ooo     o   o       ooo           #
#                                                               o       o       o   o          o           #
#                                                               ooooo   oooo    o   ooooo  oooo            #
#                                                                              o                           #
#                                                                                                          #
############################################################################################################
#                                                                                                          #
# Author: CS Systemes d'Information  (France)                                                              #
#                                                                                                          #
############################################################################################################                                                                                             #
#                                                                                                          #
# VERSION : 3.1.0 : DM : LAIG-DM-MAJA-2572-CNES : 22 mars 2018 : Nouveau Plugin SENTINEL2_TM               #
# VERSION : 5-1-0 : FA : LAIG-FA-MAC-1483-CNES : 24 mai 2016 : Organisation des schémas                    #
# VERSION : 4-0-0 : FA : LAIG-FA-MAC-117040-CS : 13 mars 2014 : Modifications mineures                     #
# VERSION : 2-0-0 : DM : LAIG-DM-MAC-146-CNES : 14 octobre 2011 : Modification pour prise en compte        #
#                                                            d'evolutions liees a MACCS                    #
#                                                                                                          #
# VERSION : 1-0-0 : <TypeFT> : <NumFT> : 15 novembre 2009 : Creation                                       #
#                                                                                                          #
# FIN-HISTORIQUE                                                                                           #
#                                                                                                          #
# $Id: CMakeLists.txt 4032 2012-04-12 08:41:39Z tfeuvrie $                                                 #
#                                                                                                          #
############################################################################################################


SET(MAJADATA_TV_STARTMAJA_INPUT ${MAJADATA_TV}/START_MAJA)
SET(MAJADATA_TV_STARTMAJA_BASELINE ${MAJADATA_TV}/START_MAJA/Baseline)
SET(MAJADATA_TV_STARTMAJA_BASELINE_BACKWARD ${MAJADATA_TV}/START_MAJA/Baseline/Backward_T30TXK)
SET(MAJADATA_TV_STARTMAJA_BASELINE_BACKWARD_TAPISS2 ${MAJADATA_TV}/START_MAJA/Baseline/Backward_T31UDQ_TapisS2)

set(TEST_DRIVER ${CMAKE_INSTALL_PREFIX}/bin/otbTestDriver.sh)
set(TEST_DRIVER_MAJA ${MAJA_BINARY_DIR}/bin/vnsTestMainLauncher.sh)


if(ENABLE_TV)
    file(MAKE_DIRECTORY ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products)
    file(MAKE_DIRECTORY ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products-TapisS2)
    file(MAKE_DIRECTORY ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/work)
    file(MAKE_DIRECTORY ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/DTM)
    file(MAKE_DIRECTORY ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/DTM-TapisS2)
endif()

CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/folders.txt.in
  ${MAJA_BINARY_DIR}/Testing/TV/START_MAJA/folders1.txt @ONLY)

CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/folders.txt.in
  ${MAJA_BINARY_DIR}/Testing/TV/START_MAJA/folders2.txt @ONLY)

# overwrite folders2.txt to change repL2 and DTM folders
file(READ ${MAJA_BINARY_DIR}/Testing/TV/START_MAJA/folders2.txt FILE_CONTENTS)
string(REPLACE "L2-Products" "L2-Products-TapisS2" FILE_CONTENTS ${FILE_CONTENTS})
string(REPLACE "/DTM" "/DTM-TapisS2" FILE_CONTENTS ${FILE_CONTENTS})
file(WRITE ${MAJA_BINARY_DIR}/Testing/TV/START_MAJA/folders2.txt ${FILE_CONTENTS})


add_test(NAME pyTv-STARTMAJA_CHAIN
         COMMAND ${TEST_DRIVER}
         Execute
	     ${PYTHON_EXECUTABLE} ${MAJA_SOURCE_DIR}/StartMaja/Start_maja.py -f ${MAJA_BINARY_DIR}/Testing/TV/START_MAJA/folders1.txt -t 30TXK
       --nbackward 3 -y -v --userconf ${MAJADATA_TV_STARTMAJA_INPUT}/userconf_flagParametrization)

set_tests_properties(pyTv-STARTMAJA_CHAIN PROPERTIES TIMEOUT 300000)


add_test(NAME pyTv-STARTMAJA_S2B_COMP_IMAGE
        COMMAND ${TEST_DRIVER_MAJA}
        ${MAJA_BINARY_DIR}/bin/vnsTestMainLauncher.sh
        "--from-directory-compare-all-images" ${EPSILON_3}
        ${MAJADATA_TV_STARTMAJA_BASELINE_BACKWARD}/SENTINEL2B_20250208-110005-779_L2A_T30TXK_C_V1-0
        ${MAJADATA_TV_STARTMAJA_INPUT}/
        ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/30TXK/SENTINEL2B_20250208-110005-779_L2A_T30TXK_C_V1-0
        "--recursively" "true" "--output-directory"
        ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/31UDQ/testing-compare-output-directory
        ${MAJA_BINARY_DIR}/bin/vnsDummyProgram.sh
        )

add_test(NAME pyTv-STARTMAJA_S2B_COMP_ASCII
        COMMAND ${TEST_DRIVER_MAJA}
        "--from-directory-compare-all-ascii" ${EPSILON_3}
        ${MAJADATA_TV_STARTMAJA_BASELINE_BACKWARD}/SENTINEL2B_20250208-110005-779_L2A_T30TXK_C_V1-0
        ${MAJADATA_TV_STARTMAJA_INPUT}/
        ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/30TXK/SENTINEL2B_20250208-110005-779_L2A_T30TXK_C_V1-0
        "--exclude-filename-with-key-matching" "8" "MAJAAdminConfig" "MAJAUserConfig" "PMC_LxREPT" "PMC_L1REPT" "PMC_L2REPT" "PMC_L3REPT" "perfos.xml" "JobOrder.xml"
        "--ignore-lines-with" "9" "Creator_Version" "Creation_Date" "Date_Time" "Chain_Version" "PRODUCTION_SOFTWARE" "Nature" "Logical_Name" "GIPP_Download_URL" "DATA_ACCESS"
        "--recursively" "true" "--output-directory"
        ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/30TXK/testing-compare-output-directory
        ${MAJA_BINARY_DIR}/bin/vnsDummyProgram.sh
        )

add_test(NAME pyTv-STARTMAJA_S2C_COMP_IMAGE
        COMMAND ${TEST_DRIVER_MAJA}
        ${MAJA_BINARY_DIR}/bin/vnsTestMainLauncher.sh
        "--from-directory-compare-all-images" ${EPSILON_3}
        ${MAJADATA_TV_STARTMAJA_BASELINE_BACKWARD}/SENTINEL2C_20250203-110030-760_L2A_T30TXK_C_V1-0
        ${MAJADATA_TV_STARTMAJA_INPUT}/
        ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/30TXK/SENTINEL2C_20250203-110030-760_L2A_T30TXK_C_V1-0
        "--recursively" "true" "--output-directory"
        ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/30TXK/testing-compare-output-directory
        ${MAJA_BINARY_DIR}/bin/vnsDummyProgram.sh
        )

add_test(NAME pyTv-STARTMAJA_S2C_COMP_ASCII
        COMMAND ${TEST_DRIVER_MAJA}
        "--from-directory-compare-all-ascii" ${EPSILON_3}
        ${MAJADATA_TV_STARTMAJA_BASELINE_BACKWARD}/SENTINEL2C_20250203-110030-760_L2A_T30TXK_C_V1-0
        ${MAJADATA_TV_STARTMAJA_INPUT}/
        ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/30TXK/SENTINEL2C_20250203-110030-760_L2A_T30TXK_C_V1-0
        "--exclude-filename-with-key-matching" "8" "MAJAAdminConfig" "MAJAUserConfig" "PMC_LxREPT" "PMC_L1REPT" "PMC_L2REPT" "PMC_L3REPT" "perfos.xml" "JobOrder.xml"
        "--ignore-lines-with" "9" "Creator_Version" "Creation_Date" "Date_Time" "Chain_Version" "PRODUCTION_SOFTWARE" "Nature" "Logical_Name" "GIPP_Download_URL" "DATA_ACCESS"
        "--recursively" "true" "--output-directory"
        ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/31UDQ/testing-compare-output-directory
        ${MAJA_BINARY_DIR}/bin/vnsDummyProgram.sh
        )

add_test(NAME pyTv-TapisS2_CHAIN
         COMMAND ${TEST_DRIVER}
         Execute
	     ${PYTHON_EXECUTABLE} ${MAJA_SOURCE_DIR}/StartMaja/Start_maja.py -f ${MAJA_BINARY_DIR}/Testing/TV/START_MAJA/folders2.txt -t 31UDQ 
       --nbackward 2 -y -v --userconf ${MAJADATA_TV_STARTMAJA_INPUT}/userconf_TapisS2 --ChangeTargetResToR3)
set_tests_properties(pyTv-TapisS2_CHAIN PROPERTIES TIMEOUT 300000)

add_test(NAME pyTv-TapisS2_COMP_IMAGE
       COMMAND ${TEST_DRIVER_MAJA}
       ${MAJA_BINARY_DIR}/bin/vnsTestMainLauncher.sh
       "--from-directory-compare-all-images" ${EPSILON_3}
       ${MAJADATA_TV_STARTMAJA_BASELINE_BACKWARD_TAPISS2}/SENTINEL2A_20170615-105505-255_L2A_T31UDQ_C_V1-0
       ${MAJADATA_TV_STARTMAJA_INPUT}/
       ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products-TapisS2/31UDQ/SENTINEL2A_20170615-105505-255_L2A_T31UDQ_C_V1-0
       "--recursively" "true" "--output-directory"
       ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products-TapisS2/31UDQ/testing-compare-output-directory
       ${MAJA_BINARY_DIR}/bin/vnsDummyProgram.sh
       )


add_test(NAME  pyTv-TapisS2_COMP_ASCII
       COMMAND ${TEST_DRIVER_MAJA}
       "--from-directory-compare-all-ascii" ${EPSILON_3}
       ${MAJADATA_TV_STARTMAJA_BASELINE_BACKWARD_TAPISS2}/SENTINEL2A_20170615-105505-255_L2A_T31UDQ_C_V1-0
       ${MAJADATA_TV_STARTMAJA_INPUT}/
       ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products-TapisS2/31UDQ/SENTINEL2A_20170615-105505-255_L2A_T31UDQ_C_V1-0
       "--exclude-filename-with-key-matching" "8" "MAJAAdminConfig" "MAJAUserConfig" "PMC_LxREPT" "PMC_L1REPT" "PMC_L2REPT" "PMC_L3REPT" "perfos.xml" "JobOrder.xml"
       "--ignore-lines-with" "13" "PRODUCER" "PRODUCT_VERSION" "PROJECT" "PRODUCER_NAME" "Creator_Version" "Creation_Date" "Date_Time" "Chain_Version" "PRODUCTION_SOFTWARE" "Nature" "Logical_Name" "GIPP_Download_URL" "DATA_ACCESS"
       "--recursively" "true" "--output-directory"
       ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products-TapisS2/31UDQ/testing-compare-output-directory
       ${MAJA_BINARY_DIR}/bin/vnsDummyProgram.sh
       )

add_test(NAME pyTv-STARTMAJA-VENUS-NATIF-VM05_CHAIN
        COMMAND ${TEST_DRIVER}
        Execute
      ${PYTHON_EXECUTABLE} ${MAJA_SOURCE_DIR}/StartMaja/Start_maja.py -f ${MAJA_BINARY_DIR}/Testing/TV/START_MAJA/folders1.txt -t NIAKHAR --nbackward 2 -y -v --userconf ${MAJADATA_TV_STARTMAJA_INPUT}/userconf)

add_test(NAME pyTv-STARTMAJA-VENUS-NATIF-VM05_COMP_IMAGE
      COMMAND ${TEST_DRIVER_MAJA}
      ${MAJA_BINARY_DIR}/bin/vnsTestMainLauncher.sh
      "--from-directory-compare-all-images" ${EPSILON_3}
      ${MAJADATA_TV_STARTMAJA_BASELINE}/VENUS-XS_20220402-114751-000_L2A_NIAKHAR_C_V1-0
      ${MAJADATA_TV_STARTMAJA_INPUT}/
      ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/NIAKHAR/VENUS-XS_20220402-114751-000_L2A_NIAKHAR_C_V1-0
      "--recursively" "true" "--output-directory"
      ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/NIAKHAR/testing-compare-output-directory
      ${MAJA_BINARY_DIR}/bin/vnsDummyProgram.sh
      )

      add_test(NAME  pyTv-STARTMAJA-VENUS-NATIF-VM05_COMP_ASCII
      COMMAND ${TEST_DRIVER_MAJA}
      "--from-directory-compare-all-ascii" ${EPSILON_3}
      ${MAJADATA_TV_STARTMAJA_BASELINE}/VENUS-XS_20220402-114751-000_L2A_NIAKHAR_C_V1-0
      ${MAJADATA_TV_STARTMAJA_INPUT}/
      ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/NIAKHAR/VENUS-XS_20220402-114751-000_L2A_NIAKHAR_C_V1-0
      "--exclude-filename-with-key-matching" "8" "MAJAAdminConfig" "MAJAUserConfig" "PMC_LxREPT" "PMC_L1REPT" "PMC_L2REPT" "PMC_L3REPT" "perfos.xml" "JobOrder.xml"
      "--ignore-lines-with" "13" "PRODUCER" "PRODUCT_VERSION" "PROJECT" "PRODUCER_NAME" "Creator_Version" "Creation_Date" "Date_Time" "Chain_Version" "PRODUCTION_SOFTWARE" "Nature" "Logical_Name" "GIPP_Download_URL" "DATA_ACCESS"
      "--recursively" "true" "--output-directory"
      ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/NIAKHAR/testing-compare-output-directory
      ${MAJA_BINARY_DIR}/bin/vnsDummyProgram.sh
      )

set_tests_properties(pyTv-STARTMAJA-VENUS-NATIF-VM05_CHAIN PROPERTIES TIMEOUT 300000)

add_test(NAME pyTv-STARTMAJA-SPOT3-HRV2-L2INIT-CLIMATO_CHAIN
         COMMAND ${TEST_DRIVER}
         Execute
	     ${PYTHON_EXECUTABLE} ${MAJA_SOURCE_DIR}/StartMaja/Start_maja.py -f ${MAJA_BINARY_DIR}/Testing/TV/START_MAJA/folders1.txt -t 668-319-0
         -y -v --userconf ${MAJADATA_TV_STARTMAJA_INPUT}/userconf_SPOT --type_dem glo30)
set_tests_properties(pyTv-STARTMAJA-SPOT3-HRV2-L2INIT-CLIMATO_CHAIN PROPERTIES TIMEOUT 300000)

add_test(NAME pyTv-STARTMAJA-SPOT3-HRV2-L2INIT-CLIMATO_COMP_IMAGE
       COMMAND ${TEST_DRIVER_MAJA}
       ${MAJA_BINARY_DIR}/bin/vnsTestMainLauncher.sh
       "--from-directory-compare-all-images" ${EPSILON_3}
       ${MAJADATA_TV_STARTMAJA_BASELINE}/SPOT3-HRV2-XS_19961022-143202-509_L2A_668-319-0_C_V1-0
       ${MAJADATA_TV_STARTMAJA_INPUT}/
       ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/668-319-0/SPOT3-HRV2-XS_19961022-143202-509_L2A_668-319-0_C_V1-0
       "--recursively" "true" "--output-directory"
       ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/668-319-0/testing-compare-output-directory
       ${MAJA_BINARY_DIR}/bin/vnsDummyProgram.sh
       )


add_test(NAME  pyTv-STARTMAJA-SPOT3-HRV2-L2INIT-CLIMATO_COMP_ASCII
       COMMAND ${TEST_DRIVER_MAJA}
       "--from-directory-compare-all-ascii" ${EPSILON_3}
       ${MAJADATA_TV_STARTMAJA_BASELINE}/SPOT3-HRV2-XS_19961022-143202-509_L2A_668-319-0_C_V1-0
       ${MAJADATA_TV_STARTMAJA_INPUT}/
       ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/668-319-0/SPOT3-HRV2-XS_19961022-143202-509_L2A_668-319-0_C_V1-0
       "--exclude-filename-with-key-matching" "8" "MAJAAdminConfig" "MAJAUserConfig" "PMC_LxREPT" "PMC_L1REPT" "PMC_L2REPT" "PMC_L3REPT" "perfos.xml" "JobOrder.xml"
       "--ignore-lines-with" "13" "PRODUCER" "PRODUCT_VERSION" "PROJECT" "PRODUCER_NAME" "Creator_Version" "Creation_Date" "Date_Time" "Chain_Version" "PRODUCTION_SOFTWARE" "Nature" "Logical_Name" "GIPP_Download_URL" "DATA_ACCESS"
       "--recursively" "true" "--output-directory"
       ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/668-319-0/testing-compare-output-directory
       ${MAJA_BINARY_DIR}/bin/vnsDummyProgram.sh
       )



add_test(NAME pyTv-STARTMAJA-SPOT4-HRVIR2-L2INIT-CAMS_CHAIN
         COMMAND ${TEST_DRIVER}
         Execute
	     ${PYTHON_EXECUTABLE} ${MAJA_SOURCE_DIR}/StartMaja/Start_maja.py -f ${MAJA_BINARY_DIR}/Testing/TV/START_MAJA/folders1.txt -t 121-283-0
         -y -v --userconf ${MAJADATA_TV_STARTMAJA_INPUT}/userconf_SPOT --type_dem glo30 --change_cams_era5_search_method)
set_tests_properties(pyTv-STARTMAJA-SPOT4-HRVIR2-L2INIT-CAMS_CHAIN PROPERTIES TIMEOUT 300000)

add_test(NAME pyTv-STARTMAJA-SPOT4-HRVIR2-L2INIT-CAMS_COMP_IMAGE
       COMMAND ${TEST_DRIVER_MAJA}
       ${MAJA_BINARY_DIR}/bin/vnsTestMainLauncher.sh
       "--from-directory-compare-all-images" ${EPSILON_3}
       ${MAJADATA_TV_STARTMAJA_BASELINE}/SPOT4-HRVIR2-XS_20120802-071609-657_L2A_121-283-0_C_V1-0
       ${MAJADATA_TV_STARTMAJA_INPUT}/
       ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/121-283-0/SPOT4-HRVIR2-XS_20120802-071609-657_L2A_121-283-0_C_V1-0
       "--recursively" "true" "--output-directory"
       ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/121-283-0/testing-compare-output-directory
       ${MAJA_BINARY_DIR}/bin/vnsDummyProgram.sh
       )


add_test(NAME  pyTv-STARTMAJA-SPOT4-HRVIR2-L2INIT-CAMS_COMP_ASCII
       COMMAND ${TEST_DRIVER_MAJA}
       "--from-directory-compare-all-ascii" ${EPSILON_3}
       ${MAJADATA_TV_STARTMAJA_BASELINE}/SPOT4-HRVIR2-XS_20120802-071609-657_L2A_121-283-0_C_V1-0
       ${MAJADATA_TV_STARTMAJA_INPUT}/
       ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/121-283-0/SPOT4-HRVIR2-XS_20120802-071609-657_L2A_121-283-0_C_V1-0
       "--exclude-filename-with-key-matching" "8" "MAJAAdminConfig" "MAJAUserConfig" "PMC_LxREPT" "PMC_L1REPT" "PMC_L2REPT" "PMC_L3REPT" "perfos.xml" "JobOrder.xml"
       "--ignore-lines-with" "13" "PRODUCER" "PRODUCT_VERSION" "PROJECT" "PRODUCER_NAME" "Creator_Version" "Creation_Date" "Date_Time" "Chain_Version" "PRODUCTION_SOFTWARE" "Nature" "Logical_Name" "GIPP_Download_URL" "DATA_ACCESS"
       "--recursively" "true" "--output-directory"
       ${MAJA_TEST_OUTPUT_ROOT}/TV_START_MAJA/L2-Products/121-283-0/testing-compare-output-directory
       ${MAJA_BINARY_DIR}/bin/vnsDummyProgram.sh
       )


