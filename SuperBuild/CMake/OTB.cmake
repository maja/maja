#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
############################################################################################################
#                                                                                                          #
#                                        __  __    __     ____   __                                        #
#                                       (  \/  )  /__\   (_  _) /__\                                       #
#                                        )    (  /(__)\ .-_)(  /(__)\                                      #
#                                       (_/\/\_)(__)(__)\____)(__)(__)                                     #
#                                                                                                          #
#                                                                                                          #
############################################################################################################
# HISTORIQUE                                                                                               #
#                                                                                                          #
# VERSION : 3.1.0 : DM : LAIG-DM-MAJA-2526-CNES : 12 novembre 2024 : Montée de version de d'OTB 9.1        #
# VERSION : 3.1.0 : DM : LAIG-DM-MAJA-2526-CNES : 9 avril 2018 : Montée de version de d'OTB 6.0            #
#                                                                                                          #
# FIN-HISTORIQUE                                                                                           #
#                                                                                                          #
# $Id$                                                                                                     #
#                                                                                                          #
############################################################################################################

set(OTB_URL "https://www.orfeo-toolbox.org/packages/archives/OTB/OTB-9.1.0.tar.gz")
set(OTB_URL_MD5 5755c0f319ab374e3b433d1b15c96149)

set(OTB_DEPENDS GDAL ITK OPENJPEG GEOTIFF TINYXML MUPARSER BOOST SWIG NUMPY PYTHON)
build_projects(OTB_DEPENDS)

ExternalProject_Add(OTB
  URL "${OTB_URL}"
  URL_MD5 ${OTB_URL_MD5}
  PREFIX       OTB
  TMP_DIR      OTB/tmp
  STAMP_DIR    OTB/stamp
  SOURCE_DIR   OTB/source
  BINARY_DIR   OTB/build
  INSTALL_DIR  ${SB_INSTALL_PREFIX}
  DOWNLOAD_DIR ${DOWNLOAD_DIR}
  DEPENDS ${OTB_DEPENDS}
  CMAKE_COMMAND ${SB_CMAKE_COMMAND}
  CMAKE_CACHE_ARGS
  ${SB_CMAKE_CACHE_ARGS}
  ${DISABLE_CXX_WARNING_OPTION}
  -DOTB_BUILD_DEFAULT_MODULES:BOOL=OFF
  -DOTBGroup_Core:BOOL=ON #Only select modules in the core group but not build the entire OTB
  -DOTBGroup_FeaturesExtraction:BOOL=ON
  # Maja use application ComputeImagesStatistics present in Learning
  -DOTBGroup_Learning:BOOL=ON
  -DOTB_WRAP_PYTHON:BOOL=ON
  # we do not need QGIS wrapper. Using it leads to compilation
  # bug as it needs jpeg lib
  -DOTB_WRAP_QGIS:BOOL=OFF
  -DOTB_USE_MUPARSERX:BOOL=OFF
  -DOTB_USE_MUPARSER:BOOL=ON
  -DOTB_USE_SVM:BOOL=OFF
  -DOTB_USE_SHARK:BOOL=OFF
  -DOTB_USE_CURL:BOOL=ON
  -DOTB_USE_6S:BOOL=ON
  -DOTB_USE_SIFTFAST:BOOL=OFF
  -DSWIG_EXECUTABLE:PATH=${SB_INSTALL_PREFIX}/bin/swig
  -DPython_EXECUTABLE:PATH=${SB_INSTALL_PREFIX}/bin/python3
  -DBoost_NO_SYSTEM_PATHS:BOOL=ON
  -DUSE_SYSTEM_OPENSSL:BOOL=ON
  -DGDAL_CONFIG_CHECKING:BOOL=OFF #GDAL already built and tested successfully
  LOG_DOWNLOAD ${WRITE_LOG}
  LOG_CONFIGURE ${WRITE_LOG}
  LOG_BUILD ${WRITE_LOG}
  LOG_INSTALL ${WRITE_LOG}
)
SUPERBUILD_PATCH_SOURCE(OTB)


