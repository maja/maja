#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
############################################################################################################
#                                                                                                          #
#                                        __  __    __     ____   __                                        #
#                                       (  \/  )  /__\   (_  _) /__\                                       #
#                                        )    (  /(__)\ .-_)(  /(__)\                                      #
#                                       (_/\/\_)(__)(__)\____)(__)(__)                                     #
#                                                                                                          #
#                                                                                                          #
############################################################################################################
# HISTORIQUE                                                                                               #
#                                                                                                          #
# VERSION : 3.1.0 : DM : LAIG-DM-MAJA-2526-CNES : 9 avril 2018 : Montée de version de d'OTB 6.0            #
#                                                                                                          #
# FIN-HISTORIQUE                                                                                           #
#                                                                                                          #
# $Id$                                                                                                     #
#                                                                                                          #
############################################################################################################

set(GDAL_URL "https://github.com/OSGeo/gdal/releases/download/v3.9.0/gdal-3.9.0.tar.gz")
set(GDAL_URL_MD5 d3780907608f381a4f662cb1480c7f97)
set(GDAL_DEPENDS ZLIB EXPAT PNG PROJ JPEG TIFF GEOTIFF SQLITE OPENJPEG GEOS HDF4 NETCDF PYTHON SWIG)
set(__custom_sb_cache_args ${SB_CMAKE_CACHE_ARGS})

# as Cmake create compiler commands with -R that does not exists, remove cmake
# args that create them
list(FILTER __custom_sb_cache_args EXCLUDE REGEX "\-DCMAKE_INSTALL_RPATH\:.*")

# set(GDAL_AUTOCONF_BUILD 1)
build_projects(GDAL_DEPENDS)

list(APPEND GDAL_CONFIG_LIBS
            -DGDAL_USE_EXPAT:BOOL=ON
            -DGDAL_USE_EXTERNAL_LIBS:BOOL=ON
            -DGDAL_USE_GEOS:BOOL=ON
            -DGDAL_USE_GEOTIFF:BOOL=ON
            -DGDAL_USE_TIFF:BOOL=ON
            -DGDAL_USE_JPEG:BOOL=ON
            -DGDAL_USE_SQLITE3:BOOL=ON
            -DGDAL_USE_NETCDF:BOOL=ON
            -DGDAL_USE_ZLIB:BOOL=ON
            -DGDAL_USE_INTERNAL_LIBS:STRING=WHEN_NO_EXTERNAL
            -DGDAL_USE_HDF4:BOOL=ON
            -DGDAL_USE_HDF5:BOOL=ON
            -DGDAL_USE_PNG:BOOL=ON
            -DGDAL_USE_JSONC:BOOL=OFF
            -DGDAL_USE_JSONC_INTERNAL:BOOL=ON
            -DGDAL_USE_LIBKML:BOOL=OFF
            -DGDAL_USE_LERC:BOOL=ON
            -DGDAL_USE_CURL:BOOL=OFF)

ExternalProject_Add(GDAL
  PREFIX GDAL
  URL "${GDAL_URL}"
  URL_MD5 ${GDAL_URL_MD5}
  DEPENDS ${GDAL_DEPENDS}
  TMP_DIR      GDAL/tmp
  STAMP_DIR    GDAL/stamp
  SOURCE_DIR   GDAL/source
  BINARY_DIR   GDAL/build #uncomment for BUILD_IN_SOURCE 1
  INSTALL_DIR ${SB_INSTALL_PREFIX}
  DOWNLOAD_DIR ${DOWNLOAD_DIR}
  CMAKE_CACHE_ARGS
  ${__custom_sb_cache_args}
  -DBUILD_APPS:BOOL=ON
  -DBUILD_CSHARP_BINDINGS:BOOL=OFF
  -DBUILD_JAVA_BINDINGS:BOOL=OFF
  -DBUILD_PYTHON_BINDINGS:BOOL=ON
  -DBUILD_SHARED_LIBS:BOOL=ON
  -DBUILD_TESTING:BOOL=ON
  -DSQLite3_HAS_RTREE:BOOL=ON
  -DSQLite3_HAS_COLUMN_METADATA:BOOL=ON
  -DSQLite3_HAS_MUTEX_ALLOC:BOOL=ON
  -DCMAKE_INSTALL_PREFIX:PATH=${SB_INSTALL_PREFIX}
  -DPython_ROOT:PATH=${SB_INSTALL_PREFIX}
  -DGDAL_PYTHON_INSTALL_PREFIX:PATH=${SB_INSTALL_PREFIX}
  -DCMAKE_INSTALL_LIBDIR:STRING=lib
  -DCMAKE_PREFIX_PATH:PATH=${SB_INSTALL_PREFIX}
  CMAKE_COMMAND ${SB_CMAKE_COMMAND}
  LOG_DOWNLOAD ${WRITE_LOG}
  LOG_CONFIGURE ${WRITE_LOG}
  LOG_BUILD ${WRITE_LOG}
  LOG_INSTALL ${WRITE_LOG}
  )

ExternalProject_Add_Step(GDAL remove_deprecated_scripts
  COMMAND /bin/sh -x
  ${CMAKE_SOURCE_DIR}/patches/GDAL/remove_deprecated_scripts.sh
  ${SB_INSTALL_PREFIX}/bin
  DEPENDEES install
)

ExternalProject_Add_Step(GDAL post_install_pip
    COMMAND /bin/sh -x
    ${CMAKE_SOURCE_DIR}/patches/PYTHON/post_install_pip.sh
    ${SB_INSTALL_PREFIX}
    DEPENDEES install)

SUPERBUILD_PATCH_SOURCE(GDAL)
