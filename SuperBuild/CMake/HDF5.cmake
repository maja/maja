#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
############################################################################################################
#                                                                                                          #
#                                        __  __    __     ____   __                                        #
#                                       (  \/  )  /__\   (_  _) /__\                                       #
#                                        )    (  /(__)\ .-_)(  /(__)\                                      #
#                                       (_/\/\_)(__)(__)\____)(__)(__)                                     #
#                                                                                                          #
#                                                                                                          #
############################################################################################################
# HISTORIQUE                                                                                               #
#                                                                                                          #
# VERSION : 3.1.0 : DM : LAIG-DM-MAJA-2526-CNES : 9 avril 2018 : Montée de version de d'OTB 6.0            #
#                                                                                                          #
# FIN-HISTORIQUE                                                                                           #
#                                                                                                          #
# $Id$                                                                                                     #
#                                                                                                          #
############################################################################################################

set(HDF5_SB_CONFIG)
set(HDF5_DEPENDS ZLIB JPEG)
# When building HDF4 in debug, the lib names are suffixed with "_debug.so"
# and with this name, NETCDF can not find it. Thus compil this lib
# in release
set(__custom_sb_cmake_args ${SB_CMAKE_CACHE_ARGS})
list(REMOVE_ITEM __custom_sb_cmake_args "-DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}")
list(REMOVE_ITEM __custom_sb_cmake_args "-DCMAKE_BUILD_TYPE:STRING=Release")

ExternalProject_Add(HDF5
URL "https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.12/hdf5-1.12.3/src/hdf5-1.12.3.tar.gz"
URL_MD5 4da24fcd281b9eeb05dae9b258f72a72
  PREFIX       HDF5
  TMP_DIR      HDF5/tmp
  STAMP_DIR    HDF5/stamp
  SOURCE_DIR   HDF5/source
  BINARY_DIR   HDF5/build
  INSTALL_DIR ${SB_INSTALL_PREFIX}
  DOWNLOAD_DIR ${DOWNLOAD_DIR}
  DEPENDS ${HDF5_DEPENDENCIES}
  CMAKE_CACHE_ARGS
  ${__custom_sb_cmake_args}
    -DBUILD_TESTING:BOOL=OFF
    -DHDF5_BUILD_CPP_LIB:BOOL=OFF
    -DHDF5_BUILD_EXAMPLES:BOOL=OFF
    -DHDF5_BUILD_FORTRAN:BOOL=OFF
    -DHDF5_BUILD_HL_LIB:BOOL=ON
    -DHDF5_BUILD_JAVA:BOOL=OFF
    -DHDF5_BUILD_TOOLS:BOOL=OFF
    -DHDF5_ENABLE_SZIP_SUPPORT:BOOL=OFF
    -DHDF5_ENABLE_Z_LIB_SUPPORT:BOOL=ON
    –DH5_USE_110_API:BOOL=ON
    -DDEFAULT_API_VERSION:STRING=v110
    ${HDF5_SB_CONFIG}
  CMAKE_COMMAND ${SB_CMAKE_COMMAND}
  LOG_DOWNLOAD 1
  LOG_CONFIGURE 1
  LOG_BUILD 1
  LOG_INSTALL 1
  )
unset(__custom_sb_cmake_args)
