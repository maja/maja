// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-145739-CS : 27 juin 2016 : Audit code - Supp de la macro ITK_EXPORT   *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 22 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsConvertTxtToImageFilter_h
#define __vnsConvertTxtToImageFilter_h

#include "itkProcessObject.h"
#include "itkMacro.h"

namespace vns
{
    /** \class  ConvertTxtToImageFilter
     * \brief This filter convert a std::string into an otb::VectorImage of 3 channels.
     *
     *
     * \author CS Systemes d'Information
     *
     * \sa Object
     *
     * \ingroup L2
     * \ingroup L3
     * \ingroup Checktool
     *
     */
    template<class TOutputImage>
        class ConvertTxtToImageFilter : public itk::ProcessObject
        {
        public:
            /** Standard class typedefs. */
            typedef ConvertTxtToImageFilter Self;
            typedef itk::ProcessObject Superclass;
            typedef itk::SmartPointer<Self> Pointer;
            typedef itk::SmartPointer<const Self> ConstPointer;

            typedef TOutputImage OutputImageType;
            typedef typename OutputImageType::Pointer OutputImagePointer;

            /** Type macro */
            itkNewMacro(Self)
            /** Creation through object factory macro */
            itkTypeMacro(ConvertTxtToImageFilter, ProcessObject )

            /** Set/Get the Font filename */
            itkSetMacro(FontFileName, std::string)
            itkGetConstReferenceMacro(FontFileName, std::string);

        /** Set/Get the Font filename */
        itkSetMacro (FontSize, unsigned int)
        itkGetConstReferenceMacro(FontSize, unsigned int)

        /** string to convert into image */
        itkSetMacro(Text, std::string)
        itkGetConstReferenceMacro(Text, std::string)

        /** red channel value */
        itkSetMacro(Red, int)
        itkGetConstReferenceMacro(Red, int);

        /** green channel value */
        itkSetMacro(Green, int);
        itkGetConstReferenceMacro(Green, int);

        /** blue channel value */
        itkSetMacro(Blue, int);
        itkGetConstReferenceMacro(Blue, int);

        /** Set the used color */
        void SetColor( int redVal, int greenVal, int blueVal )
        {
            this->SetRed(redVal);
            this->SetGreen(greenVal);
            this->SetBlue(blueVal);
        }

        virtual OutputImagePointer GetOutputImage(void)
        {
            return m_OutputImage;
        }

        void Convert();

    protected:
        /** Constructor */
        ConvertTxtToImageFilter();
        /** Destructor */
        virtual ~ConvertTxtToImageFilter();
        /** PrintSelf method */
        virtual void PrintSelf(std::ostream& os, itk::Indent indent) const;

        //void my_draw_bitmap(FT_Bitmap & bitmap, FT_Int & x, FT_Int y);

    private:
        ConvertTxtToImageFilter(const Self&);//purposely not implemented
        void operator=(const Self&);//purposely not implemented

        /** Font file name */
        std::string m_FontFileName;

        unsigned int m_FontSize;
        std::string m_Text;

        OutputImagePointer m_OutputImage;

        int m_Red;
        int m_Green;
        int m_Blue;
    };

}
// End namespace vns

#ifndef VNS_MANUAL_INSTANTIATION
#include "vnsConvertTxtToImageFilter.txx"
#endif

#endif /* __vnsConvertTxtToImageFilter_h */
