// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 19 octobre 2016 : Audit code - Correction qualite         *
 * VERSION : 5.1.0 : DM : LAIG-DM-MAC-1769-CNES : 9 aout 2016 : Modification des checktools                 *
 * VERSION : 1-0-0-3 : DM : 251 : 17 aout 2011 : Modif. pour prise en compte DM 251 Checktools dans SMIGS     *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 14 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsExtractBase_h
#define __vnsExtractBase_h

#include "itkProcessObject.h"
#include "vnsMacro.h"
#include "itkImageRegion.h"

namespace vns
{
/** \class  ExtractBase
 * \brief Base class for Extract tool.
 * Contains method ComputeStats which loop over all pixel of the input image and call the virtual function ComputeIndexStat.
 * This virtual function must be overloaded in derived class.
 *
 * 
 *
 * \author CS Systemes d'Information
 *
 * \sa ProcessObject
 *
 * \ingroup L2
 * \ingroup L3
 * \ingroup Checktool
 *
 */


class ExtractBase : public itk::ProcessObject
{
public:
    /** Standard class typedefs. */
    typedef ExtractBase                     Self;
    typedef itk::ProcessObject              Superclass;
    typedef itk::SmartPointer<Self>         Pointer;
    typedef itk::SmartPointer<const Self>   ConstPointer;

    typedef itk::ImageRegion<2> RegionType;
    typedef RegionType::IndexType IndexType;
    typedef RegionType::IndexValueType IndexValueType;

    typedef std::vector<IndexType>                IndexVectorType;
    typedef std::vector<unsigned int>             UIVectorType;


    /** Type macro */
    itkNewMacro(Self);
    /** Creation through object factory macro */
    itkTypeMacro(ExtractBase, ProcessObject);

    /** Set/Get Macro */
    vnsUIntMemberAndGetConstReferenceMacro(Radius)
    vnsMemberAndSetAndGetConstReferenceMacro( Point, IndexType)
    vnsMemberAndSetAndGetConstReferenceMacro( ImageRegion, RegionType)

    void ComputeStats();

    // Error const value
    static const double ERROR_VALUE;

protected:
    /** Constructor */
    ExtractBase();
    /** Destructor */
    virtual ~ExtractBase() = default;
    /** PrintSelf method */
    virtual void PrintSelf(std::ostream& os, itk::Indent indent) const;

    virtual void ComputeIndexStat(IndexType & id);

private:
    ExtractBase(const Self&); //purposely not implemented
    void operator=(const Self&); //purposely not implemented

};

} // End namespace vns


#endif /* __vnsExtractBase_h */
