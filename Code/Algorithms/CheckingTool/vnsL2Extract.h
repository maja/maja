// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 2.0.0 : FA : LAIG-FA-MAJA-2156-CNES : 20 fevrier 2017 : Correction checktool si pixel en dehors*
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 03 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 5-1-3 : FA : LAIG-DM-MAC-1916-CNES : 15 septembre 2016 : bug calcul pourcentage si dans bord   *
 * VERSION : 5-1-0 : DM : LAIG-DM-MAC-1769-CNES : 6 juillet 2016 : Implémentation écart type checktools     *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-145739-CS : 27 juin 2016 : Audit code - Supp de la macro ITK_EXPORT   *
 * VERSION : 4-0-0 : FA : LAIG-FA-MAC-120653-CS : 3 avril 2014 : Correction de règles de qualité			*
 * VERSION : 3-0-0 : FA : LAIG-FA-MAC-371-CNES : 05 octobre 2012 : Correction qualite : TxCom et Vg         *
 * VERSION : 1-0-0-3 : DM : 251 : 17 aout 2011 : Modif. pour prise en compte DM 251 Checktools dans SMIGS     *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 16 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL2Extract_h
#define __vnsL2Extract_h

#include "vnsExtractBase.h"
#include "vnsMacro.h"
// #include "vnsCaching.h"
#include "itkImageRegionConstIterator.h"
#include "otbMultiChannelExtractROI.h"
#include "otbExtractROI.h"
#include "otbStreamingStatisticsImageFilter.h"
#include "vnsStreamingMaskConditionalStatisticsVectorImageFilter.h"
#include "otbExtractROI.h"
#include "itkAddImageFilter.h"
#include "otbImageToVectorImageCastFilter.h"

namespace vns
{
    /** \class  L2Extract
     * \brief Extract informations from L2 product.
     * These informations are computed for a list of Pixels (use SetListPixelLineId and SetListPixelColId)
     * and are computed on a neighborhood (if any).
     *  -SRE Means
     *  -FRE Means
     *  -AOT Means
     *  -VAP Means
     *  -Cloud Value
     *  -Water Value
     *  -Edge Value
     *
     * \author CS Systemes d'Information
     *
     * \sa ProcessObject
     *
     * \ingroup L2
     * \ingroup L3
     * \ingroup Checktool
     *
     */
    template<class TInputReflectanceImage, class TInputAmountImage, class TInputMaskCLDImage, class TInputMaskImage>
        class L2Extract : public ExtractBase
        {
        public:
            /** Standard class typedefs. */
            typedef L2Extract Self;
            typedef ExtractBase Superclass;
            typedef itk::SmartPointer<Self> Pointer;
            typedef itk::SmartPointer<const Self> ConstPointer;

            /** Some convenient typedefs. */
            typedef TInputReflectanceImage InputReflectanceImageType;
            typedef typename InputReflectanceImageType::ConstPointer InputReflectanceImageConstPointer;
            typedef typename InputReflectanceImageType::Pointer InputReflectanceImagePointer;
            typedef typename InputReflectanceImageType::PixelType InputReflectanceImagePixelType;
            typedef typename InputReflectanceImageType::InternalPixelType InputReflectanceImageInternalPixelType;
            typedef typename InputReflectanceImageType::IndexType IndexType;
            typedef typename InputReflectanceImageType::SizeType SizeType;
            typedef typename InputReflectanceImageType::RegionType RegionType;

            typedef TInputAmountImage InputAmountImageType;
            typedef typename InputAmountImageType::ConstPointer InputAmountImageConstPointer;
            typedef typename InputAmountImageType::Pointer InputAmountImagePointer;
            typedef typename InputAmountImageType::PixelType InputAmountImagePixelType;
            typedef typename InputAmountImageType::InternalPixelType InputAmountImageInternalPixelType;

            typedef TInputMaskCLDImage InputMaskCLDImageType;
            typedef typename InputMaskCLDImageType::ConstPointer InputMaskCLDImageConstPointer;
            typedef typename InputMaskCLDImageType::Pointer InputMaskCLDImagePointer;
            typedef typename InputMaskCLDImageType::PixelType InputMaskCLDImagePixelType;
            typedef typename InputMaskCLDImageType::InternalPixelType InputMaskCLDImageInternalPixelType;

            typedef TInputMaskImage InputMaskImageType;
            typedef typename InputMaskImageType::ConstPointer InputMaskImageConstPointer;
            typedef typename InputMaskImageType::Pointer InputMaskImagePointer;
            typedef typename InputMaskImageType::PixelType InputMaskImagePixelType;
            typedef typename InputMaskImageType::InternalPixelType InputMaskImageInternalPixelType;

            /** Internam typedef to cast amount images */
            typedef otb::VectorImage<InputAmountImagePixelType> InputAmountImageCastType;
            typedef typename InputAmountImageCastType::PixelType InputAmountImageCastPixelType;

            typedef std::vector<InputAmountImagePixelType> AmountVectorType;
            typedef std::vector<InputMaskImagePixelType> MaskVectorType;

            typedef otb::ExtractROI<InputMaskImagePixelType, InputMaskImagePixelType> ExtractROIMaskImageType;
            typedef typename ExtractROIMaskImageType::Pointer ExtractROIMaskImagePointer;

            typedef otb::ExtractROI<InputMaskCLDImageInternalPixelType, InputMaskImagePixelType> CLDExtractROIImageType;
            typedef typename CLDExtractROIImageType::Pointer CLDExtractROIImagePointer;

            typedef otb::ImageToVectorImageCastFilter<InputAmountImageType, InputAmountImageCastType> CasterType;

            // Statistics filters typedefs
            typedef otb::StreamingStatisticsImageFilter<InputMaskImageType> StatisticsMaskFilterType;
            typedef typename StatisticsMaskFilterType::Pointer StatisticsMaskFilterPointer;

            typedef itk::AddImageFilter<InputMaskImageType, InputMaskImageType, InputMaskImageType> AdderType;

            /** Type macro */
            itkNewMacro(Self)

            /** Creation through object factory macro */
            itkTypeMacro(L2Extract, ExtractBase )

            /** Set/Get NoData Macro */
            itkSetMacro(NoData, RealNoDataType)

            itkGetMacro(NoData, RealNoDataType)

            /** Set/Get NoData Macro */
            itkSetMacro(VAPNoData, RealNoDataType)

            itkGetMacro(VAPNoData, RealNoDataType)

            /** Set/Get NoData Macro */
            itkSetMacro(AOTNoData, RealNoDataType)

            itkGetMacro(AOTNoData, RealNoDataType)

            /** Set/Get Macro */
            itkSetMacro(VapRadius, unsigned int)

            itkGetMacro(VapRadius, unsigned int)

            /** Set/Get Macro */
            itkSetMacro(AOTRadius, unsigned int)

            itkGetMacro(AOTRadius, unsigned int)

            /** Set/Get Macro */
            itkSetMacro(UseFRE, bool)

            itkGetMacro(UseFRE, bool)

            itkSetObjectMacro(VAPImage, InputAmountImageType)

            itkSetObjectMacro(AOTImage, InputAmountImageType)

            itkSetObjectMacro(CLDImage, InputMaskCLDImageType)

            itkSetObjectMacro(SREImage, InputReflectanceImageType)

            itkSetObjectMacro(FREImage, InputReflectanceImageType)

            itkSetObjectMacro(WATImage, InputMaskImageType)

            itkSetObjectMacro(EDGImage, InputMaskImageType)

            //Get output vector
            vnsGetConstReferenceMacro(SREMeans, InputReflectanceImagePixelType )

            vnsGetConstReferenceMacro(FREMeans, InputReflectanceImagePixelType )

            vnsGetConstReferenceMacro(AOTMeans, InputAmountImagePixelType )

            vnsGetConstReferenceMacro(VAPMeans, InputAmountImagePixelType )

            vnsGetConstReferenceMacro(SREStDv, InputReflectanceImagePixelType )

            vnsGetConstReferenceMacro(FREStDv, InputReflectanceImagePixelType )

            vnsGetConstReferenceMacro(AOTStDv, InputAmountImagePixelType )

            vnsGetConstReferenceMacro(VAPStDv, InputAmountImagePixelType )

            vnsGetConstReferenceMacro(UseFulPixelsPercentage, InputReflectanceImagePixelType )

            vnsGetConstReferenceMacro(AOTUseFulPixelsPercentage, InputAmountImagePixelType )

            vnsGetConstReferenceMacro(VAPUseFulPixelsPercentage, InputAmountImagePixelType )

            vnsGetConstReferenceMacro(ValidPixelsPercentage, InputReflectanceImagePixelType )

            vnsGetConstReferenceMacro(AOTValidPixelsPercentage, InputAmountImagePixelType )

            vnsGetConstReferenceMacro(VAPValidPixelsPercentage, InputAmountImagePixelType )

            vnsGetConstReferenceMacro(CloudValR1, InputMaskImagePixelType )

            vnsGetConstReferenceMacro(CloudValR2, InputMaskImagePixelType )

            vnsGetConstReferenceMacro(CloudValR3, InputMaskImagePixelType )
            vnsGetConstReferenceMacro(WaterValR1, InputMaskImagePixelType )
            vnsGetConstReferenceMacro(WaterValR2, InputMaskImagePixelType )
            vnsGetConstReferenceMacro(WaterValR3, InputMaskImagePixelType )
            vnsGetConstReferenceMacro(EdgeValR1, InputMaskImagePixelType )
            vnsGetConstReferenceMacro(EdgeValR2, InputMaskImagePixelType )
            vnsGetConstReferenceMacro(EdgeValR3, InputMaskImagePixelType )

            /** Get if the SRE statistics of the band is valid */
            itkGetConstMacro(SREStatIsValid,bool)
            /** Get if the FRE statistics of the band is valid */
            itkGetConstMacro(FREStatIsValid,bool)
            /** Get if the VAP statistics of the band is valid */
            itkGetConstMacro(VAPStatIsValid,bool)
            /** Get if the AOT statistics of the band is valid */
            itkGetConstMacro(AOTStatIsValid,bool)

        protected:
            /** Constructor */
            L2Extract();
            /** Destructor */
            ~L2Extract() = default;
            /** PrintSelf method */
            virtual void
            PrintSelf(std::ostream& os, itk::Indent indent) const;
            virtual void
            ComputeIndexStat(IndexType & id);

            // function templates
            template<class localImageType>
                bool
                computeROIStats(unsigned int & pRadius, IndexType & id, typename localImageType::Pointer pImage,
                        typename localImageType::PixelType & pMean, typename localImageType::PixelType & pStDv,
                        typename localImageType::PixelType & pUsefulPixelPerc, typename localImageType::PixelType & pValidPixelPerc,
                        InputMaskImagePixelType & pCloudVal, InputMaskImagePixelType & pWaterVal, InputMaskImagePixelType & pEdgeVal,
                        const RealNoDataType & pNoData)
                {

                    // typedefs
                    typedef typename localImageType::PixelType LocalPixelType;
                    typedef typename localImageType::InternalPixelType LocalInternalPixelType;

                    typedef otb::MultiChannelExtractROI<LocalInternalPixelType, LocalInternalPixelType> ExtractROILocalImageType;
                    typedef typename ExtractROILocalImageType::Pointer ExtractROILocalImagePointer;

                    typedef StreamingMaskConditionalStatisticsVectorImageFilter<localImageType, InputMaskImageType> LocalStatisticsImageFilterType;
                    typedef typename LocalStatisticsImageFilterType::Pointer LocalStatisticsImageFilterPointer;

                    // Initialization of the returned boolean
                    bool l_StatsValid(false);

                    // Store the number of band of the image
                    const unsigned l_Size(pImage->GetNumberOfComponentsPerPixel());

                    //compute upper left point and size of the extract using m_Radius.
                    // Check the validity of input the region
                    const int startX = id[0] - pRadius - 1; // -1 Because Index coordinate start to [1 ...]
                    const int startY = id[1] - pRadius - 1; // -1 Because Index coordinate start to [1 ...]
                    const unsigned int sizeX = 2 * pRadius + 1;
                    const unsigned int sizeY = sizeX;

                    // Initialize the region
                    RegionType l_ExtractedRegion;
                    IndexType idROI;
                    idROI[0] = startX;
                    idROI[1] = startY;
                    l_ExtractedRegion.SetIndex(idROI);
                    SizeType size;
                    size[0] = sizeX;
                    size[1] = sizeY;
                    l_ExtractedRegion.SetSize(size);
                    vnsLogDebugMacro("ExtractedRegion : idROI  : "<<idROI)
                    vnsLogDebugMacro("                  size   : "<<size)
                    vnsLogDebugMacro("                  NoData : "<<pNoData)
                    RegionType l_Region = m_EDGImage->GetLargestPossibleRegion();
                    vnsLogSuperDebugMacro("InputRegion m_EDGImage    : "<<l_Region)
                    vnsLogSuperDebugMacro("InputRegion pImage        : "<<pImage)

                    //Set to default value
                    pCloudVal = 0;
                    pWaterVal = 0;
                    pEdgeVal = 0;
                    // mean
                    pMean = LocalPixelType(l_Size);
                    pMean.Fill(ERROR_VALUE);
                    // standard deviation
                    pStDv = LocalPixelType(l_Size);
                    pStDv.Fill(0);
                    // Useful pixel perc
                    pUsefulPixelPerc = LocalPixelType(l_Size);
                    pUsefulPixelPerc.Fill(0);
                    // Useful pixel perc
                    pValidPixelPerc = LocalPixelType(l_Size);
                    pValidPixelPerc.Fill(0);

                    // Check if the region is inside the input image largest possible region
                    if (l_ExtractedRegion.Crop(l_Region) != false)
                    {
                        // Extract the selected region the different masks of the L1 product
                        //this is made with MultiChannelExtractROI and not NeighborrhoodIterator
                        //to avoid load in memory the entire image.
                        // => edge mask
                        ExtractROIMaskImagePointer l_EDGExtractROI = ExtractROIMaskImageType::New();
                        l_EDGExtractROI->SetInput(m_EDGImage);
                        l_EDGExtractROI->SetExtractionRegion(l_ExtractedRegion);

                        // => water mask
                        ExtractROIMaskImagePointer l_WATExtractROI = ExtractROIMaskImageType::New();
                        l_WATExtractROI->SetInput(m_WATImage);
                        l_WATExtractROI->SetExtractionRegion(l_ExtractedRegion);

                        // => cloud mask
                        CLDExtractROIImagePointer l_CLDExtractROI = CLDExtractROIImageType::New();
                        l_CLDExtractROI->SetInput(m_CLDImage);
                        l_CLDExtractROI->SetExtractionRegion(l_ExtractedRegion);

                        // Extract the ROI on the surface reflectance image
                        ExtractROILocalImagePointer l_MultiChannelExtractROI = ExtractROILocalImageType::New();
                        l_MultiChannelExtractROI->SetInput(pImage);
                        l_MultiChannelExtractROI->SetExtractionRegion(l_ExtractedRegion);

                        // Create new mask that contains the water mask, the EDG mask and the cloud mask
                        typename AdderType::Pointer l_Adder1 = AdderType::New();
                        l_Adder1->SetInput1(l_WATExtractROI->GetOutput());
                        l_Adder1->SetInput2(l_EDGExtractROI->GetOutput());
                        typename AdderType::Pointer l_Adder2 = AdderType::New();
                        l_Adder2->SetInput1(l_Adder1->GetOutput());
                        l_Adder2->SetInput2(l_CLDExtractROI->GetOutput());

                        //vnsCrocodileClipMacro(InputMaskImageType, l_WATExtractROI->GetOutput(), "l_WATExtractROI.tif")
                        //vnsCrocodileClipMacro(InputMaskImageType, l_EDGExtractROI->GetOutput(), "l_EDGExtractROI.tif")
                        //vnsCrocodileClipMacro(InputMaskImageType, l_CLDExtractROI->GetOutput(), "l_CLDExtractROI.tif")
                        //vnsCrocodileClipMacro(InputMaskImageType, l_Adder2->GetOutput(), "Water_EDG_Cloud.tif")



                        // Compute mask statistics
                        StatisticsMaskFilterPointer l_CLDStat = StatisticsMaskFilterType::New();
                        StatisticsMaskFilterPointer l_WATStat = StatisticsMaskFilterType::New();
                        StatisticsMaskFilterPointer l_EDGStat = StatisticsMaskFilterType::New();

                        LocalStatisticsImageFilterPointer l_localImageStat = LocalStatisticsImageFilterType::New();

                        // Compute statistics of the masks
                        l_CLDStat->SetInput(l_CLDExtractROI->GetOutput());
                        l_WATStat->SetInput(l_WATExtractROI->GetOutput());
                        l_EDGStat->SetInput(l_EDGExtractROI->GetOutput());

                        vnsLogImageInfoDebugMacro("l_MultiChannelExtractROI->GetOutput()", l_MultiChannelExtractROI->GetOutput())
                        vnsLogImageInfoDebugMacro("l_WATExtractROI->GetOutput()", l_WATExtractROI->GetOutput())
                        vnsLogImageInfoDebugMacro("l_EDGExtractROI->GetOutput()", l_EDGExtractROI->GetOutput())
                        vnsLogImageInfoDebugMacro("l_CLDExtractROI->GetOutput()", l_CLDExtractROI->GetOutput())
                        vnsLogImageInfoDebugMacro("l_Adder1->GetOutput()", l_Adder1->GetOutput())
                        vnsLogImageInfoDebugMacro("l_Adder2->GetOutput()", l_Adder2->GetOutput())

                        // Compute statistcis of the surface reflectance image taking into account the new mask
                        l_localImageStat->SetInput(l_MultiChannelExtractROI->GetOutput());
                        l_localImageStat->SetMaskInput(l_Adder2->GetOutput());
                        l_localImageStat->SetForegroundValue(0);
                        l_localImageStat->SetEnableStandardDeviation(true);
                        l_localImageStat->SetEnableSecondOrderStats(false);
                        l_localImageStat->SetEnableExcludeValue(true);
                        l_localImageStat->SetExcludeValue(pNoData);

                        l_CLDStat->Update();
                        l_WATStat->Update();
                        l_EDGStat->Update();
                        vnsLogDebugMacro("Number of point in EDG : "<<l_EDGStat->GetSum()<<".")
                        l_localImageStat->Update();

                        // Get the surface reflectance mean
                        pMean = l_localImageStat->GetMean();
                        pStDv = l_localImageStat->GetStandardDeviation();
                        //Percentage of useful/valid pixels
                        pUsefulPixelPerc = (l_localImageStat->GetNbOfValidValuesPerBand() * 100.0) / (sizeX * sizeY);
                        LocalPixelType l_nbExcludedValuePerBand(l_Size);
                        l_nbExcludedValuePerBand.Fill(0);
                        l_nbExcludedValuePerBand = l_localImageStat->GetNbOfExcludedValuesPerBand()
                                - l_localImageStat->GetNbOfExcludedValues();
                        for (unsigned int i = 0; i < l_Size; i++)
                        {
                            if (vnsEqualsDoubleMacro(pUsefulPixelPerc[i], 0))
                            {
                                pMean[i] = ERROR_VALUE;
                                pStDv[i] = 0;
                            }
                            if (l_ExtractedRegion.GetNumberOfPixels() > (l_nbExcludedValuePerBand[i] + l_EDGStat->GetSum()))
                            {
                                //Number of valid pixel / (pixel in the image - no_data - edge)
                                pValidPixelPerc[i] = l_localImageStat->GetNbOfValidValuesPerBand()[i] * 100.0
                                        / (l_ExtractedRegion.GetNumberOfPixels() - l_nbExcludedValuePerBand[i] - l_EDGStat->GetSum());
                            }
                        }

                        //Stats is valid ?
                        l_StatsValid = l_localImageStat->GetIsValid();

                        // Check if at least one pixel is cloud, water or edge in the region
                        if (l_CLDStat->GetSum() > 0)
                        {
                            pCloudVal = 1;
                        }
                        else
                        {
                            pCloudVal = 0;
                        }
                        if (l_WATStat->GetSum() > 0)
                        {
                            pWaterVal = 1;
                        }
                        else
                        {
                            pWaterVal = 0;
                        }
                        if (l_EDGStat->GetSum() > 0)
                        {
                            pEdgeVal = 1;
                        }
                        else
                        {
                            pEdgeVal = 0;
                        }
                    }
                    return l_StatsValid;

                } // end computeROIStats

        private:
            L2Extract(const Self&); //purposely not implemented
            void
            operator=(const Self&); //purposely not implemented

            unsigned int m_VapRadius;
            unsigned int m_AOTRadius;

            //input Images
            InputAmountImagePointer m_VAPImage;
            InputAmountImagePointer m_AOTImage;
            InputMaskCLDImagePointer m_CLDImage;
            InputReflectanceImagePointer m_SREImage;
            InputReflectanceImagePointer m_FREImage;
            InputMaskImagePointer m_WATImage;
            InputMaskImagePointer m_EDGImage;
            //No Data
            RealNoDataType m_NoData;
            RealNoDataType m_VAPNoData;
            RealNoDataType m_AOTNoData;

            //outputs vector
            InputReflectanceImagePixelType m_SREMeans;
            InputReflectanceImagePixelType m_FREMeans;
            InputAmountImagePixelType m_VAPMeans;
            InputAmountImagePixelType m_AOTMeans;
            InputReflectanceImagePixelType m_SREStDv;
            InputReflectanceImagePixelType m_FREStDv;
            InputAmountImagePixelType m_VAPStDv;
            InputAmountImagePixelType m_AOTStDv;
            InputReflectanceImagePixelType m_UseFulPixelsPercentage;
            InputAmountImagePixelType m_VAPUseFulPixelsPercentage;
            InputAmountImagePixelType m_AOTUseFulPixelsPercentage;
            InputReflectanceImagePixelType m_ValidPixelsPercentage;
            InputAmountImagePixelType m_VAPValidPixelsPercentage;
            InputAmountImagePixelType m_AOTValidPixelsPercentage;
            InputMaskImagePixelType m_WaterValR1;
            InputMaskImagePixelType m_WaterValR2;
            InputMaskImagePixelType m_WaterValR3;
            InputMaskImagePixelType m_CloudValR1;
            InputMaskImagePixelType m_CloudValR2;
            InputMaskImagePixelType m_CloudValR3;
            InputMaskImagePixelType m_EdgeValR1;
            InputMaskImagePixelType m_EdgeValR2;
            InputMaskImagePixelType m_EdgeValR3;
            bool m_UseFRE;

            /** Check if the SRE statistics of the band is valid */
            bool m_SREStatIsValid;
            /** Check if the FRE statistics of the band is valid */
            bool m_FREStatIsValid;
            /** Check if the VAP statistics of the band is valid */
            bool m_VAPStatIsValid;
            /** Check if the AOT statistics of the band is valid */
            bool m_AOTStatIsValid;
        };

} // End namespace vns
#ifndef VNS_MANUAL_INSTANTIATION
#include "vnsL2Extract.txx"
#endif

#endif /* __vnsL2Extract_h */
