// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-145739-CS : 27 juin 2016 : Audit code - Supp de la macro ITK_EXPORT   *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 31 août 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL2QLExtractEdge_h
#define __vnsL2QLExtractEdge_h

#include "vnsMacro.h"
#include "itkProcessObject.h"
#include "itkShiftScaleImageFilter.h"
#include "itkGradientMagnitudeImageFilter.h"

namespace vns
{
/** \class  L2QLExtractEdge
 * \brief This filters extracts the edges of an input image
 *
 * 
 *
 * \author CS Systemes d'Information
 *
 * \sa ImageToImageFilter
 *
 * \ingroup L2
 * \ingroup L3
 * \ingroup Checktool
 *
 */
template <class TInputImage, class TOutputImage>
class L2QLExtractEdge : public itk::ProcessObject
{
public:
    /** Standard class typedefs. */
    typedef L2QLExtractEdge                                        Self;
    typedef itk::ProcessObject      Superclass;
    typedef itk::SmartPointer<Self>                                Pointer;
    typedef itk::SmartPointer<const Self>                          ConstPointer;

    /** Type macro */
    itkNewMacro(Self);
    /** Creation through object factory macro */
    itkTypeMacro(L2QLExtractEdge, ProcessObject );

    /** Some convenient typedefs. */
    typedef TInputImage   InputImageType;
    typedef typename InputImageType::ConstPointer InputImageConstPointer;
    typedef typename InputImageType::RegionType   RegionType;
    typedef typename InputImageType::PixelType    InputImagePixelType;
    typedef typename InputImageType::InternalPixelType    InputImageInternalPixelType;
    typedef typename InputImageType::SizeType     SizeType;
    typedef TOutputImage  OutputImageType;
    typedef typename OutputImageType::Pointer     OutputImagePointer;
    typedef typename OutputImageType::PixelType   OutputImagePixelType;
    typedef typename OutputImageType::InternalPixelType    OutputImageInternalPixelType;

    typedef itk::ShiftScaleImageFilter<OutputImageType,OutputImageType>                  ShiftScaleImageFilterType;
    typedef itk::GradientMagnitudeImageFilter<OutputImageType,OutputImageType>           GradientMagnitudeImageFilterType;

    typedef typename ShiftScaleImageFilterType::Pointer ShiftScaleImageFilterPointerType;
    typedef typename ShiftScaleImageFilterType::RealType RealType;
    typedef typename GradientMagnitudeImageFilterType::Pointer GradientMagnitudeImageFilterPointerType;


    otbSetObjectMemberMacro(ShiftScaleImageFilter,Input, InputImageType *)
    otbGetObjectMemberMacro(GradientMagnitudeImageFilter,Output, OutputImageType *)

    otbSetObjectMemberMacro(ShiftScaleImageFilter,Shift, RealType)
    otbGetObjectMemberMacro(ShiftScaleImageFilter,Shift, RealType)

    otbSetObjectMemberMacro(ShiftScaleImageFilter,Scale, RealType)
    otbGetObjectMemberMacro(ShiftScaleImageFilter,Scale, RealType)

    void UpdateData(void);

protected:
    /** Constructor */
    L2QLExtractEdge();
    /** Destructor */
    virtual ~L2QLExtractEdge();

    /** PrintSelf method */
    virtual void PrintSelf(std::ostream& os, itk::Indent indent) const;

private:
    L2QLExtractEdge(const Self&); //purposely not implemented
    void operator=(const Self&); //purposely not implemented

    ShiftScaleImageFilterPointerType m_ShiftScaleImageFilter;
    GradientMagnitudeImageFilterPointerType m_GradientMagnitudeImageFilter;

};

} // End namespace vns
#ifndef VNS_MANUAL_INSTANTIATION
#include "vnsL2QLExtractEdge.txx"
#endif

#endif /* __vnsL2QLExtractEdge_h */
