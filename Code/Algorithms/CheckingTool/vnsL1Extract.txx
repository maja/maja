// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 2.0.0 : DM : LAIG-DM-MAJA-153436-CS : 29 mars 2017 : Refactoring pour ameliorer la qualite     *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 03 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 5-1-0 : DM : LAIG-DM-MAC-1755-CNES : 6 juillet 2016 : Implémentation pourcentages utils/valids *
 * VERSION : 5-1-0 : DM : LAIG-DM-MAC-1769-CNES : 6 juillet 2016 : Implémentation écart type checktools     *
 * VERSION : 4-0-0 : FA : LAIG-FA-MAC-120653-CS : 3 avril 2014 : Correction de règles de qualité			*
 * VERSION : 1-0-0-3 : DM : 251 : 17 aout 2011 : Modif. pour prise en compte DM 251 Checktools dans SMIGS     *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 15 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL1Extract_txx
#define __vnsL1Extract_txx

#include "vnsL1Extract.h"

namespace vns
{

    /** Constructor */
    template<class TInputImage, class TPIXInputMask, class TCLDInputMask>
    L1Extract<TInputImage, TPIXInputMask, TCLDInputMask>::L1Extract() :
    m_NoData(0),m_CloudVal(0),m_MissingDataVal(0)
    {
        //instantiate filter
        m_TOAImage = InputImageType::New();
        m_CLDMask = CLDInputMaskType::New();
        m_PIXMask = PIXInputMaskType::New();
        m_TOAStatIsValid = false;

    }

    /** Destructor */
    template<class TInputImage, class TPIXInputMask, class TCLDInputMask>
    L1Extract<TInputImage, TPIXInputMask, TCLDInputMask>::~L1Extract()
    {
    }

    template<class TInputImage, class TPIXInputMask, class TCLDInputMask>
    void
    L1Extract<TInputImage, TPIXInputMask, TCLDInputMask>::ComputeIndexStat(IndexType & id)
    {
        // Update input image information
        m_TOAImage->UpdateOutputInformation();

        //Get Radius
        const unsigned int radius(this->GetRadius());

        // Store the number of band of the TOA reflectance image
        const unsigned l_Size(m_TOAImage->GetNumberOfComponentsPerPixel());

        // Check work region availability
        m_TOAImage->UpdateOutputInformation();
        RegionType l_Region = m_TOAImage->GetLargestPossibleRegion();
        m_PIXMask->UpdateOutputInformation();
        m_CLDMask->UpdateOutputInformation();

        if (l_Region != m_PIXMask->GetLargestPossibleRegion() || l_Region != m_CLDMask->GetLargestPossibleRegion())
        {
            vnsExceptionBusinessMacro("Input image size mismatch (PIX: "<<m_PIXMask->GetLargestPossibleRegion().GetSize()<<", CLD: "<<m_CLDMask->GetLargestPossibleRegion().GetSize()<<" TOA: "<<l_Region.GetSize()<<")" );
        }

        const int startX = id[0] - radius - 1; // -1 Because Index coordinate start to [1 ...];
        const int startY = id[1] - radius - 1; // -1 Because Index coordinate start to [1 ...];
        unsigned int sizeX = 2 * radius + 1;
        unsigned int sizeY = 2 * radius + 1;

        // Initialization
        RegionType l_ExtractedRegion;
        IndexType idROI;
        idROI[0] = startX;
        idROI[1] = startY;
        l_ExtractedRegion.SetIndex(idROI);
        SizeType size;
        size[0] = sizeX;
        size[1] = sizeY;
        l_ExtractedRegion.SetSize(size);

        // TOA reflectance mean
        m_TOAMeans = PixelType(l_Size);
        m_TOAMeans.Fill(ERROR_VALUE);
        // TOA reflectance standard deviation
        m_TOAStDv = PixelType(l_Size);
        m_TOAStDv.Fill(0);
        //UsefulPixelPerc
        m_UsefulPixelPerc = PixelType(l_Size);
        m_UsefulPixelPerc.Fill(0);
        //UsefulPixelPerc
        m_ValidsPixelPerc = PixelType(l_Size);
        m_ValidsPixelPerc.Fill(0);
        m_TOAStatIsValid = false;

        // Check if at least one pixel is cloud or aberrant in the region
        m_CloudVal = 0;
        m_MissingDataVal = 1;

        // If the extracted region is outside the input largest possible region
        if (l_ExtractedRegion.Crop(l_Region) != false)
        {
            // Extract the selected region in each plan of the L1 product
            // Link extractors
            typename CLDExtractROIType::Pointer l_cldExtract = CLDExtractROIType::New();
            typename PIXExtractROIType::Pointer l_pixExtract = PIXExtractROIType::New();
            MultiChannelExtractROIPointer l_toaExtract = MultiChannelExtractROIType::New();

            // TOA
            l_toaExtract->SetInput(m_TOAImage);
            l_toaExtract->SetExtractionRegion(l_ExtractedRegion);

            // PIX
            l_pixExtract->SetInput(m_PIXMask);
            l_pixExtract->SetExtractionRegion(l_ExtractedRegion);

            // CLD
            l_cldExtract->SetInput(m_CLDMask);
            l_cldExtract->SetExtractionRegion(l_ExtractedRegion);

            // Concatenate the aberrant pixels masks (PIX) and the cloud mask (CLD)
            typename AdderType::Pointer l_Adder = AdderType::New();
            l_Adder->SetInput1(l_pixExtract->GetOutput());
            l_Adder->SetInput2(l_cldExtract->GetOutput());

            // Compute statistics
            StatisticsMaskFilterPointer l_pixStater = StatisticsMaskFilterType::New();
            StatisticsMaskFilterPointer l_cldStater = StatisticsMaskFilterType::New();
            StatisticsImageFilterPointer l_toaStater = StatisticsImageFilterType::New();

            l_pixStater->SetInput(l_pixExtract->GetOutput());
            l_cldStater->SetInput(l_cldExtract->GetOutput());

            l_toaStater->SetInput(l_toaExtract->GetOutput());
            l_toaStater->SetMaskInput(l_Adder->GetOutput());
            l_toaStater->SetForegroundValue(0);
            l_toaStater->SetEnableStandardDeviation(true);
            l_toaStater->SetEnableSecondOrderStats(false);
            l_toaStater->SetEnableExcludeValue(true);
            l_toaStater->SetExcludeValue(m_NoData);

            // Launch stat computation
            l_pixStater->Update();
            l_cldStater->Update();
            l_toaStater->Update();

            //Nominal case: more than one util point
            if ( l_toaStater->GetNbOfValidValues() > 0 ) {
                // TOA reflectance mean
                m_TOAMeans = l_toaStater->GetMean();
                // TOA reflectance standard deviation
                m_TOAStDv = l_toaStater->GetStandardDeviation();
                m_TOAStatIsValid = l_toaStater->GetIsValid();

                // Check if at least one pixel is cloud or aberrant in the region
                if (l_cldStater->GetSum() > 0)
                {
                    m_CloudVal = 1;
                }
                else
                {
                    m_CloudVal = 0;
                }
                if (l_pixStater->GetSum() > 0)
                {
                    m_MissingDataVal = 1;
                }
                else
                {
                    m_MissingDataVal = 0;
                }

                //Percentage of useful/valid pixels
                m_UsefulPixelPerc = (l_toaStater->GetNbOfValidValuesPerBand() * 100.0 ) / (sizeX*sizeY);
                PixelType l_nbExcludedValuePerBand(l_Size);
                l_nbExcludedValuePerBand.Fill(0);
                l_nbExcludedValuePerBand = l_toaStater->GetNbOfExcludedValuesPerBand() - l_toaStater->GetNbOfExcludedValues();
                for (unsigned int i = 0; i < l_Size ; i++) {
                    if (vnsEqualsDoubleMacro(m_UsefulPixelPerc[i], 0)) {
                        m_TOAMeans[i] = ERROR_VALUE;
                        m_TOAStDv[i] = 0;
                    }
                    if (l_ExtractedRegion.GetNumberOfPixels() > l_nbExcludedValuePerBand[i]) {
                        m_ValidsPixelPerc[i] = l_toaStater->GetNbOfValidValuesPerBand()[i] *100.0 / (l_ExtractedRegion.GetNumberOfPixels()-l_nbExcludedValuePerBand[i]);
                    }
                }

            }
        }
    }

    /** PrintSelf method */
    template<class TInputImage, class TPIXInputMask, class TCLDInputMask>
    void
    L1Extract<TInputImage, TPIXInputMask, TCLDInputMask>::PrintSelf(std::ostream& os, itk::Indent indent) const
    {
        this->Superclass::PrintSelf(os, indent);
    }

} // End namespace vns

#endif /* __vnsL1Extract_txx */
