/*
 * Copyright (C) 2022 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
// clang-format off
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS GROUP - France                                                                                *
 *                                                                                                          *
 ************************************************************************************************************/

#ifndef __vnsApplyOffsetScaleFunctor_h
#define __vnsApplyOffsetScaleFunctor_h

#include "itkVariableLengthVector.h"
#include "array"

namespace vns
{
namespace Functor
{

/** \class  ApplyOffsetScaleFunctor
 * \brief This class applies a scale factor and an offset to each pixel of an image.
 *
 * Applies this formula to each pixel of the input image:
 *
 * outPixel = offset + scale_factor * inPixel
 *
 * Need to call SetScaleFactor() and SetOffset() before using the functor.
 *
 * Input and output images are of type otb::VectorImage<double>.
 *
 * \author CS GROUP - France
 *
 * \ingroup L2
 *
 */
class ApplyOffsetScaleFunctor
{
public:
  ApplyOffsetScaleFunctor()
    : m_ScaleFactor(1),
      m_Offset(0)
  {
  }

  virtual ~ApplyOffsetScaleFunctor()
  {
  }

  /** Set the Scale Factor */
  void SetScaleFactor(double scaleFactor)
  {
    this->m_ScaleFactor = scaleFactor;
  }

  void SetOffset(double offset)
  {
    this->m_Offset = offset;
  }

  inline itk::VariableLengthVector<double> operator()(const itk::VariableLengthVector<double>& inPix) const
  {
    return m_Offset + m_ScaleFactor * inPix;
  }

  size_t OutputSize(const std::array<size_t, 1> & nbBands) const
  {
    return nbBands[0];
  }

private:
  /** parameters declaration */
  double m_ScaleFactor;
  double m_Offset;
};

}
}

#endif /* __vnsApplyOffsetScaleFunctor_h */
