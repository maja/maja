/*
 * Copyright (C) 2022 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
// clang-format off
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS GROUP - France                                                                                *
 *                                                                                                          *
 ************************************************************************************************************/

#ifndef __vnsEra5SpecificHumiditiesFunctor_h
#define __vnsEra5SpecificHumiditiesFunctor_h

#include "vnsMacro.h"
#include "vnsMath.h"
#include "itkVariableLengthVector.h"
#include "otbStringUtils.h"
#include "vector"

namespace vns
{
namespace Functor
{

namespace
{
/** Coefficient to apply to the DEM to compute the pressure estimate \f$ =\frac{0.0065}{288.15} \f$  */
constexpr double PRESSURE_COEF = 0.0065 / 288.15;
}

/** \class  Era5SpecificHumiditiesFunctor
 * \brief This class computes the water vapor from the specific humidity.
 *
 * Input image 1 contains the speific humidity.
 *
 * Input image 2 is the DEM.
 *
 * Call SetPressureLevels() to provide the pressure levels.
 *
 * Input images are of type otb::VectorImage<double>.
 * Output image is of type otb::VectorImage<float>.
 *
 * \author CS GROUP - France
 *
 * \ingroup L2
 *
 */
class Era5SpecificHumiditiesFunctor
{
public:
  using PressureLevelsType = std::vector<int>;

  Era5SpecificHumiditiesFunctor()
  {
  }

  virtual ~Era5SpecificHumiditiesFunctor()
  {
  }

  /** Set the pressure levels
   *
   * \param[in] pressureLevels: an std::vector containing the pressure levels.
   */
  void SetPressureLevels(PressureLevelsType pressureLevels)
  {
    m_PressureLevels = pressureLevels;
  }

  /** Set the pressure levels from a string
   *
   * \param[in] pressureLevels: a string containing the pressure levels comma separated.
   */
  void SetPressureLevels(std::string pressureLevels)
  {
    otb::Utils::ConvertStringToVector(pressureLevels,
                                      m_PressureLevels,
                                      "Pressure levels",
                                      ",");
  }

  inline double operator()(const itk::VariableLengthVector<double>& inPix,
                           const itk::VariableLengthVector<float>& dem) const
  {
    // Estimate pressure as function of altitude (hPa)
    double pressureEstimate = 1013.25 * vcl_pow(1 - PRESSURE_COEF * dem[0], 5.31);

    // Initialize the water vapor
    double waterVapor = 0;
    double deltaPressure;

    // Loop over the pressure levels
    for(unsigned int plId = 0 ; plId < inPix.Size() - 1 ; ++plId)
    {
      // Estimated pressure value is higher than pressure level + 1
      if(pressureEstimate >= m_PressureLevels[plId+1])
        deltaPressure = m_PressureLevels[plId+1] - m_PressureLevels[plId];
      // Estimated pressure value is lover than pressure level
      else if(pressureEstimate < m_PressureLevels[plId])
        deltaPressure = 0;
      // Estimated pressure is between pressure level and pressure level + 1
      else
        deltaPressure = pressureEstimate - m_PressureLevels[plId];

      // Increment pixel WV with the WV corresponding to delta_pressure
      // To obtain the correct unit, we will multiply the result by 10/g at the end.
      waterVapor += inPix[plId] * deltaPressure;
    }

    // Take care of the last pressure level
    if(pressureEstimate > m_PressureLevels.back())
    {
      deltaPressure = pressureEstimate - m_PressureLevels.back();
      waterVapor += inPix[inPix.Size() - 1] * deltaPressure;
    }

    // Return the Water Vapor (g/cm2)
    waterVapor *= 10 / CONST_g;

    return waterVapor;
  }

  size_t OutputSize(const std::array<size_t, 2> & nbBands) const
  {
    if(!(nbBands[0] == m_PressureLevels.size()))
    {
      vnsStaticExceptionBusinessMacro("Number of pressure level doesn't match.");
    }
    return nbBands[0];
  }

private:
  /** parameters declaration */
  PressureLevelsType m_PressureLevels;

};

}
}

#endif /* __vnsEra5SpecificHumiditiesFunctor_h */
