/*
 * Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              *
 *                                                                                                          *
 ************************************************************************************************************
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 15 nov. 2017 : Creation                                           *
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$                                                                                                     *
 *                                                                                                          *
 ************************************************************************************************************/
#include "vnsMACv2ClimatoFileHandler.h"
#include "vnsLoggers.h"
#include "vnsMacro.h"
#include "vnsMath.h"
#include "vnsDate.h"
#include "itksys/SystemTools.hxx"
#include <itkImageRegionConstIterator.h>
#include <itkImageRegionIterator.h>
#include "vnsUtilities.h"
#include <iostream>
#include <string>
#include <algorithm>

namespace vns
{
const unsigned int MACv2ClimatoFileHandler::NETCDF_MAX_DATASET_NUMBER = 100;


/** Constructor */
MACv2ClimatoFileHandler::MACv2ClimatoFileHandler()
  : m_beforeFileFound(false),
    m_afterFileFound(false),
    m_IsValid(false),
    m_ImageTimeAquisitionJD(0)
{
}

/** Destructor */
MACv2ClimatoFileHandler::~MACv2ClimatoFileHandler()
{
}

void MACv2ClimatoFileHandler::cleanup()
{
  m_IsValid               = false;
  m_ImageTimeAquisitionJD = 0;

  // Internal algo data
  m_beforeAOTs.clear();   // AOT for before file
  m_afterAOTs.clear();    // AOT for before file
}
void MACv2ClimatoFileHandler::verifyData()
{
  // No file given
  if (!m_beforeFileFound and !m_afterFileFound)
  {
    vnsExceptionDataMacro("No file given to treat")
  }
}


void MACv2ClimatoFileHandler::ExtractAOTData(const CornerType& p_CenterCorner, const double pImageProductAquisitionTimeJD)
{
  // Do some cleanup
  this->cleanup();

  // Get the acquisition date in julian day
  m_ImageTimeAquisitionJD = pImageProductAquisitionTimeJD;

  // No need to continue if no valid file found
  if (!m_afterFileFound && !m_beforeFileFound)
  {
    vnsLogInfoMacro("No MACv2 Climato file found for JD date " << Date::GetDateUTCFromTm(Date::GetTmFromJulianDay(m_ImageTimeAquisitionJD)));
    return;
  }

  // Check that input file are available
  this->verifyData();
  // ---------------------------------------------------------------------------------------------
  // Process file after date to interpolate
  if (m_afterFileFound)
  {
    vnsLogInfoMacro("Treating after MACv2 Climato file : " << m_afterFile.AOT_Filename);
    this->TreatOneFile(m_afterFile, p_CenterCorner, AFTER);
  }
  // Process file before date to interpolate
  if (m_beforeFileFound)
  {
    vnsLogInfoMacro("Treating before MACv2 Climato file : " << m_beforeFile.AOT_Filename);
    this->TreatOneFile(m_beforeFile, p_CenterCorner, BEFORE);
  }

  // Consolidate data
  // only after
  if (m_afterFileFound && !m_beforeFileFound)
  {
    m_IsValid        = true;
    m_AOTImage       = m_AOTAfterImage;
  }
  // only before
  if (!m_afterFileFound && m_beforeFileFound)
  {
    m_IsValid        = true;
    m_AOTImage       = m_AOTBeforeImage;
  }
  // Both found need temporal interpolation
  if (m_afterFileFound && m_beforeFileFound)
  {
    this->TemporalInterpolation();
    m_IsValid = true;
  }
}

void MACv2ClimatoFileHandler::TreatOneFile(MACv2FileType const& p_ClimatoFile, const CornerType& p_CenterCorner, const Mode& p_mode)
{
  // Retrive the input file
  const std::string   l_AOTFile           = p_ClimatoFile.AOT_Filename;
  // and check its validity
  if (l_AOTFile.empty())
  {
    vnsLogInfoMacro("l_AOTFile: " << l_AOTFile);
    vnsExceptionAlgorithmsProcessingMacro("Missing one Climato file")
  }

  // Get AOT Data
  AOTMapType l_AOTValueMap;
  std::stringstream l_ss;
  l_ss.str("");
  bool         l_isValid = true;
  unsigned int l_idx     = 0;
  while (l_isValid)
  {
    // Browse the different datset contained in the NetCDF file
    l_ss << l_AOTFile << "?&sdataidx=" << l_idx;
    // Ensure that dataset is valide before accessing its content
    if (isValidDataset(l_ss.str()))
    {
      vnsLogInfoMacro("Opening: " << l_ss.str());
      const std::pair<std::string, VectorPixel> l_value = GetClimatoValueAndDescription(l_ss.str(), p_CenterCorner);
      vnsLogDebugMacro("Current layer description " << l_value.first)
      // Loading the data is the dataset is not empty
      if (!l_value.first.empty())
      {
        // TODO better check that the layer correspond to monthly aod
        // for now it is hardcoded
        vnsLogDebugMacro("Found " << l_value.first);
        l_AOTValueMap[l_value.first] = l_value.second;
        l_isValid = false;
        break;
      }
      l_ss.str("");
      ++l_idx;
      // Ensure that there is not more dataset than expected
      if (l_idx > NETCDF_MAX_DATASET_NUMBER)
      {
        vnsExceptionDataMacro("Netcdf " << l_AOTFile << " is malformed, stopping processing");
      }
    }
    else
    {
      vnsExceptionDataMacro("Netcdf " << l_AOTFile << " is not valid.")
      l_isValid = false;
      break;
    }
  }
  // Verify Integrity
  if (l_AOTValueMap.size() != 1)
  {
    vnsExceptionDataMacro("AOT file does not contains the monthly aod");
  }

  // Get the aot image (interpolation based on product Day of Year)
  this->ComputeAOT(l_ss.str(), p_mode);

  // Affect to corresponding
  switch (p_mode)
  {
  case BEFORE:
    // AOT for before file
    m_beforeAOTs       = l_AOTValueMap;
    break;
  case AFTER:
    // AOT for after file
    m_afterAOTs       = l_AOTValueMap;
    break;
  default:
    // this should not happen
    break;
  }
}

/** Temporal interpolation between MACv2 Climato files */
void MACv2ClimatoFileHandler::TemporalInterpolation()
{
  // Compute the weights for both climato files based on their year
  // (the interpolation whitin year is already done)
  // load the three dates to interpolate
  Tm l_interpDate = Date::GetTmFromJulianDay(m_ImageTimeAquisitionJD);
  Tm l_beforeDate = Date::GetTmFromJulianDay(m_beforeFile.JulianDayAquisition);
  Tm l_afterpDate = Date::GetTmFromJulianDay(m_afterFile.JulianDayAquisition);
  // Compute the weights for linear interpolation
  double l_denom = (double)(l_afterpDate.tm_year - l_beforeDate.tm_year);
  double l_weightAfter = 0;
  // Managing the case where denomiator is 0, otherwise it means that after and before are the same
  if(l_denom)
  {
    l_weightAfter = (l_interpDate.tm_year - l_beforeDate.tm_year) / l_denom;
  }
  double l_weightBefore = 1 - l_weightAfter;
  // Logging debug information
  vnsLogDebugMacro("YearBefore : " << l_beforeDate.tm_year);
  vnsLogDebugMacro("YearAfter : " << l_afterpDate.tm_year);
  vnsLogDebugMacro("YearProduct : " << l_interpDate.tm_year);
  vnsLogDebugMacro("WeightAfter : " << std::setprecision(20) << l_weightAfter);
  vnsLogDebugMacro("WeightBefore : " << std::setprecision(20) << l_weightBefore);

  // Interpolation of the AOT before and after
  itk::ImageRegionIterator<AOTImageType> l_afterIt(m_AOTAfterImage, m_AOTBeforeImage->GetLargestPossibleRegion());
  itk::ImageRegionIterator<AOTImageType> l_beforIt(m_AOTBeforeImage, m_AOTAfterImage->GetLargestPossibleRegion());
  while (!l_afterIt.IsAtEnd() && !l_beforIt.IsAtEnd())
  {
    // Application of the weights
    l_afterIt.Set(l_afterIt.Get() * l_weightAfter + l_beforIt.Get() * l_weightBefore);
    ++l_afterIt;
    ++l_beforIt;
  }
  // Set result image
  m_AOTImage = m_AOTAfterImage;
}

/** Read the MACv2 Climato file and interpolate pixel at lat/long */
std::pair<std::string, MACv2ClimatoFileHandler::VectorPixel> 
    MACv2ClimatoFileHandler::GetClimatoValueAndDescription(const std::string& filename, 
                                                           const CornerType& p_CenterCorner)
{
  // Read the file
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(filename);
  reader->Update();

  // Interpolate
  const ImageType*    image = reader->GetOutput();
  ContinuousIndexType l_contindex;
  PointType           l_point;
  // Compute Lat,Long coordinate of the central point of the image
  // Read the UsefulImageGeoCoverageCenterCorner data from the HEADER_IMAGE_INFORMATIONS registered file
  l_point[0] = p_CenterCorner.Longitude;
  l_point[1] = p_CenterCorner.Latitude;
  image->TransformPhysicalPointToContinuousIndex(l_point, l_contindex);
  InterpolatorType::Pointer interp = InterpolatorType::New();
  interp->SetInputImage(reader->GetOutput());
  VectorPixel l_outPix = static_cast<VectorPixel>(interp->EvaluateAtContinuousIndex(l_contindex));

  // Search description
  std::string   l_desc("");
  ListOfStrings l_longnames = GetMetadataValueAsListOfString(reader->GetOutput(), "long_name");
  // Search description containing one of the models requested
  for (ListOfStrings::const_iterator it = l_longnames.begin(); it != l_longnames.end(); it++)
  {
    std::string l_tmpdesc = *it;
    // we retrive the layer base on his description
    // TODO this layer name should not be hardcoded
    std::string l_monthlyaodName = "monthly average - aod";
    // convert to lower case before search
    std::transform(l_tmpdesc.begin(), l_tmpdesc.end(), l_tmpdesc.begin(), ::tolower);
    if (l_tmpdesc.find(l_monthlyaodName) != std::string::npos)
    {
      l_desc = *it;
      if (l_desc.find("=") != std::string::npos)
      {
        l_desc = l_desc.substr(l_desc.find("=") + 1);
      }
      break;
    }
  }
  // Return the values and its description
  return std::pair<std::string, VectorPixel>(l_desc, l_outPix);
}


/**
 * Get the AOT interpolated within year
 */
void MACv2ClimatoFileHandler::ComputeAOT(const std::string& l_AOTDatasetPath, const Mode& p_mode)
{
  // Get AOT Data
  vnsLogDebugMacro("Opening: " << l_AOTDatasetPath);
  // Read the file
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(l_AOTDatasetPath);
  reader->Update();

  // Get the aot dataset
  const ImageType* image = reader->GetOutput();
  vnsLogDebugMacro("Dataset componants per pixel: " << image->GetNumberOfComponentsPerPixel())

  // Convert date to interpolate in year and doy
  Tm l_interpDate = Date::GetTmFromJulianDay(m_ImageTimeAquisitionJD);
  vnsLogDebugMacro("Current year:" << l_interpDate.tm_year << ",doy:" << l_interpDate.tm_yday);

  // Get time values associated to the layer
  ListOfDoubles l_climatoTimes = GetMetadataValueAsDoubleVector(image, "time_VALUES");

  // Check that the time dimensions matches
  if(image->GetNumberOfComponentsPerPixel() != l_climatoTimes.size())
  {
    vnsExceptionDataMacro("Time dimensions do not match");
  }

  // Retrieve the times index needed for the interpolation
  unsigned int l_before_idx = 0;
  unsigned int l_after_idx = 0;
  // Case would require extrapolation before
  if(l_interpDate.tm_yday <= l_climatoTimes.at(0))
  {
    l_before_idx = 0;
    l_after_idx = 0;
  }
  // Case would require extrapolation after
  else if(l_interpDate.tm_yday >= l_climatoTimes.at(l_climatoTimes.size()-1))
  {
    l_before_idx = l_climatoTimes.size()-1;
    l_after_idx = l_climatoTimes.size()-1;
  }
  // Case interpolation is possible
  else
  {
    // loop according the time dimension
    for (unsigned int i = 0; i < l_climatoTimes.size()-1; i++)
    {
      // to find the days of year that surround the date to interpolate
      if(l_climatoTimes.at(i) <= l_interpDate.tm_yday 
          && l_climatoTimes.at(i+1) >= l_interpDate.tm_yday)
      {
          l_before_idx = i;
          l_after_idx = i+1;
      }
    }
  }
  vnsLogDebugMacro("temporal idx before=" << l_before_idx << ",doy=" << l_climatoTimes.at(l_before_idx))
  vnsLogDebugMacro("temporal idx after=" << l_after_idx << ",doy=" << l_climatoTimes.at(l_after_idx))

  // Compute the weights needed for the interpolation
  double l_denom = (l_climatoTimes.at(l_after_idx) - l_climatoTimes.at(l_before_idx));
  double l_weightBefore = 0;
  double l_weightAfter = 0;
  // Case where interpolation is possible (before != after)
  // Otherwise the interpolation does nothing but do not fail.
  if(l_denom)
  {
    l_weightAfter = (l_interpDate.tm_yday - l_climatoTimes.at(l_before_idx)) / l_denom;
  }
  l_weightBefore = 1 - l_weightAfter;
  vnsLogDebugMacro("l_weightBefore=" << l_weightBefore)
  vnsLogDebugMacro("l_weightAfter=" << l_weightAfter)

  // Allocate internal image (input)
  itk::ImageRegionConstIterator<ImageType> l_inIt(image, image->GetLargestPossibleRegion());
  l_inIt.GoToBegin();

  // Allocate internal image (output)
  // This allocation is based on input image extent
  itk::ImageRegionIterator<AOTImageType> l_outIt;
  switch (p_mode)
  {
  case AFTER:
    m_AOTAfterImage = AOTImageType::New();
    m_AOTAfterImage->SetRegions(image->GetLargestPossibleRegion());
    m_AOTAfterImage->SetOrigin(image->GetOrigin());
    m_AOTAfterImage->SetSignedSpacing(image->GetSignedSpacing());
    m_AOTAfterImage->Allocate();
    m_AOTAfterImage->FillBuffer(0.0);
    l_outIt = itk::ImageRegionIterator<AOTImageType>(m_AOTAfterImage, image->GetLargestPossibleRegion());
    break;
  case BEFORE:
    m_AOTBeforeImage = AOTImageType::New();
    m_AOTBeforeImage->SetRegions(image->GetLargestPossibleRegion());
    m_AOTBeforeImage->SetOrigin(image->GetOrigin());
    m_AOTBeforeImage->SetSignedSpacing(image->GetSignedSpacing());
    m_AOTBeforeImage->Allocate();
    m_AOTBeforeImage->FillBuffer(0.0);
    l_outIt = itk::ImageRegionIterator<AOTImageType>(m_AOTBeforeImage, image->GetLargestPossibleRegion());
    break;
  }
  l_outIt.GoToBegin();

  // Browse the images and compute the interpolation
  while (!l_inIt.IsAtEnd() && !l_outIt.IsAtEnd())
  {
    // Interpolated aod based on monthly weights
    l_outIt.Set(l_weightBefore*l_inIt.Get()[l_before_idx] +
                l_weightAfter*l_inIt.Get()[l_after_idx]);
    ++l_outIt;
    ++l_inIt;
  }
  return;  
}


/**
 * Extract variables from dataset, as vector of doubles
 */
MACv2ClimatoFileHandler::ListOfDoubles MACv2ClimatoFileHandler::GetMetadataValueAsDoubleVector(const ImageType* image, std::string const& name)
{
  MACv2ClimatoFileHandler::ListOfDoubles l_ret;
  // First extracting the raw variable as string
  std::string                    value = GetMetadataValueAsString(image, name);
  // Ensure that variables has a value to extract
  if (value.find("={") != std::string::npos)
  {
    value = value.substr(value.find("={") + 2);
    value = value.substr(0, value.size() - 1);
    std::istringstream iss(value);
    double             sub;
    char               c;
    // convert the value as string to double
    while (iss >> sub)
    {
      l_ret.push_back(sub);
      // Remove coma
      iss >> c;
    };
  }
  // the vector of double values
  return l_ret;
}

/**
 * Extract the first variable from dataset that match the search pattern, as string
 */
std::string MACv2ClimatoFileHandler::GetMetadataValueAsString(const ImageType* image, std::string const& name)
{
  std::string   l_ret("");
  // Get all variables that match the searched name
  ListOfStrings l_vec = GetMetadataValueAsListOfString(image, name);
  // If the list is not empty, it keeps only the first result
  if (l_vec.size() != 0)
  {
    l_ret = l_vec.front();
  }
  return l_ret;
}

/**
 * Extract all variables from the dataset that match the search pattern, as list of string
 */
MACv2ClimatoFileHandler::ListOfStrings MACv2ClimatoFileHandler::GetMetadataValueAsListOfString(const ImageType* image, std::string const& name)
{
  ListOfStrings                          l_ret;
  // Load all the image metadata
  itk::MetaDataDictionary                meta = image->GetMetaDataDictionary();
  itk::MetaDataDictionary::ConstIterator it   = meta.Begin();
  // Loop on the metadata
  for (; it != meta.End(); ++it)
  {
    std::string value;
    itk::ExposeMetaData<std::string>(meta, it->first, value);
    itk::ExposeMetaData<std::string>(meta, it->first, value);
    // Keep only the variables matching the search name
    if (value.find(name) != std::string::npos)
    {
      l_ret.push_back(value);
    }
  }
  return l_ret;
}

/**
 * Validate the dataset based on file name and dataset index
 * i.e. filename = {basename}?&sdataidx={index}
 */
bool MACv2ClimatoFileHandler::isValidDataset(const std::string& filename)
{
  bool l_ret = true;
  // Read the file
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(filename);
  try
  {
    // Try to load the data
    reader->Update();
  }
  catch (itk::ExceptionObject& e)
  {
    // In case of exception the datset is considered invalid
    l_ret = false;
  }
  return l_ret;
}


/** PrintSelf method */
void MACv2ClimatoFileHandler::PrintSelf(std::ostream& os, itk::Indent indent) const
{
  this->Superclass::PrintSelf(os, indent);
}

} // End namespace vns
