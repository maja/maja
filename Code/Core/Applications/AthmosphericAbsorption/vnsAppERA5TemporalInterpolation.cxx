/*
 * Copyright (C) 2022 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
 *                                                                                                          *
 * Author: CS GROUP - France                                                                                *
 *                                                                                                          *
 ************************************************************************************************************/

#include "vnsLoggers.h"
#include "vnsMacro.h"
#include "vnsDate.h"
#include "vnsApplyOffsetScaleFunctor.h"
#include "vnsTemporalInterpolationFunctor.h"
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbFunctorImageFilter.h"
#include "otbGDALDriverManagerWrapper.h"

namespace vns
{
namespace Wrapper
{

using namespace otb::Wrapper;

class ERA5TemporalInterpolation : public Application
{
public:
  /** Standard class typedefs. */
  using Self = ERA5TemporalInterpolation;
  using Superclass = otb::Wrapper::Application;
  using Pointer = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;

  /** Standard macro */
  itkNewMacro(Self)

  itkTypeMacro(ERA5TemporalInterpolation, otb::Wrapper::Application)

  /** Some convenient typedefs. */
  using ApplyOffsetScaleFilterType = otb::FunctorImageFilter<Functor::ApplyOffsetScaleFunctor>;
  using TemporalInterpolationFilterType = otb::FunctorImageFilter<Functor::TemporalInterpolationFunctor>;

  /** The pressure levels vector */
  using PressureLevelsType = std::vector<int>;

private:
  void DoInit()
  {
    SetName("ERA5TemporalInterpolation");
    SetDescription("Temporal interpolation of Era5SpecificHumidityMap.");
    Loggers::GetInstance()->Initialize(GetName());
    // Documentation
    SetDocLongDescription("Compute the temporal interplation of two ERA5 specific humidities map.");
    SetDocLimitations("None");
    SetDocAuthors("MAJA-Team");
    SetDocSeeAlso("MAJA Doc");

    AddDocTag("Statistics");

    AddParameter(ParameterType_Double, "datejulian", "Date to extract in julian day");
    MandatoryOff("datejulian");
    AddParameter(ParameterType_String, "dateutc", "Date to extract in UTC format");
    MandatoryOff("dateutc");

    AddParameter(ParameterType_Group, "before", "ERA5 data before the date");
    MandatoryOff("before");
    AddParameter(ParameterType_Double, "before.datejulian", "CAMS julian date of before");
    AddParameter(ParameterType_String, "before.dateutc", "CAMS julian date of before in UTC format");
    AddParameter(ParameterType_InputImage, "before.era5file", "ERA5 file of before");

    AddParameter(ParameterType_Group, "after", "ERA5 data after the date");
    MandatoryOff("after");
    AddParameter(ParameterType_Double, "after.datejulian", "CAMS julian date of after");
    AddParameter(ParameterType_String, "after.dateutc", "CAMS julian date of after in UTC format");
    AddParameter(ParameterType_InputImage, "after.era5file", "ERA5 of after");

    AddParameter(ParameterType_String, "pl", "Pressure levels");
    SetParameterDescription("pl", "The presure level values read from the ERA5 file, comma separated.");
    MandatoryOff("pl");

    AddParameter(ParameterType_OutputImage, "out", "Interpolated SH map");
    SetParameterDescription("out", "The map of Specific Humidity interpolated at the requested date.");

    AddRAMParameter("ram");
    SetDefaultParameterInt("ram", 2048);
  }

  void DoUpdateParameters()
  {
  }

  /** Read the pressure levels, scale and offset from the provided file
   *
   * /param[in] filename path to the ERA5 file
   * /return true if no error
   */
  bool FetchMetadata(const std::string& filename)
  {
    // Fetch the metadata
    auto dataset = otb::GDALDriverManagerWrapper::GetInstance().Open(filename);
    auto fileMetadata = dataset->GetDataSet()->GetMetadata(NULL);
    // Check that the metadata were correctly read
    if (!fileMetadata)
      return false;

    // Fetch the pressure level as a string
    std::string valueAsString = CSLFetchNameValue(fileMetadata, MDKey_pressure_level_values.c_str());
    // Check that the string is not empty
    if (valueAsString.empty())
      return false;
    // Remove the brackets arround the data
    this->m_PressureLevels = valueAsString.substr(1, valueAsString.length() - 2);

    // Fetch the offset
    valueAsString = CSLFetchNameValue(fileMetadata, MDKey_offset.c_str());
    // Check that the string is not empty
    if (valueAsString.empty())
      return false;
    // Convert to double
    this->m_Offset = otb::Utils::LexicalCast<double>(valueAsString, "offset");

    // Fetch the scale factor
    valueAsString = CSLFetchNameValue(fileMetadata, MDKey_scale_factor.c_str());
    // Check that the string is not empty
    if (valueAsString.empty())
      return false;
    // Convert to double
    m_ScaleFactor = otb::Utils::LexicalCast<double>(valueAsString, "offset");

    return true;
  }

  /** Return the julian date
   *
   * If "julian" is provided, return the corresponding value.
   * If "utc" is provided, converts it to julian and returns it.
   * Else, rises an exception
   *
   * /param[in] julian application parameter for julian date
   * /param[in] utc    application parameter for UTC date
   * /return the required julian date
   * /throw StaticExceptionBusinessException if neigher julien or utc date is provided
   */
  double GetJulianDate(const std::string & julian, const std::string & utc)
  {
    // If the input date is already Julian day
    if (this->HasValue(julian))
    {
      // Return directly
      return this->GetParameterDouble(julian);
    }
    // If the input date is UTC
    else if (this->HasValue(utc))
    {
      // Convert UTC to Julian day
      auto utcDate = this->GetParameterString(utc);
      auto TmDate = Date::GetTmFromDateUTC(utcDate);
      auto JDdate = Date::GetJulianDayAsDouble(TmDate);
      return JDdate;
    }
    else
    {
      vnsStaticExceptionBusinessMacro("No date given. You need to provide either " << julian << " or " << utc << ".");
    }
  }

  void DoExecute()
  {
    // Get the date to extract
    double julianDate = GetJulianDate("datejulian", "dateutc");

    // Get the BEFORE ERA5 file and date if provided
    bool hasBeforeDate = false;
    double beforeJulianDate;
    if (this->HasValue("before.era5file"))
    {
      // Fetch the before date
      beforeJulianDate = GetJulianDate("before.datejulian", "before.dateutc");
      // Check that before date is indeed before
      if (beforeJulianDate > julianDate)
      {
        vnsStaticExceptionBusinessMacro("Before ERA5 is not before the requested date");
      }
      // Read the pressure levels, scale and offset from before ERA file
      if (!FetchMetadata(this->GetParameterString("before.era5file")))
        vnsStaticExceptionBusinessMacro("Cannot fetch the metadata from " << this->GetParameterString("before.era5file") << ".");
      // Set the Offset and Scale Factor
      m_BeforeApplyOffsetScaleFilter = ApplyOffsetScaleFilterType::New();
      m_BeforeApplyOffsetScaleFilter->SetInput(this->GetParameterDoubleVectorImage("before.era5file"));
      m_BeforeApplyOffsetScaleFilter->GetModifiableFunctor().SetScaleFactor(m_ScaleFactor);
      m_BeforeApplyOffsetScaleFilter->GetModifiableFunctor().SetOffset(m_Offset);
      // The before ERA5 file is correctly provided
      hasBeforeDate = true;
    }

    // Get the AFTER ERA5 file and date if provided
    bool hasAfterDate = false;
    double afterJulianDate;
    if (this->HasValue("after.era5file"))
    {
      // Fetch the after date
      afterJulianDate = GetJulianDate("after.datejulian", "after.dateutc");
      // Check that after date is indeed after
      if (afterJulianDate < julianDate)
      {
        vnsStaticExceptionBusinessMacro("AFTER ERA5 is not after the requested date");
      }
      // Read the pressure levels, scale and offset from after ERA file
      if (!FetchMetadata(this->GetParameterString("after.era5file")))
        vnsStaticExceptionBusinessMacro("Cannot fetch the metadata from " << this->GetParameterString("after.era5file") << ".");
      // Set the Offset and Scale Factor
      m_AfterApplyOffsetScaleFilter = ApplyOffsetScaleFilterType::New();
      m_AfterApplyOffsetScaleFilter->SetInput(this->GetParameterDoubleVectorImage("after.era5file"));
      m_AfterApplyOffsetScaleFilter->GetModifiableFunctor().SetScaleFactor(m_ScaleFactor);
      m_AfterApplyOffsetScaleFilter->GetModifiableFunctor().SetOffset(m_Offset);
      // The after ERA5 file is correctly provided
      hasAfterDate = true;
    }

    // If two ERA5 files are provided, interpolation needed
    if(hasBeforeDate && hasAfterDate)
    {
      m_Era5TempoInterp = TemporalInterpolationFilterType::New();
      m_Era5TempoInterp->SetInput<0>(m_BeforeApplyOffsetScaleFilter->GetOutput());
      m_Era5TempoInterp->SetInput<1>(m_AfterApplyOffsetScaleFilter->GetOutput());
      m_Era5TempoInterp->GetModifiableFunctor().SetJulianDates(julianDate, beforeJulianDate, afterJulianDate);
      SetParameterOutputImage("out", m_Era5TempoInterp->GetOutput());
    }
    else if(hasBeforeDate)
    {
      SetParameterOutputImage("out", m_BeforeApplyOffsetScaleFilter->GetOutput());
    }
    else if(hasAfterDate)
    {
      SetParameterOutputImage("out", m_AfterApplyOffsetScaleFilter->GetOutput());
    }
    // No era file set -> error
    else
    {
      vnsStaticExceptionBusinessMacro("At least one ERA5 file is required.");
    }

    // Output the pressure levels
    SetParameterString("pl", m_PressureLevels);
  }

  /** Filters declaration */
  ApplyOffsetScaleFilterType::Pointer m_BeforeApplyOffsetScaleFilter;
  ApplyOffsetScaleFilterType::Pointer m_AfterApplyOffsetScaleFilter;
  TemporalInterpolationFilterType::Pointer m_Era5TempoInterp;

  /** Pressure levels */
  std::string m_PressureLevels;
  const std::string MDKey_pressure_level_values = "NETCDF_DIM_level_VALUES";

  /** Offset */
  double m_Offset;
  const std::string MDKey_offset = "q#add_offset";

  /** Scale factor */
  double m_ScaleFactor;
  const std::string MDKey_scale_factor = "q#scale_factor";
};

} // namespace Wrapper
} // namespace vns


OTB_APPLICATION_EXPORT(vns::Wrapper::ERA5TemporalInterpolation)
