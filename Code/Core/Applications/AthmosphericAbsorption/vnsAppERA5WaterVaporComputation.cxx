/*
 * Copyright (C) 2022 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
 *                                                                                                          *
 * Author: CS GROUP - France                                                                                *
 *                                                                                                          *
 ************************************************************************************************************/

#include "vnsLoggers.h"
#include "vnsEra5SpecificHumiditiesFunctor.h"
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbFunctorImageFilter.h"
#include "vector"

namespace vns
{
namespace Wrapper
{

using namespace otb::Wrapper;

class ERA5WaterVaporComputation : public Application
{
public:
  /** Standard class typedefs. */
  using Self = ERA5WaterVaporComputation;
  using Superclass = otb::Wrapper::Application;
  using Pointer = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;

  /** Standard macro */
  itkNewMacro(Self)

  itkTypeMacro(ERA5WaterVaporComputation, otb::Wrapper::Application)

  /** Some convenient typedefs. */
  using Era5SpecificHumiditiesFilterType = otb::FunctorImageFilter<Functor::Era5SpecificHumiditiesFunctor>;

  /** The pressure levels vector */
  using PressureLevelsType = std::vector<int>;

private:
  void DoInit()
  {
    SetName("ERA5WaterVaporComputation");
    SetDescription("Compute the water vapor from the Era5SpecificHumidityMap.");
    Loggers::GetInstance()->Initialize(GetName());
    // Documentation
    SetDocLongDescription("Compute the water vapor content using the ERA5 specific humidities map.");
    SetDocLimitations("None");
    SetDocAuthors("MAJA-Team");
    SetDocSeeAlso("MAJA Doc");

    AddDocTag("Statistics");

    AddParameter(ParameterType_InputImage, "in", "ERA5 specific humidities map");
    SetParameterDescription("in", "The ERA5 specific humidities map.");

    AddParameter(ParameterType_String, "pl", "Pressure levels");
    SetParameterDescription("pl", "The presure level values, comma separated.");

    AddParameter(ParameterType_InputImage, "dtm", "Digital elevation model");
    SetParameterDescription("dtm", "Image used as DEM at L2_coarse resolution.");

    AddParameter(ParameterType_OutputImage, "out", "Water vapor map");
    SetParameterDescription("out", "The map of water vapor content computed from ERA5 data at L2_coarse resolution in g/cm2.");

    AddRAMParameter("ram");
    SetDefaultParameterInt("ram", 2048);
  }

  void DoUpdateParameters()
  {
  }

  void DoExecute()
  {
    m_Era5SpecificHumiditiesFilter = Era5SpecificHumiditiesFilterType::New();
    m_Era5SpecificHumiditiesFilter->SetInput<0>(this->GetParameterDoubleVectorImage("in"));
    m_Era5SpecificHumiditiesFilter->SetInput<1>(this->GetParameterImage("dtm"));
    m_Era5SpecificHumiditiesFilter->GetModifiableFunctor().SetPressureLevels(this->GetParameterAsString("pl"));

    SetParameterOutputImage("out", m_Era5SpecificHumiditiesFilter->GetOutput());
  }

  /** Filters declaration */
  Era5SpecificHumiditiesFilterType::Pointer m_Era5SpecificHumiditiesFilter;
};

} // namespace Wrapper
} // namespace vns


OTB_APPLICATION_EXPORT(vns::Wrapper::ERA5WaterVaporComputation)
