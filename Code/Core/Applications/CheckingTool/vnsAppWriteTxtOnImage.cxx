/*
 * Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              *
 *                                                                                                          *
 ************************************************************************************************************
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 15 nov. 2017 : Creation
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "vnsWriteTxtOnImageFilter.h"

#include <string>

namespace vns
{

namespace Wrapper
{

using namespace otb::Wrapper;

class WriteTxtOnImage : public Application
{
public:
  /** Standard class typedefs. */
  typedef WriteTxtOnImage               Self;
  typedef otb::Wrapper::Application     Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(WriteTxtOnImage, otb::Wrapper::Application);

  typedef UInt8VectorImageType  UInt8VectorImage;
  typedef DoubleVectorImageType DoubleVectorImage;


  typedef vns::WriteTxtOnImageFilter<DoubleVectorImage, UInt8VectorImage> FilterType;
  typedef typename FilterType::Pointer                                    FilterPointerType;


private:
  void DoInit()
  {
    SetName("WriteTxtOnImage");
    SetDescription("WriteTxtOnImage");
    Loggers::GetInstance()->Initialize(GetName());
    // Documentation
    SetDocLongDescription("This application writes an std::string into an image");
    SetDocLimitations("None");
    SetDocAuthors("MAJA-Team");
    SetDocSeeAlso("MAJA Doc");

    AddDocTag("Mask");

    AddParameter(ParameterType_InputImage, "in", "input image");
    AddParameter(ParameterType_OutputImage, "out", "output image");
    SetParameterDescription("out", "output image");

    AddParameter(ParameterType_String, "font", "font file");
    SetParameterDescription("font", "font file");

    AddParameter(ParameterType_String, "text", "text to add");
    SetParameterDescription("text", "text to add");

    AddParameter(ParameterType_Int, "fontsize", "font size");
    SetParameterDescription("fontsize", "font size");


    AddParameter(ParameterType_Int, "red", "red color");
    SetParameterDescription("red", "Set the used color");
    SetDefaultParameterInt("red", 0);

    AddParameter(ParameterType_Int, "green", "green color");
    SetParameterDescription("green", "Set the used color");
    SetDefaultParameterInt("green", 255);

    AddParameter(ParameterType_Int, "blue", "blue color");
    SetParameterDescription("blue", "Set the used color");
    SetDefaultParameterInt("blue", 0);

    AddRAMParameter("ram");
    SetDefaultParameterInt("ram", 2048);
  }

  void DoUpdateParameters()
  {
  }


  void DoExecute()
  {
    // Init filters
    filter = FilterType::New();

    filter->SetInput(this->GetParameterDoubleVectorImage("in"));
    filter->SetFontSize(this->GetParameterInt("fontsize"));
    filter->SetFontFileName(this->GetParameterString("font"));
    filter->SetText(this->GetParameterString("text"));
    filter->SetTextColor(this->GetParameterInt("red"), this->GetParameterInt("green"), this->GetParameterInt("blue"));
    filter->UpdateData();


    SetParameterOutputImage<UInt8VectorImage>("out", filter->GetOutput());
  }


  /** Filters declaration */
  FilterPointerType filter;
};

} // namespace Wrapper
} // namespace vns

OTB_APPLICATION_EXPORT(vns::Wrapper::WriteTxtOnImage)
