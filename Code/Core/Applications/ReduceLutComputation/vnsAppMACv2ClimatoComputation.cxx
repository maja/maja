/*
 * Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/************************************************************************************************************
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              *
 *                                                                                                          *
 ************************************************************************************************************
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 15 nov. 2017 : Creation
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "vnsUtilities.h"
#include "vnsDate.h"
#include "vnsMACv2ClimatoFileHandler.h"

namespace vns
{
namespace Wrapper
{

using namespace otb::Wrapper;

class MACv2ClimatoComputation : public Application
{
public:
  /** Standard class typedefs. */
  typedef MACv2ClimatoComputation       Self;
  typedef otb::Wrapper::Application     Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(MACv2ClimatoComputation, otb::Wrapper::Application);

  /** Some convenient typedefs. */


private:
  void DoInit()
  {
    SetName("MACv2ClimatoComputation");
    SetDescription("MACv2ClimatoComputation application to retrive AOT.");
    Loggers::GetInstance()->Initialize(GetName());
    // Documentation
    SetDocLongDescription("This application retrieve the AOT from MACv2 climato files");
    SetDocLimitations("None");
    SetDocAuthors("MAJA-Team");
    SetDocSeeAlso("MAJA Doc");

    AddDocTag("Statistics");

    //TODO verify if no other params are required
    AddParameter(ParameterType_Float, "lat", "Latitude of point to extract");
    AddParameter(ParameterType_Float, "lon", "Longitude of point to extract");
    AddParameter(ParameterType_Float, "datejulian", "Date to extract in julian day");
    MandatoryOff("datejulian");
    AddParameter(ParameterType_String, "dateutc", "Date to extract in UTC format");
    MandatoryOff("dateutc");

    AddParameter(ParameterType_Group, "before", "MACv2 Climato file before the date");
    MandatoryOff("before");
    AddParameter(ParameterType_Float, "before.datejulian", "MACv2 Climato file julian date (start date)");
    AddParameter(ParameterType_String, "before.dateutc", "MACv2 Climato file date in UTC format (start date)");
    AddParameter(ParameterType_InputFilename, "before.aotfile", "MACv2 Climato file");

    AddParameter(ParameterType_Group, "after", "MACv2 Climato file after the date");
    MandatoryOff("after");
    AddParameter(ParameterType_Float, "after.datejulian", "MACv2 Climato file julian date (start date)");
    AddParameter(ParameterType_String, "after.dateutc", "MACv2 Climato file date in UTC format (start date)");
    AddParameter(ParameterType_InputFilename, "after.aotfile", "MACv2 Climato file");

    AddParameter(ParameterType_OutputImage, "interpaot", "Resulting interpolation of AOT");

    AddRAMParameter("ram");
    SetDefaultParameterInt("ram", 2048);
  }

  void DoUpdateParameters()
  {
  }


  void DoExecute()
  {
    // Instanciate MACv2 Climato file handler
    MACv2ClimatoFileHandler::Pointer l_climatoHandler = MACv2ClimatoFileHandler::New();
    
    // Get the point to extract
    MACv2ClimatoFileHandler::CornerType l_corner;
    l_corner.Latitude  = this->GetParameterFloat("lat");
    l_corner.Longitude = this->GetParameterFloat("lon");

    // Get the date to extract
    double l_julianDate = 0;
    if (this->HasValue("datejulian"))
    {
      l_julianDate = this->GetParameterFloat("datejulian");
    }
    else if (this->HasValue("dateutc"))
    {
      // Convert the UTC date to Julian date
      l_julianDate = Date::GetJulianDayAsDouble(Date::GetTmFromDateUTC(this->GetParameterString("dateutc")));
    }
    else
    {
      vnsExceptionDataMacro("No date given to extract");
    }

    // Get the MACv2 Climato file
    // Before
    if ((this->HasValue("before.datejulian") || this->HasValue("before.dateutc")) 
          && this->HasValue("before.aotfile"))
    {
      const std::string l_beforeAOT  = this->GetParameterString("before.aotfile");

      // Get the date of the climato file
      double l_beforeDate = 0;
      if (this->HasValue("before.datejulian"))
      {
        l_beforeDate = this->GetParameterFloat("before.datejulian");
      }
      else if (this->HasValue("before.dateutc"))
      {
        l_beforeDate = Date::GetJulianDayAsDouble(Date::GetTmFromDateUTC(this->GetParameterString("before.dateutc")));
      }

      // Check the provided file is actually before the requested date
      if (l_beforeDate > l_julianDate)
      {
        vnsExceptionDataMacro("Before MACv2 Climato file is not before the requested date");
      }

      // Set in the handler
      l_climatoHandler->SetBeforeClimato(l_beforeAOT, l_beforeDate);
    }
    // After
    if ((this->HasValue("after.datejulian") || this->HasValue("after.dateutc")) 
          && this->HasValue("after.aotfile"))
    {
      const std::string l_afterAOT = this->GetParameterString("after.aotfile");

      // Get the date of the climato file
      double l_afterDate = 0;
      if (this->HasValue("after.datejulian"))
      {
        l_afterDate = this->GetParameterFloat("after.datejulian");
      }
      else if (this->HasValue("after.dateutc"))
      {
        l_afterDate = Date::GetJulianDayAsDouble(Date::GetTmFromDateUTC(this->GetParameterString("after.dateutc")));
      }

      // Check the provided file is actually after the requested date
      if (l_afterDate < l_julianDate)
      {
        vnsExceptionDataMacro("After MACv2 Climato file is not after the requested date");
      }
      // Set in the handler
      l_climatoHandler->SetAfterClimato(l_afterAOT, l_afterDate);
    }

    // Proceed with the AOT extraction and interpolation at requested date.
    l_climatoHandler->ExtractAOTData(l_corner, l_julianDate);

    //Check that the result is ok before setting the outputs.
    if (l_climatoHandler->isValid())
    {
      typedef MACv2ClimatoFileHandler::AOTImageType AOTImageType;
      this->SetParameterOutputImage<AOTImageType>("interpaot", l_climatoHandler->GetAOTImage());
    }
  }
};

} // namespace Wrapper
} // namespace vns


OTB_APPLICATION_EXPORT(vns::Wrapper::MACv2ClimatoComputation)
