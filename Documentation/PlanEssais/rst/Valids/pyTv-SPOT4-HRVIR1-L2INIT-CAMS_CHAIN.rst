pyTv-SPOT4-HRVIR1-L2INIT-CAMS_CHAIN
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Titre
*****

Validation fonctionnelle et numérique de la chaîne L2 (Pre-PostProcessing, entrée/sorties) en mode INIT
sur un produit L1C SPOT4 HRVIR1 en utilisant des données CAMS.


Objectif
********

L’objectif de cet essai est de valider le fonctionnement global de la chaîne L2 avec un produit SPOT4 HRVIR1 au format Muscate.


Description
***********

Les options de traitement sont :

- Estimation des aérosols depuis les données CAMS,
- Correction de l’environnement et des pentes,
- Ecriture à la résolution L2,

La validation se fait sur un contexte avec un DEM à 20m et un produit L2 à 20m.


Liste des données d’entrées
***************************

- SPOT4-HRVIR1_TEST_GIP_L2ALBD_L_BLACKCAR_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2ALBD_L_BLACKCAR_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2ALBD_L_CONTINEN_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2ALBD_L_CONTINEN_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2ALBD_L_DUST_____00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2ALBD_L_DUST_____00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2ALBD_L_ORGANICM_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2ALBD_L_ORGANICM_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2ALBD_L_SEASALT__00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2ALBD_L_SEASALT__00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2ALBD_L_SULPHATE_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2ALBD_L_SULPHATE_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2COMM_L_ALLSITES_00001_19980324_20130629.EEF
- SPOT4-HRVIR1_TEST_GIP_L2DIFT_L_BLACKCAR_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2DIFT_L_BLACKCAR_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2DIFT_L_CONTINEN_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2DIFT_L_CONTINEN_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2DIFT_L_DUST_____00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2DIFT_L_DUST_____00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2DIFT_L_ORGANICM_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2DIFT_L_ORGANICM_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2DIFT_L_SEASALT__00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2DIFT_L_SEASALT__00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2DIFT_L_SULPHATE_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2DIFT_L_SULPHATE_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2DIRT_L_BLACKCAR_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2DIRT_L_BLACKCAR_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2DIRT_L_CONTINEN_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2DIRT_L_CONTINEN_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2DIRT_L_DUST_____00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2DIRT_L_DUST_____00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2DIRT_L_ORGANICM_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2DIRT_L_ORGANICM_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2DIRT_L_SEASALT__00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2DIRT_L_SEASALT__00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2DIRT_L_SULPHATE_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2DIRT_L_SULPHATE_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2SITE_L_ALLSITES_00001_19980324_20130629.EEF
- SPOT4-HRVIR1_TEST_GIP_L2SMAC_L_ALLSITES_00001_19980324_20130629.EEF
- SPOT4-HRVIR1_TEST_GIP_L2TOCR_L_BLACKCAR_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2TOCR_L_BLACKCAR_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2TOCR_L_CONTINEN_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2TOCR_L_CONTINEN_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2TOCR_L_DUST_____00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2TOCR_L_DUST_____00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2TOCR_L_ORGANICM_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2TOCR_L_ORGANICM_00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2TOCR_L_SEASALT__00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2TOCR_L_SEASALT__00001_19980324_20130629.HDR
- SPOT4-HRVIR1_TEST_GIP_L2TOCR_L_SULPHATE_00001_19980324_20130629.DBL.DIR
- SPOT4-HRVIR1_TEST_GIP_L2TOCR_L_SULPHATE_00001_19980324_20130629.HDR
- SPOT4-HRVIR1-XS_20120528-100912-797_L1C_049-262-2_C_V1-0
- SPOT4-HRVIR1-XS_20120528-100912_TEST_AUX_REFDE2_049-262-2_1001.DBL.DIR
- SPOT4-HRVIR1-XS_20120528-100912_TEST_AUX_REFDE2_049-262-2_1001.HDR
- SPOT_OPER_EXO_CAMS_RA_20120528T090000_21000101T000000.DBL.DIR
- SPOT_OPER_EXO_CAMS_RA_20120528T090000_21000101T000000.HDR
- SPOT_OPER_EXO_CAMS_RA_20120528T120000_21000101T000000.DBL.DIR
- SPOT_OPER_EXO_CAMS_RA_20120528T120000_21000101T000000.HDR
- SPOT_OPER_EXO_ERA5_20120528T100000.DBL.DIR
- SPOT_OPER_EXO_ERA5_20120528T100000.HDR
- SPOT_OPER_EXO_ERA5_20120528T110000.DBL.DIR
- SPOT_OPER_EXO_ERA5_20120528T110000.HDR
- SPOT_OPER_EXO_MAC2_19850101T000000_20250101T000000.DBL.DIR
- SPOT_OPER_EXO_MAC2_19850101T000000_20250101T000000.HDR


Liste des produits de sortie
****************************

Produit SPOT4-HRVIR1 L2A MUSCATE

Prérequis
*********
Il n’y a pas de prérequis.

Durée attendue
***************
La durée d’exécution de l’essai n’est pas un critère attendu.

Epsilon utilisé sur la non regression
*************************************
0.001

Vérifications à effectuer
**************************
Les tests COMP_ASCII et COMP_IMAGE associés permettent de valider la non regression

Mise en oeuvre du test
**********************

Ce test est exécuté en lançant la commande :
ctest -R pyTv-SPOT4-HRVIR1-L2INIT-CAMS_CHAIN


Exigences
*********
Ce test couvre les exigences suivantes :
MACCS-Exigence 640 (C) ; MACCS-Exigence 650 (C) ; MACCS-Exigence 50 (C) ; MACCS-Exigence 630 (C) ; [JIRA] MAJA-3340;


Journal d’essai de la recette
*****************************

Notes sur le déroulement du test
--------------------------------
Rien de particulier n’a été noté lors du déroulement du test.

Conclusion du déroulement du test
---------------------------------
RAS

Validation du test
------------------

================== =================
Date de validation    Résultat
|today|              OK
================== =================

